<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.locator.model.Capture"%>
<%@page import="java.sql.ResultSet"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${pageContext.errorData.statusCode} Page Not Found</title>
    <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/img/favicon-loc.ico" />
    <!-- Bootstrap core CSS -->
    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/main.css" type="text/css" />
     <link rel="stylesheet" href="${pageContext.request.contextPath}/css/view.css" type="text/css" />
     <link rel="stylesheet" href="${pageContext.request.contextPath}/css/index.css" type="text/css" />
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <c:set var="root" value="${pageContext.request.contextPath}" />	
  </head>
  <body>
<div class="body">
  <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="${root}/home">Locator</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse" aria-expanded="true">
          <form class="navbar-form navbar-right search" action="${pageContext.request.contextPath}/find" method="post">
            <div class="form-group">
              <input type="text" placeholder="Look for" class="form-control findTextBox" name="txtfind" >
            </div>
            <div class="form-group">
              <select class="form-control" name="district">
              <option value="none">District</option>
							<option value="Colombo">Colombo</option>
							<option value="Kandy">Kandy</option>
							<option value="Galle">Galle</option>
							<option value="Ampara">Ampara</option>
							<option value="Anuradhapura">Anuradhapura</option>
							<option value="Badulla">Badulla</option>
							<option value="Batticaloa">Batticaloa</option>
							<option value="Gampaha">Gampaha</option>
							<option value="Hambantota">Hambantota</option>
							<option value="Jaffna">Jaffna</option>
							<option value="Kalutara">Kalutara</option>
							<option value="Kegalle">Kegalle</option>
							<option value="Kilinochchi">Kilinochchi</option>
							<option value="Kurunegala">Kurunegala</option>
							<option value="Mannar">Mannar</option>
							<option value="Matale">Matale</option>
							<option value="Matara">Matara</option>
							<option value="Moneragala">Moneragala</option>
							<option value="Mullativu">Mullativu</option>
							<option value="Nuwara Eliya">Nuwara Eliya</option>
							<option value="Polonnaruwa">Polonnaruwa</option>
							<option value="Puttalam">Puttalam</option>
							<option value="Ratnapura">Ratnapura</option>
							<option value="Trincomalee">Trincomalee</option>
							<option value="Vavuniya">Vavuniya</option>
              </select>
            </div>
            <button type="submit" class="btn btn-success form-control" name="btfind">Find</button>
            <div class="form-group">
            <a href="${pageContext.request.contextPath}/YourPlace" class="hlink">Your Shop</a>
            </div>
            <div class="form-group">
            <a href="${pageContext.request.contextPath}/ContactUs" class="hlink">Contact Us</a>
            </div>
          </form>
        </div><!--/.navbar-collapse -->
      </div>
    </nav>
	
	<div class="mainContent">	
	<form action="${root}/delete" method="post">
	<c:if test="${pageContext.errorData.statusCode == 404}">
		<article class="topcontent-rds-less">
				<div class="bg-danger"><div class="well-lg text-center"> 
				<h3 class="text-danger">We're sorry. The Web address you entered is not a functioning page on our site.</h3>
				<h4 class="text-default">Go to Locator's <a href="${root}/home"><u>Home</u></a> Page</h4>
				</div></div>
			</article>
	</c:if>	
	<c:if test="${pageContext.errorData.statusCode == 400}">
		<article class="topcontent-rds-less">
				<div class="bg-danger"><div class="well-lg text-center">
				<h3 class="text-danger">Bad request, invalid url. The request had bad syntax or was impossible to be satisified.</h3> 
				<h4 class="text-default">Go to Locator's <a href="${root}/home"><u>Home</u></a> Page</h4>
				</div></div>
			</article>
	</c:if>	
	<c:if test="${pageContext.errorData.statusCode != 404 and pageContext.errorData.statusCode !=400}">
		<article class="topcontent-rds-less">
				<div class="bg-danger"><div class="well-lg text-center"> 
				<h3 class="text-danger">The page You were looking for was not found. Sorry for the inconvenience caused.</h3>
				<h4 class="text-default">Go to Locator's <a href="${root}/home"><u>Home</u></a> Page</h4>
				</div></div>
			</article>
	</c:if>	
	</form>	
	<div class="row">
					<div class="div-trans text-center" style="background-color: transparent;">
                           
                            <form class="" action="${root}/find" method="post">
                            <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12" >
                             <h3>FIND PLACES AROUND YOUR AREA</h3>
							<div class="input-group">
						      <input type="text" class="form-control" name="txtfind" placeholder="Looking for ....">
						      <span class="input-group-btn">
						        <select class="btn-success" name="district">
											  <option value="none">District</option>
							<option value="Colombo">Colombo</option>
							<option value="Kandy">Kandy</option>
							<option value="Galle">Galle</option>
							<option value="Ampara">Ampara</option>
							<option value="Anuradhapura">Anuradhapura</option>
							<option value="Badulla">Badulla</option>
							<option value="Batticaloa">Batticaloa</option>
							<option value="Gampaha">Gampaha</option>
							<option value="Hambantota">Hambantota</option>
							<option value="Jaffna">Jaffna</option>
							<option value="Kalutara">Kalutara</option>
							<option value="Kegalle">Kegalle</option>
							<option value="Kilinochchi">Kilinochchi</option>
							<option value="Kurunegala">Kurunegala</option>
							<option value="Mannar">Mannar</option>
							<option value="Matale">Matale</option>
							<option value="Matara">Matara</option>
							<option value="Moneragala">Moneragala</option>
							<option value="Mullativu">Mullativu</option>
							<option value="Nuwara Eliya">Nuwara Eliya</option>
							<option value="Polonnaruwa">Polonnaruwa</option>
							<option value="Puttalam">Puttalam</option>
							<option value="Ratnapura">Ratnapura</option>
							<option value="Trincomalee">Trincomalee</option>
							<option value="Vavuniya">Vavuniya</option>
											</select>
						      </span>
						      <span class="input-group-btn">
						        <button class="btn btn-success" type="submit" name="btfind">Search</button>
						      </span>
						    </div>
                             </div>
                             </form>
                    </div>
				</div>
	</div>
	
	 
	</div>
	<footer class="mainFooter">
	<p>	Copyright &copy; locator.com </p>
	</footer>

  <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.js"></script>
</body>	
	
</html>