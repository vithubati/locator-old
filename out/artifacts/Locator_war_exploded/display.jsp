<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Locator</title>
    <link rel="icon" type="image/png" href="img/favicon-loc.ico" />
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/main.css" type="text/css" />
     <link rel="stylesheet" href="css/samp.css" type="text/css" />
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
<body>
<div class="body">
  <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="home">Locator</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse" aria-expanded="true">
          <form class="navbar-form navbar-right search" action="find" method="post">
            <div class="form-group">
              <input type="text" placeholder="Look for" class="form-control findTextBox" name="txtfind">
            </div>
            <div class="form-group">
              <select class="form-control" name="district">
              <option value="none">District</option>
							<option value="Colombo">Colombo</option>
							<option value="Kandy">Kandy</option>
							<option value="Galle">Galle</option>
							<option value="Ampara">Ampara</option>
							<option value="Anuradhapura">Anuradhapura</option>
							<option value="Badulla">Badulla</option>
							<option value="Batticaloa">Batticaloa</option>
							<option value="Gampaha">Gampaha</option>
							<option value="Hambantota">Hambantota</option>
							<option value="Jaffna">Jaffna</option>
							<option value="Kalutara">Kalutara</option>
							<option value="Kegalle">Kegalle</option>
							<option value="Kilinochchi">Kilinochchi</option>
							<option value="Kurunegala">Kurunegala</option>
							<option value="Mannar">Mannar</option>
							<option value="Matale">Matale</option>
							<option value="Matara">Matara</option>
							<option value="Moneragala">Moneragala</option>
							<option value="Mullativu">Mullativu</option>
							<option value="Nuwara Eliya">Nuwara Eliya</option>
							<option value="Polonnaruwa">Polonnaruwa</option>
							<option value="Puttalam">Puttalam</option>
							<option value="Ratnapura">Ratnapura</option>
							<option value="Trincomalee">Trincomalee</option>
							<option value="Vavuniya">Vavuniya</option>
              </select>
            </div>
            <button type="submit" class="btn btn-success form-control" name="btfind">Find</button>
            <div class="form-group">
            <a href="YourPlace" class="hlink">Your Shop</a>
            </div>
            <div class="form-group">
            <a href="ContactUs" class="hlink">Contact Us</a>
            </div>
          </form>
        </div><!--/.navbar-collapse -->
      </div>
    </nav>
<div class="mainContent">	
	<aside class="left-sidebar">
		<article><h3>Destricts</h3>
		<div class="destrics"><ul>	
						<li><a href="find?destrict=Colombo">Colombo</a></li>
						<li><a href="find?destrict=Kandy">Kandy</a></li>
						<li><a href="find?destrict=Galle">Galle</a></li>
						<li><a href="find?destrict=Ampara">Ampara</a></li>
						<li><a href="find?destrict=Anuradhapura">Anuradhapura</a></li>
						<li><a href="find?destrict=Badulla">Badulla</a></li>
						<li><a href="find?destrict=Batticaloa">Batticaloa</a></li>
						<li><a href="find?destrict=Gampaha">Gampaha</a></li>
						<li><a href="find?destrict=Hambantota">Hambantota</a></li>
						<li><a href="find?destrict=Jaffna">Jaffna</a></li>
						<li><a href="find?destrict=Kalutara">Kalutara</a></li>
						<li><a href="find?destrict=Kegalle">Kegalle</a></li>
						<li><a href="find?destrict=Kilinochchi">Kilinochchi</a></li>
						<li><a href="find?destrict=Kurunegala">Kurunegala</a></li>
						<li><a href="find?destrict=Mannar">Mannar</a></li>
						<li><a href="find?destrict=Matale">Matale</a></li>
						<li><a href="find?destrict=Matara">Matara</a></li>
						<li><a href="find?destrict=Moneragala">Moneragala</a></li>
						<li><a href="find?destrict=Mullativu">Mullativu</a></li>
						<li><a href="find?destrict=Nuwara Eliya">Nuwara Eliya</a></li>
						<li><a href="find?destrict=Polonnaruwa">Polonnaruwa</a></li>
						<li><a href="find?destrict=Puttalam">Puttalam</a></li>
						<li><a href="find?destrict=Ratnapura">Ratnapura</a></li>
						<li><a href="find?destrict=Trincomalee">Trincomalee</a></li>
						<li><a href="find?destrict=Vavuniya">Vavuniya</a></li>
			</ul></div>
		</article>
	</aside>

	<div class="content">
			<article class="topcontent">
				<header>
					<span class="subtitle">Search Result</span>
				</header>

			</article>
			<c:if test="${Error eq 'noData'}">
			<article class="topcontent">
				0 Data Found for '${currentKey}'!
			</article>
			</c:if>
			<c:if test="${Error eq 'error'}">
			<article class="topcontent">
				Something went wrong. We apologize for the inconvenience!
			</article>
			</c:if>
			<c:if test="${Bad eq 'BadRequest'}">
			<article class="topcontent">
				Bad Request, invalid Input
			</article>
			</c:if>
			<c:forEach var="institute" items="${instituteList}">
	         <article class="topcontent">
			 <div class="row"><div class="col-xs-10 col-md-10 col-lg-10">
				<a href="view/${institute.link}"><span class="shopname">${institute.nme} </span> </a>
			</div></div>
			<div class="row"><div class="col-xs-10 col-md-10 col-lg-10">
				${institute.no} &nbsp;${institute.street} &nbsp;${institute.city} &nbsp;${institute.district} 
			</div>
			</div>
			<div class="row"><div class="col-xs-3 col-md-6 col-lg-2">
				<span class="addresstitle" >Tele</span></div>
				<div class="col-xs-9 col-md-6 col-lg-4">${institute.tele1}</div>
			</div>
	          <c:if test="${institute.tele2 ne '0'}">
	          <div class="row"><div class="col-xs-3 col-md-6 col-lg-2">
				<span class="addresstitle" >Tele-2</span></div>
				<div class="col-xs-9 col-md-6 col-lg-4">${institute.tele2}</div>
			</div>
	           </c:if>
	            <c:if test="${institute.fax ne '0'}">
	            <div class="row"><div class="col-xs-3 col-md-6 col-lg-2">
				<span class="addresstitle" >Fax</span></div>
				<div class="col-xs-9 col-md-6 col-lg-4">${institute.fax}</div>
				</div>
	            </c:if>
				<c:if test="${institute.shpemail ne ''}">
				<div class="row"><div class="col-xs-3 col-md-6 col-lg-2">
				<span class="addresstitle" >Email</span></div>
				<div class="col-xs-9 col-md-6 col-lg-4">${institute.shpemail}</div>
				</div>
	            </c:if>
	            <c:if test="${institute.website ne ''}">
	            <div class="row"><div class="col-xs-3 col-md-6 col-lg-2">
				<span class="addresstitle" >Website</span></div>
				<div class="col-xs-9 col-md-6 col-lg-4">${institute.website}</div>
				</div>
				</c:if>
				<div class="row"><div class="col-xs-3 col-md-6 col-lg-2">
				<span class="addresstitle" >About</span></div>
				<div class="col-xs-9 col-md-6 col-lg-10" style="text-align: justify;">${institute.descript}</div>
				</div>				 
				</article>
				</c:forEach>
				 <%-- <p>${currentPage} and ${noOfPages}</p> --%>
				<div class="topcontent">
					<%--For displaying Previous link except for the 1st page --%>
					    <c:if test="${currentPage > 1}">
					       <a href="find?page=${currentPage - 1}&destrict=${currentDist}&keyword=${currentKey}" class="page">Previous</a>
					    </c:if>
					    <%--For displaying Page numbers. 
					    The when condition does not display a link for the current page--%>
					    <c:set var="p" value="${currentPage}" /> <%-- current page (1-based) --%>
						<c:set var="l" value="10" /> <%-- amount of page links to be displayed --%>
						<c:set var="r" value="${l / 2}" /> <%-- minimum link range ahead/behind --%>
						<c:set var="t" value="${noOfPages}" /> <%-- total amount of pages --%>
						<c:set var="begin" value="${((p - r) > 0 ? ((p - r) < (t - l + 1) ? (p - r) : (t - l)) : 0) + 1}" />
						<c:set var="end" value="${(p + r) < t ? ((p + r) > l ? (p + r) : l) : t}" />
						<%-- <p>${begin} and ${end}</p> --%>
						<c:set var="begins" value="1" />
						<c:set var="ends" value="10"/>
						<c:choose>
						<c:when test="${noOfPages lt l}">
								<c:forEach begin="${begins}" end="${noOfPages}" var="i">
					                <c:choose>
					                    <c:when test="${currentPage eq i}">
					                        <span class="page active">${i}</span>
					                    </c:when>
					                    <c:otherwise>
					                        <a href="find?page=${i}&destrict=${currentDist}&keyword=${currentKey}" class="page">${i}</a>
					                    </c:otherwise>
					                </c:choose>
					            </c:forEach>
						</c:when>
						<c:otherwise>
					            <c:forEach begin="${begin}" end="${end}" var="i">
					                <c:choose>
					                    <c:when test="${currentPage eq i}">
					                        <span class="page active">${i}</span>
					                    </c:when>
					                    <c:otherwise>
					                        <a href="find?page=${i}&destrict=${currentDist}&keyword=${currentKey}" class="page">${i}</a>
					                    </c:otherwise>
					                </c:choose>
					            </c:forEach>
					    </c:otherwise> 	
					    </c:choose>				     
					    <%--For displaying Next link --%>
					    <c:if test="${currentPage lt noOfPages}">
					    	 <%-- <a href="find?page=${noOfPages}&destrict=${currentDist}&keyword=${currentKey}" class="page">${noOfPages}</a> --%>
					    	 <a href="find?page=${currentPage + 1}&destrict=${currentDist}&keyword=${currentKey}" class="page" >Next</a>
					    </c:if>
					    
					   
			</div>
		
						
	</div>
</div>
</div>
<footer class="mainFooter">
	<p>	Copyright &copy; locator.com </p>
	</footer>
  <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script type="text/javascript" src="js/function.js"></script>	
</body>	
	
</html>