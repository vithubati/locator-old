<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="img/favicon-loc.ico" />
    <title>Confirmation</title>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/main.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="css/register.css" />
    <link rel="stylesheet" href="css/samp.css" type="text/css" />
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
<body>
<div class="body">
  <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="home">Locator</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse" aria-expanded="true">
          <form class="navbar-form navbar-right search" action="find" method="post">
            <div class="form-group">
              <input type="text" placeholder="Look for" class="form-control findTextBox" name="txtfind" >
            </div>
            <div class="form-group">
              <select class="form-control" name="district">
              <option value="none">District</option>
							<option value="Colombo">Colombo</option>
							<option value="Kandy">Kandy</option>
							<option value="Galle">Galle</option>
							<option value="Ampara">Ampara</option>
							<option value="Anuradhapura">Anuradhapura</option>
							<option value="Badulla">Badulla</option>
							<option value="Batticaloa">Batticaloa</option>
							<option value="Gampaha">Gampaha</option>
							<option value="Hambantota">Hambantota</option>
							<option value="Jaffna">Jaffna</option>
							<option value="Kalutara">Kalutara</option>
							<option value="Kegalle">Kegalle</option>
							<option value="Kilinochchi">Kilinochchi</option>
							<option value="Kurunegala">Kurunegala</option>
							<option value="Mannar">Mannar</option>
							<option value="Matale">Matale</option>
							<option value="Matara">Matara</option>
							<option value="Moneragala">Moneragala</option>
							<option value="Mullativu">Mullativu</option>
							<option value="Nuwara Eliya">Nuwara Eliya</option>
							<option value="Polonnaruwa">Polonnaruwa</option>
							<option value="Puttalam">Puttalam</option>
							<option value="Ratnapura">Ratnapura</option>
							<option value="Trincomalee">Trincomalee</option>
							<option value="Vavuniya">Vavuniya</option>
              </select>
            </div>
            <button type="submit" class="btn btn-success form-control" name="btfind">Find</button>
            <div class="form-group">
            <a href="YourPlace" class="hlink">Your Shop</a>
            </div>
            <div class="form-group">
            <a href="ContactUs" class="hlink">Contact Us</a>
            </div>
          </form>
        </div><!--/.navbar-collapse -->
      </div>
    </nav>
    
		<div class="mainContent">
			<div id="msform">
				<!-- progressbar -->
				<ul id="progressbar">
					<li class="active">Fill in details</li>
					<li class="active">Verify your Ad</li>
					<li class="active">Confirmation</li>
				</ul>
				<!-- fieldsets -->
				<fieldset>
					<h2 class="fs-title">Confirmation</h2>
					<h3 class="fs-subtitle"></h3>
					<div class="center-col">
					<article class="topcontent effect6" style="text-align: center;">
					<c:if test="${flagadd eq 'success'}">
						
						<h3 class="text-success">Your institution ad is successfully submitted for
							review</h3>
						It may take a few hours for your institution ad to be published across the site as it undergoes a routine quality control review.
						Once the institution ad details have been reviewed, you will receive an
email with a link. You can keep track of your place through the Link.
						<c:remove var="flagadd" scope="session" />
					</c:if>
					<c:if test="${flagadd eq 'error'}">
						<h3 class="text-warning">There was an issue posting your place!</h3>
									We apologize for the inconvenience. Please Try again or Contact Customer Service
					<c:remove var="flagadd" scope="session" />
					</c:if>
					<c:if test="${flagadd eq 'emailError'}">
					<h3 class="text-warning">There was an issue posting your place!</h3>
									We apologize for the inconvenience. Please Try again or Contact Customer Service
					<c:remove var="flagadd" scope="session" />
					</c:if>
					<c:if test="${flagadd eq 'insertError'}">
					<h3 class="text-warning">There was an issue posting your place!</h3>
									We apologize for the inconvenience. Please Try again or Contact Customer Service
					<c:remove var="flagadd" scope="session" />
					</c:if>
					</article>
					</div>
				</fieldset>
			</div>
		</div>
		</div>
	<!-- clearfix -->
	<footer class="mainFooter">
	<p>	Copyright &copy; locator.com </p>
	</footer>
	<!-- jQuery -->
	<!-- <script src="http://thecodeplayer.com/uploads/js/jquery-1.9.1.min.js"
		type="text/javascript"></script>
	jQuery easing plugin
	<script src="http://thecodeplayer.com/uploads/js/jquery.easing.min.js"
		type="text/javascript"></script> -->
		<script src="js/jquery.min.js"></script>
	<script src="js/register.js" type="text/javascript"></script>
    <script src="js/bootstrap.js"></script>

</body>
</html>