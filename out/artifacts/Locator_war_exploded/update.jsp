<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="img/favicon-loc.ico" />
    <title>Update</title>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="css/main.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="css/samp.css">
    <link rel="stylesheet" type="text/css" href="css/view.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	

</head>
<body>
<div class="body">
  <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="home">Locator</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse" aria-expanded="true">
          <form class="navbar-form navbar-right search" action="find" method="post">
            <div class="form-group">
             <input type="text" placeholder="Look for" class="form-control findTextBox" name="txtfind" >
            </div>
            <div class="form-group">
              <select class="form-control" name="district">
              <option value="none">District</option>
							<option value="Colombo">Colombo</option>
							<option value="Kandy">Kandy</option>
							<option value="Galle">Galle</option>
							<option value="Ampara">Ampara</option>
							<option value="Anuradhapura">Anuradhapura</option>
							<option value="Badulla">Badulla</option>
							<option value="Batticaloa">Batticaloa</option>
							<option value="Gampaha">Gampaha</option>
							<option value="Hambantota">Hambantota</option>
							<option value="Jaffna">Jaffna</option>
							<option value="Kalutara">Kalutara</option>
							<option value="Kegalle">Kegalle</option>
							<option value="Kilinochchi">Kilinochchi</option>
							<option value="Kurunegala">Kurunegala</option>
							<option value="Mannar">Mannar</option>
							<option value="Matale">Matale</option>
							<option value="Matara">Matara</option>
							<option value="Moneragala">Moneragala</option>
							<option value="Mullativu">Mullativu</option>
							<option value="Nuwara Eliya">Nuwara Eliya</option>
							<option value="Polonnaruwa">Polonnaruwa</option>
							<option value="Puttalam">Puttalam</option>
							<option value="Ratnapura">Ratnapura</option>
							<option value="Trincomalee">Trincomalee</option>
							<option value="Vavuniya">Vavuniya</option>
              </select>
            </div>
            <button type="submit" class="btn btn-success form-control" name="btfind">Find</button>
            <div class="form-group">
            <a href="YourPlace" class="hlink">Your Shop</a>
            </div>
            <div class="form-group">
            <a href="ContactUs" class="hlink">Contact Us</a>
            </div>
          </form>
        </div><!--/.navbar-collapse -->
      </div>
    </nav>
<!--Nav bar end  -->
<!--  allmainContent">
		<div class="allcontent">
			<article class="alltopContent">-->

<div class="center-col">
<c:if test="${validUpd eq 'false'}">
			<article class="topcontent">
				No Data Found
			</article>
</c:if>	
<c:forEach var="institute" items="${instiList}">
<article class="topcontent effect6">
	<form action="yourshop" method="post" class="form-horizontal" name="regForm2" id="regForm2" enctype="multipart/form-data">
	<div id="ribbon-container"> <span style="text-align: center;">Update Your info</span></div>
<!-- Left  -->
<div class="col-md-6" style="padding-top: 2%">
<div class="form-group required">
    <label for="inputEmail3" class="control-label col-md-3">Name</label>
    <div class="col-sm-6">
      <input type="text" class="form-control " id="shopName" name="shopName" value="${institute.nme}" placeholder="">
    </div>
</div>
<div class="form-group required">
    <label for="inputEmail3" class="control-label col-md-3">Category</label>
    <div class="col-sm-6">
    <select class="form-control " id="category" name="category">
    	<option value="${institute.catgry}">${institute.catgry}</option>
							<optgroup label="Arts and Crafts">
												<option value="Antiques Shops">Antiques Shop</option>
											</optgroup>
											<optgroup label="Books & Stationaries">
												<option value="Library">Library</option>
												<option value="Novelty Store">Novelty Store</option>
												<option value="Stationary Shop">Stationary Shop</option>
											</optgroup>
											<optgroup label="Clothing & Fashion Shops">
												<option value="Clothing and Designer Wear Shop">Clothing and Designer Wear Shop</option>
												<option value="Jewelry shop">Jewelry shop</option>
												<option value="Launderette">Launderette</option>
												<option value="Shoe Shop">Shoe Shop</option>
												<option value="Watch Shop">Watch Shop</option>	
											</optgroup>
											<optgroup label="Computing & Electronics">
												<option value="Camera and Camcorder Shop">Camera and Camcorder Shop</option>
												<option value="Computer Accessories and Repair Shop">Computer Accessories and Repair Shop</option>
												<option value="Cellphones and Accessories Shop">Cellphones and Accessories Shop</option>
												<option value="Home Electronics Shop">Home Electronics Shop</option>
												<option value="Laptop, Desktop and Tablet Shop">Laptop, Desktop and Tablet Shop</option>
												<option value="TV, Audio and Surveillance Shop">TV, Audio and Surveillance Shop</option>
												<option value="Video games and Consoles Shop">Video games and Consoles Shop</option>
											</optgroup>
											<optgroup label="Groceries & Retails Shops">
												<option value="Bakery">Bakery</option>
												<option value="Butcher">Butcher</option>
												<option value="Corner Shop / Convenience Shop">Corner Shop / Convenience Shop</option>
												<option value="Department Store">Department Store</option>
												<option value="Fishmonger">Fishmonger</option>
												<option value="Florist">Florist</option>
												<option value="Gift Shop">Gift Shop</option>
												<option value="Liquor Shop">Liquor Shop</option>
												<option value="Super Market">Super Market</option>
											</optgroup>
											<optgroup label="Education">
												<option value="Campus">Campus</option>
												<option value="College">College</option>
												<option value="Institute">Institute</option>
												<option value="Montessori">Montessori</option>
												<option value="Tuition">Tuition</option>
											</optgroup>
											<optgroup label="Health & Beauty">
												<option value="Beauty Saloon / Barber Shop">Beauty Saloon / Barber Shop</option>
												<option value="Gym and Fitness center">Gym and Fitness center</option>
												<option value="Hairdresser">Hairdresser</option>
												<option value="Hospital">Hospital</option>
												<option value="Medical">Medical</option>
												<option value="Pharmacy / Chemist">Pharmacy / Chemist</option>
												<option value="Sports Shop">Sports Shop</option>
											</optgroup>
											<optgroup label="Personal Finance">
												<option value="Bank">Bank</option>
												<option value="Insurance Company">Insurance Company</option>
												<option value="Money Transfer">Money Transfer</option>
												<option value="Pawning Center">Pawning Center</option>
											</optgroup>
											<optgroup label="Others">
												<option value="Communication">Communication</option>
												<option value="Furniture Shop">Furniture Shop</option>
												<option value="Garage">Garage</option>
												<option value="Gas Station">Gas Station</option>
												<option value="Hardware Shop">Hardware Shop</option>
												<option value="Hotels">Hotels</option>
												<option value="Pet Shop">Pet Shop</option>
												<option value="Restaurant">Restaurant</option>
												<option value="Service Center">Service Center</option>
												<option value="Theatres">Theatres</option>
												<option value="Toy Store">Toy Store</option>
												<option value="Travel Agent">Travel Agent</option>
												<option value="Vehicle Shop">Vehicle Shop</option>
												<option value="Video Rental Shop">Video Rental Shop</option>
											</optgroup>
    </select>
    </div>
</div>
<div class="form-group  required">
    <label for="inputEmail3" class="control-label col-md-3">Type</label>
    <div class="col-sm-6">
     <select class="form-control " id="type" name="type">
     					<option value="${institute.type}">${institute.type}</option>
						<option>On-line</option>
						<option>Physical</option>
					</select> 
    </div>
</div>
    <label for="inputEmail3" class="control-label">Address</label>
    <div class="effect8">
       <div class="form-group required">
    <label for="inputEmail3" class="control-label col-md-3">No</label>
    <div class="col-sm-6">
      <input type="text" class="form-control " name="no" id="no" value="${institute.no}" placeholder="">
    </div>
    </div>
       <div class="form-group required">
    <label for="inputEmail3" class="control-label col-md-3">Street</label>
    <div class="col-sm-6">
      <input type="text" class="form-control " name="street" id="street" value="${institute.street}" placeholder="">
    </div>
    </div>
       <div class="form-group required">
    <label for="inputEmail3" class="control-label col-md-3">District</label>
    <div class="col-sm-6">
      <select id="dists" name="dists" class="form-control ">
							<option value="${institute.district}">${institute.district}</option>
							<option value="Colombo">Colombo</option>
							<option value="Kandy">Kandy</option>
							<option value="Galle">Galle</option>
							<option value="Ampara">Ampara</option>
							<option value="Anuradhapura">Anuradhapura</option>
							<option value="Badulla">Badulla</option>
							<option value="Batticaloa">Batticaloa</option>
							<option value="Gampaha">Gampaha</option>
							<option value="Hambantota">Hambantota</option>
							<option value="Jaffna">Jaffna</option>
							<option value="Kalutara">Kalutara</option>
							<option value="Kegalle">Kegalle</option>
							<option value="Kilinochchi">Kilinochchi</option>
							<option value="Kurunegala">Kurunegala</option>
							<option value="Mannar">Mannar</option>
							<option value="Matale">Matale</option>
							<option value="Matara">Matara</option>
							<option value="Moneragala">Moneragala</option>
							<option value="Mullativu">Mullativu</option>
							<option value="Nuwara Eliya">Nuwara Eliya</option>
							<option value="Polonnaruwa">Polonnaruwa</option>
							<option value="Puttalam">Puttalam</option>
							<option value="Ratnapura">Ratnapura</option>
							<option value="Trincomalee">Trincomalee</option>
							<option value="Vavuniya">Vavuniya</option>
						</select>
    </div>
    </div>
       <div class="form-group required">
    <label for="inputEmail3" class="control-label col-md-3">City / Area</label>
    <div class="col-sm-6">
      <select class="form-control "	name="city" id="city">
      <option value="${institute.city}">${institute.city}</option>
      	</select>
    </div>
    </div>
    </div>
    <div class="form-group required">

    <label for="inputEmail3" class="control-label col-md-3">Description</label>

    <div class="col-sm-6">
      <textarea class="form-control" rows="7" name="description" id="description" >${institute.descript}</textarea>
    </div>
    </div>
 </div>
  <!--Left-End  -->
  
  <div class="form-group col-md-6">
  <label for="inputEmail3" class="control-label">Contects</label>
  <div class="effect8">
   <div class="form-group required">
    <label for="inputEmail3" class="control-label col-md-3">Tele 1</label>
    <div class="col-sm-6">
      <input type="text" class="form-control " name="telephone1" id="telephone1" value="${institute.tele1}" placeholder="">
    </div>
    </div>
       <div class="form-group">
    <label for="inputEmail3"class="control-label col-md-3">Tele 2</label>
    <div class="col-sm-6">
      <input type="text" class="form-control " name="telephone2" id="telephone2" value="${institute.tele2}" placeholder="">
    </div>
    </div>
       <div class="form-group">
    <label for="inputEmail3" class="control-label col-md-3">Fax</label>
    <div class="col-sm-6">
      <input type="text" class="form-control " name="fax" id="fax" value="${institute.fax}" placeholder="">
    </div>
    </div>
       <div class="form-group">
    <label for="inputEmail3" class="control-label col-md-3">Email</label>
    <div class="col-sm-6">
      <input type="text" class="form-control " name="shopEmail" id="shopEmail" value="${institute.shpemail}" placeholder="">
    </div>
    </div>
       <div class="form-group">
    <label for="inputEmail3" class="control-label col-md-3">Website</label>
    <div class="col-sm-6">
      <input type="text" class="form-control " name="website" id="website" value="${institute.website}" placeholder="">
    </div>
    </div>
    </div>
      <div class="form-group">
    <label for="inputEmail3" class="control-label col-md-3">Photo</label>
    <div class="col-sm-6">
      <input type="file" id="photo" class="" name="photo">
    </div>
    </div>
    <div class="form-group">
     <span class="control-label col-md-6 text-primary">
     Note: If you upload a new photo. it will ovewrite the previous photo.
      Therefore the previous photo will be deleted 
      </span>
    <div class="col-sm-6">   
    <c:forEach var="institut" items="${instiList}">
    <c:if test="${empty institut.photoPath}">
      <img src="img/logo2.gif" style="width:100%" class="img-thumbnail">
     </c:if>
    <c:if test="${not empty institut.photoPath}">
      <img src="/Locator/images/${institute.photoPath}" style="width:100%" class="img-thumbnail">
     </c:if>
     </c:forEach>
    </div>
    </div>
     <div class="form-group required">
    <label for="inputEmail3" class="control-label col-md-3">Email</label>
    <div class="col-sm-6">
      <input type="text" class="form-control" name="email" id="email" value="${institute.mail}" placeholder="">
    </div>
    </div>
   <!-- <div class="form-group required">
    <label for="inputEmail3" class="control-label col-md-3">Password</label>
    <div class="col-sm-6">
      <input type="password" class="form-control " name="password" id="password" placeholder="">
    </div>
    </div>
       <div class="form-group required">
    <label for="inputEmail3" class="control-label col-md-3">Confirm Password</label>
    <div class="col-sm-6">
      <input type="password" class="form-control " name="cpassword" id="cpassword" placeholder="">
    </div>
    </div> -->
    <div class="form-group text-center" >
		<span id="errormsg" class="text-danger"></span>
	</div>
  </div>
  <!-- right end -->
 <div class="form-group">
 	 <div class="col-sm-6 col-md-6 col-md-offset-6">
      	<button type="submit" class="btn btn-success pull-right" name="update" id="update" value="update">Submit</button>
    </div>
 </div>
	<input type="hidden" name="s_url" value="${link}">
</form>
</article>
</c:forEach>
</div>
</div>
	<footer class="mainFooter">
	<p>	Copyright &copy; locator.com </p>
	</footer>

<!-- jQuery -->
	<script src="js/jquery-1.9.1.min.js" type="text/javascript"></script>
	<!-- jQuery easing plugin -->
	<script src="js/jquery.easing.min.js" type="text/javascript"></script>
		<script type="text/javascript" src="js/function.js"></script>	
	<script src="js/upd.js" type="text/javascript"></script>
	<script src="js/location.js" type="text/javascript"></script>
<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

	<script type="text/javascript" src="js/function.js"></script>
</body>
</html>