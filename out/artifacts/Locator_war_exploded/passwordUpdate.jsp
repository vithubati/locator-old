<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.locator.model.Capture"%>
<%@page import="java.sql.ResultSet"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Password Update</title>
    <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/img/favicon-loc.ico" />
    <!-- Bootstrap core CSS -->
    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/main.css" type="text/css" />
     <link rel="stylesheet" href="${pageContext.request.contextPath}/css/view.css" type="text/css" />
     <link rel="stylesheet" href="${pageContext.request.contextPath}/css/common.css" type="text/css" />
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <c:set var="root" value="${pageContext.request.contextPath}" />	
  </head>
  <body>
<div class="body">
  <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="home">Locator</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse" aria-expanded="true">
          <form class="navbar-form navbar-right search" action="${pageContext.request.contextPath}/find" method="post">
            <div class="form-group">
              <input type="text" placeholder="Look for" class="form-control findTextBox" name="txtfind" >
            </div>
            <div class="form-group">
              <select class="form-control" name="district">
              <option value="none">District</option>
							<option value="Colombo">Colombo</option>
							<option value="Kandy">Kandy</option>
							<option value="Galle">Galle</option>
							<option value="Ampara">Ampara</option>
							<option value="Anuradhapura">Anuradhapura</option>
							<option value="Badulla">Badulla</option>
							<option value="Batticaloa">Batticaloa</option>
							<option value="Gampaha">Gampaha</option>
							<option value="Hambantota">Hambantota</option>
							<option value="Jaffna">Jaffna</option>
							<option value="Kalutara">Kalutara</option>
							<option value="Kegalle">Kegalle</option>
							<option value="Kilinochchi">Kilinochchi</option>
							<option value="Kurunegala">Kurunegala</option>
							<option value="Mannar">Mannar</option>
							<option value="Matale">Matale</option>
							<option value="Matara">Matara</option>
							<option value="Moneragala">Moneragala</option>
							<option value="Mullativu">Mullativu</option>
							<option value="Nuwara Eliya">Nuwara Eliya</option>
							<option value="Polonnaruwa">Polonnaruwa</option>
							<option value="Puttalam">Puttalam</option>
							<option value="Ratnapura">Ratnapura</option>
							<option value="Trincomalee">Trincomalee</option>
							<option value="Vavuniya">Vavuniya</option>
              </select>
            </div>
            <button type="submit" class="btn btn-success form-control" name="btfind">Find</button>
            <div class="form-group">
            <a href="${pageContext.request.contextPath}/YourPlace" class="hlink">Your Shop</a>
            </div>
            <div class="form-group">
            <a href="${pageContext.request.contextPath}/ContactUs" class="hlink">Contact Us</a>
            </div>
          </form>
        </div><!--/.navbar-collapse -->
      </div>
    </nav>
	
	
<div class="mainContent">
<div class="topcontent-rds-less">
		<c:if test="${resetError eq 'badRequest'}">
				<p class="well-lg bg-danger text-center text-danger">Bad Request. The request had bad syntax or was impossible to be satisified.!</p>
			</c:if>
		<c:if test="${resetError eq 'error'}">
				<p class="well-lg bg-danger text-center text-danger"> Some error occured while Requesting for Password Reset! Please try again! 
				We apologize for the inconvenience! </p>
		</c:if>
		<c:if test="${valid eq 'false'}">
				<p class="well-lg bg-danger text-center text-danger">Some error occured while Requesting for Password Reset! Please try again!</p>
			</c:if>
		<c:if test="${valid eq 'true'}">
			<article class="topcontent-rds-less">
				<div class="well-lg bg-success text-justify"><h2 class="headtg"><span class="glyphicon glyphicon-envelope"></span>
				 We have sent an email to ${rEmail}.</h2></div>
				<br>
				<p class="well-sm bg-success text-justify">
				Follow the instruction in the email to reset the password of your place.
				If you have not received an email, Please check your SPAM or JUNK mail folder.
				</p>			
			</article>
			</c:if>
			<c:if test="${passValidated eq 'success'}">
			<form class="form-horizontal" method="post" action="PasswordReset" name="contform" id="contform" onsubmit="return validatPassFields()">
                        <h3 class="" id="ribbon-container"><span>Reset Password</span></h3>
                        <div class="form-group">
                        <label for="inputEmail3" class="control-label col-md-3">New Password</label>
                            <div class="col-md-8">
                                <input id="npasstxt" name="npasstxt" type="password" placeholder="New Password" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                        <label for="inputEmail3" class="control-label col-md-3">Confirm Password</label>
                            <div class="col-md-8">
                                <input id="cnpasstxt" name="cnpasstxt" type="password" placeholder="Confirm Password" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 text-right">
                           		<span id="worning"></span>
                                <button type="submit" class="btn btn-success btn-default" id="resetbtn" name="resetbtn" value="submit">Submit</button>
                            </div>
                        </div>
                        <input type="hidden" value="${email}" name="acemail">
                        <input type="hidden" value="${url}" name="link">
                        <div class="form-group text-center" >
								<span id="errormsg" class="text-danger">${errorMessages.rstPass}</span>
						</div>
            </form>
			</c:if>
			<c:if test="${invalidToken eq 'true'}">
			<article class="topcontent-rds-less">
				<div class="bg-danger"><div class="well-lg text-center">
				<h4 class="text-danger">The Password Reset link is invalid or Expired. Please Try again.</h4> 
				</div></div>
			</article>
			</c:if>
			<c:if test="${passReset eq 'success'}">
			<article class="topcontent-rds-less">
				<div class="bg-success"><div class="well-lg text-center">
				<h3 class="text-success">Your Place Password has been reset successfully!</h3> 
				<h4 class="text-default">Go to Your <a href="view/${url}" style="color: #5CB85C;"><u>Place</u></a></h4>
				</div></div>
			</article>
			</c:if>
</div>
<form action="${root}/PasswordReset" method="post">
			<c:if test="${available eq 'false'}">
		
				<article class="topcontent">
					<div id="ribbon-container"><span>Password Reset for '${nme}'</span></div>
					<h3><span class="text-danger">The Email is incorrect!</span> </h3>
					<h4><span>Please Enter Registered Email of <span class="headtg">'${nme}'</span>.</span></h4>
					<span style="color: #FF6666">Account Email</span>	<br>
					<span><input type="text" name="txtemail" class="input-sm" ></span>
					<span> <input type="submit" id="forgotbtn" name="forgotbtn" class="btn btn-info deleteButton" value="Submit" /></span>
					<input type="hidden" name="url" value="${link}">
					<input type="hidden" name="name" value="${nme}">
				</article>
		</c:if>
		<c:if test="${validPsw eq 'false'}">		
				<article class="topcontent-rds-less">
					<div id="ribbon-container"><span>Password Reset for '${nme}'</span></div>
					<h3><span class="text-danger">${errorMessages.rEmail}</span></h3>
					<h4><span>Please Enter Registered Email of <span class="headtg">'${nme}'</span>.</span></h4>	
					<span style="color: #FF6666">Account Email</span>	<br>	
					<span><input type="text" name="txtemail" class="input-sm" value="${fn:escapeXml(param.txtemail)}"></span>
					<span> <input type="submit" id="forgotbtn" name="forgotbtn" class="btn btn-info forgButton" value="Submit" /></span>
					<input type="hidden" name="url" value="${link}">
					<input type="hidden" name="name" value="${nme}">
				</article>
		</c:if>		
</form>
	</div>
	
	</div>
	<footer class="mainFooter">
	<p>	Copyright &copy; locator.com </p>
	</footer>

  <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/validator.js"></script>
</body>	
	
</html>