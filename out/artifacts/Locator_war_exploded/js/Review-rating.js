
$(document).ready(function(){
		getReview(1);
		
		$("#btsave").click(function(){
			reviewBoxValidate();
		});
		$("#btclose").click(function(){
			hideDiv();
		});
		$("#btclosee").click(function(){
			hideDiv();
		});
		$("#btsamp").click(function(){
			getReview();
			
		});
	});
function getReview(pg) {
	pag = $("#currentPage").val(pg);
	$("#rviewPage").empty();
	$("#Pagination").empty();
	var pag = $("#currentPage").val();
	//alert(pag);
	var curURL = $("#url").val();
	var but = $("#pagiButPrev").val();
	var datas = {
		page : pag,	
		linkparam : curURL,
		butn : but
		};	
	$.post( "../review", datas,function( data ) { //changed json to post
			var rating, lnk, cmt, tmestmp, norcrd, nopge, curpge;
		  $.each( data, function( index, revw ) {
			  rating = revw.rating -1;
			  lnk = revw.url;
			  cmt = revw.coment;
			  tmestmp = revw.timestamp;
			  norcrd = revw.noOfRecords;
			  nopge = revw.noOfPages +1;
			  curpge = revw.curentPage;
			  $("#currentPage").val(curpge);
			  $("#noOfPages").val(nopge);
			 /* alert(rating +" "+ lnk +" "+ cmt +" "+ tmestmp +" "+ norcrd +" "+ nopge +" "+ curpge)*/
			  $("#rviewPage").prepend("<div class='col-lg-8' id='s1'/>");
			  $("#s1").append('<div class="topcontent-rds-less" id="s2"/>');
			  $("#s2").append('<div class="stars-small" data-rating="0" id="s3"/>');
			  for (var int = 0; int < 5; int++) {
				  if(int <= rating){
					  $("#s3").append("<span id='s4' class='glyphicon glyphicon-star'></span>");
				  }else{
					  $("#s3").append("<span id='s4' class='glyphicon glyphicon-star-empty'></span>");
				  }
				  
			}		  
			  $("#s2").append("<p id='s5'>" +cmt +"</p>");
			  $("#s2").append("<p>" +tmestmp +"</p>");
		  });
		 // pagination
		  if(curpge > 1){
			  //alert("hi");
			  $("#Pagination").append("<button class='page btn btn-default' type='submit' id='pagiButPrev' value='Save' name='pagiButPrev' onclick=getReview("+ (curpge-1) +")>Previous</button>");
		  }
		  var p = curpge; 
		  var l = 10; //  amount of page links to be displayed 
		  var r = l / 2; // minimum link range ahead/behind --%>
		  var t = nopge;  //total amount of pages 
		  var begin =((p - r) > 0 ? ((p - r) < (t - l + 1) ? (p - r) : (t - l)) : 0) + 1;
		  var end = (p + r) < t ? ((p + r) > l ? (p + r) : l) : t;
		  // alert(p + " " +l + " " + r + " " + t + " " +begin + " " + end);
		  var begins = 1;
		  var ends = 10;
		  if(nopge <l){
			  for (var i = begins; i< nopge; i++) {
				if(curpge == i){
					$("#Pagination").append("<span class='page active'>" + i + "</span>");
				}else{
					$("#Pagination").append("<button class='page btn btn-default' type='submit' id='pagiBut' value='Save' name='pagiBut' onclick=getReview("+ i +")>" + i +"</button>");
				}
			}
		  }else{
			  for (var i = begin; i< end; i++) {
				  if(curpge == i){
						$("#Pagination").append("<span class='page active btn btn-default disabled'><u>" + i + "</u></span>");
					}else{
						$("#Pagination").append("<button class='page btn btn-default' type='submit' id='pagiBut' value='Save' name='pagiBut' onclick=getReview("+ i +")>" + i +"</button>");
					}
			  }
		  }
		  //For displaying Next link
		  if (curpge < nopge-1) {
			  $("#Pagination").append("<button class='page btn btn-default' type='submit' id='pagiBut' value='Save' name='pagiBut' onclick=getReview("+ (curpge+1)  +")>Next</button>");
		}
	});
/*	request.done(function(msg) {
			alert("sucess");
		
	});

	request.fail(function(jqXHR, textStatus) {					
		showFail();
	});*/
}
/*function getNxtReview() {
	
	var pag = ${currentPage - 1};
	var curURL = $("#url").val();
	var rate = $("#ratings-hidden").val();
	var coment = $("#new-review").val();
	var ur = $("#url").val();
	var but = $("#btsave").val();
	var request = $.ajax({
		url : "../review",
		type : "POST",
		data : {
			page : pag,
			linkparam : curURL
		}
	});
	request.done(function(msg) {
		if (msg =="success") {
			showSuccess();
		}else if (msg =="fail"){
			showFail();
		}else if (msg =="error"){
			showFail();
		}
		
	});

	request.fail(function(jqXHR, textStatus) {					
		showFail();
	});
}*/
function saveReview() {
	var rate = $("#ratings-hidden").val();
	var coment = $("#new-review").val();
	var ur = $("#url").val();
	var but = $("#btsave").val();
	var request = $.ajax({
		url : "../review",
		type : "POST",
		data : {
			rat : rate,
			com : coment,
			link : ur,
			butn : but
		}
	});
	request.done(function(msg) {
		if (msg =="success") {
			showSuccess();
			$('.form-control').val('');
			//$("#ratings-hidden").val('');
			$('#rvwStar span').removeClass('glyphicon-star').addClass('glyphicon-star-empty');
			$('#worning').text('');
		}else if (msg =="fail"){
			showFail();
		}else if (msg =="error"){
			showFail();
		}
		
	});

	request.fail(function(jqXHR, textStatus) {					
		showFail();
	});
}

function showSuccess(){
	var succBut = $('#success');
	var postRvw = $('#post-review-box');
	var reviewBox = $('#open-review-box');
	reviewBox.show("slow");
	postRvw.hide();
	succBut.show("slow");
	
}
function hideDiv(){
	var succBut = $('#success');
	var failBut = $('#fail');
	succBut.slideUp(400);
	failBut.slideUp(400);
}
function showFail(){
	var failBut = $('#fail');
	var postRvw = $('#post-review-box');
	var reviewBox = $('#open-review-box');
	postRvw.hide();
	reviewBox.show("slow");
	failBut.show("slow");
}
function reviewBoxValidate(){
	$('.form-control').css("border-color","#66afe9"); 
	
	var boxtVal = $('#new-review').val();
	var rate = $("#ratings-hidden").val();
	if(boxtVal.length == ''){
		$('#worning').text("Enter Your comments!");
		$('#worning').css("color", "red");
		$('.form-control').css("border-color","red");
	}else if(rate == ''){
		$('#worning').text("Select Your Ratings!");
		$('#worning').css("color", "red");
	}else{
		saveReview();
	}
}




