
//jQuery time
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches

/* back to top*/
jQuery(document).ready(function(){
		//duration of the top scrolling animation (in ms)
		scroll_top_duration = 10,

	//grab the "back to top" link
	$back_to_top = $('#next');
	$back_to_top.on('click', function(event){
		event.preventDefault();
		$('body,html').animate({
			scrollTop: 10 ,
		 	}, scroll_top_duration
		);
	});
	
	$back_to_top = $('#previous');
	$back_to_top.on('click', function(event){
		event.preventDefault();
		$('body,html').animate({
			scrollTop: 11 ,
		 	}, scroll_top_duration
		);
	});
	
	$back_to_top2 = $('.submit');
	$back_to_top2.on('click', function(event){
		//event.preventDefault();
		$('body,html').animate({
			scrollTop: 5 ,
		 	}, scroll_top_duration
		);
	});
});
/*--------*/

$("#next").click(function(){

	var validd = validation();
	if(validd == true){
		
	if(animating) return false;
	animating = true;
	
	current_fs = $(this).parent().parent().parent();
	next_fs = $(this).parent().parent().parent().next();
	
	//activate next step on progressbar using the index of next_fs
	$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
	
	
	//hide the current fieldset with style
	current_fs.animate({opacity: 0}, {
		step: function(now, mx) {
			//as the opacity of current_fs reduces to 0 - stored in "now"
			//1. scale current_fs down to 80%
			scale = 1 - (1 - now) * 0.2;
			//2. bring next_fs from the right(50%)
			left = (now * 10)+"%";
			//3. increase opacity of next_fs to 1 as it moves in
			opacity = 1 - now;
			//current_fs.css({'transform': 'scale('+scale+')'});
			next_fs.css({'left': left, 'opacity': opacity});
		}, 
		duration: 500, 
		complete: function(){
			current_fs.hide();
			animating = false;
		}, 
		//this comes from the custom easing plugin
		easing: 'easeInOutBack'
	});
	
	}
	// fetching data
	var fields = new Array(
		$('#category').val(),
		$('#type').val(),
		$('#shopName').val(),
		$('#no').val() + ' ' + 
				$('#street').val() + '<br>' + 
				$('#city').val() + ' ' + 
				$('#dists').val(),
		$('#telephone1').val() + ' <br>' + 
		$('#telephone2').val() + ' <br>' + 
				$('#fax').val() + '<br> ' +
				$('#shopEmail').val(),
		$('#website').val(),
		$('#description').val());
		
	var tr = $('#fetch_step tr');	
	tr.each(function(){
		//alert( fields[$(this).index()] );
		//$(this).children('td:nth-child(2)').html(fields[$(this).index()]);
		if ($(this).children('td:nth-child(2)').attr("class") == "des"){
			//alert( fields[$(this).index()] );
			$(".ds").html(fields[$(this).index()]);
		}else{
			$(this).children('td:nth-child(2)').html(fields[$(this).index()]);
		} 
	});
		
	/**--------------*/
	//show the next fieldset
	next_fs.show(); 
	
});


 function validation(){
				$('#msform input').removeClass('error').removeClass('valid');
				$('#category').removeClass('error').removeClass('valid');
				$('#type').removeClass('error').removeClass('valid');
				$('#dists').removeClass('error').removeClass('valid');
				$('#errormsg').text("");
				 var fields = $('#msform input[type=text], input[type=password]');
				 var email = $('#msform input[id=email]');
				 var emailVal = $('#msform input[id=email]').val().trim();
				 var teleNo = $('#msform input[id=telephone1]');
				 var telNoVal = $('#msform input[id=telephone1]').val().trim();
				 var teleNo2 = $('#msform input[id=telephone2]');
				 var telNoVal2 = $('#msform input[id=telephone2]').val().trim();
				 var fax = $('#msform input[id=fax]');
				 var faxVal = $('#msform input[id=fax]').val().trim();;
				 var descrpt = $('#description');
				 var descrptVal = $('#description').val();
				 var semail = $('#msform input[id=shopEmail]');
				 var semailVal = $('#msform input[id=shopEmail]').val().trim();
				 var pswd = $('#msform input[id=password]');
				 var pswdVal = $('#msform input[id=password]').val().trim() ;
				 var cpswd = $('#msform input[id=cpassword]');
				 var cpswdVal = $('#msform input[id=cpassword]').val().trim();
				 var category = $("#category");
				 var categoryVal = $("#category").val();
				 var type = $("#type");
				 var typeVal = $("#type").val();
				 var dist = $("#dists");
				 var distVal = $("#dists").val();
				 var isValid = true;
				 var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
				 var Valid = true;
				 fields.each(function() {
				    if ($.trim($(this).val()) == '') {
				    	Valid = false;
						$(this).removeClass('valid');
						$(this).addClass('error');
						$('#errormsg').text("Please Fill Required(*) Fields!");
						if(this.id === 'fax' || this.id === 'telephone2' || this.id === 'shopEmail' || this.id === 'website'){
							$(this).removeClass('error');	
							Valid =true;
						}
					}
					else{
						$(this).removeClass('error');
						$(this).addClass('valid');
					}
				});
				 if (categoryVal == null) {
				    	$(category).removeClass("valid");
				    	$(category).addClass("error");
				    	Valid =false;
				 } 
				 if (typeVal == null) {
				    	$(type).removeClass("valid");
				    	$(type).addClass("error");
				    	Valid =false;
				 } 
				 if (distVal == null) {
				    	$(dist).removeClass("valid");
				    	$(dist).addClass("error");
				    	Valid =false;
				 }
				if(Valid){
					if ( pswdVal.length > 5) {
						if(pswdVal != cpswdVal){
							isValid = false;
							$(pswd).removeClass('valid');
							$(pswd).addClass('error');
							$(cpswd).removeClass('valid');
							$(cpswd).addClass('error');
							$('#errormsg').text("Passwords don't match!");
						}else{
							$(pswd).removeClass('error');
							$(pswd).addClass('valid');
							$(cpswd).removeClass('error');
							$(cpswd).addClass('valid');
						}
					}else{
						$('#errormsg').text("Password must contains atleast 6 characters!");
						isValid = false;
						$(pswd).removeClass('valid');
						$(pswd).addClass('error');
						$(cpswd).removeClass('valid');
						$(cpswd).addClass('error');
					}
					if (emailVal.match(filter) == null) {
						$('#errormsg').text("Invalid Email!");
						$(email).removeClass('valid');
						$(email).addClass('error');
						isValid = false;
					}else{
						$(email).removeClass('error');
						$(email).addClass('valid');
					}
					if(semailVal.length > 0){
						if ( semailVal.match(filter) == null) {
							isValid = false;
							$(semail).removeClass('valid');
							$(semail).addClass('error');
							$('#errormsg').text("Invalid Email!");
						}else{
							$(semail).removeClass('error');
							$(semail).addClass('valid');
						}					
					}
					if(telNoVal2.length > 0){
						if ( telNoVal2.length > 10 || telNoVal2.length < 9 || telNoVal2.match(/^[0-9]+$/) == null) {
							isValid = false;
							$(teleNo2).removeClass('valid');
							$(teleNo2).addClass('error');
							$('#errormsg').text("Telephone Number not valid!");
						}else{
							$(teleNo2).removeClass('error');
							$(teleNo2).addClass('valid');
						}					
					}
					if(faxVal.length > 0){
						if ( faxVal.length > 10 || faxVal.length < 9 || faxVal.match(/^[0-9]+$/) == null) {
							isValid = false;
							$(fax).removeClass('valid');
							$(fax).addClass('error');
							$('#errormsg').text("Fax number not valid!");
						}else{
							$(fax).removeClass('error');
							$(fax).addClass('valid');
						}					
					}
					if ( telNoVal.length > 10 || telNoVal.length < 9 || telNoVal.match(/^[0-9]+$/) == null) {
						isValid = false;
						$(teleNo).removeClass('valid');
						$(teleNo).addClass('error');
						$('#errormsg').text("Telephone Number not valid!");
					}else{
						$(teleNo).removeClass('error');
						$(teleNo).addClass('valid');
					}
					
					if ( descrptVal.length == '' ) {
						isValid = false;
						$('#errormsg').text("Please Fill Required(*) Fields!");
						$(descrpt).removeClass('valid');
						$(descrpt).addClass('error');
					}else{
						$(descrpt).removeClass('error');
						$(descrpt).addClass('valid');
					}
					
				}else{
					isValid=false;
					Valid = false;
				}
				if (isValid == false){
					return false;
				}
				if(Valid == false){
					return false;
				}
				else{ 
					$('#errormsg').text("");
					return true;
				}
};
				
$(".previous").click(function(){
	if(animating) return false;
	animating = true;
	
	current_fs = $(this).parent().parent().parent();
	previous_fs = $(this).parent().parent().parent().prev();
	
	//de-activate current step on progressbar
	$("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
	
	//show the previous fieldset
	
	//hide the current fieldset with style
	current_fs.animate({opacity: 0}, {
		step: function(now, mx) {
			//as the opacity of current_fs reduces to 0 - stored in "now"
			//1. scale previous_fs from 80% to 100%
			scale = 0.8 + (1 - now) * 0.2;
			//2. take current_fs to the right(50%) - from 0%
			left = ((1-now) * 20)+"%";
			//3. increase opacity of previous_fs to 1 as it moves in
			opacity = 2 - now;
			//current_fs.css({'left': left});
			previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
		}, 
		duration: 10, 
		complete: function(){
			current_fs.hide();
			animating = false;
		}, 
		//this comes from the custom easing plugin
		easing: 'easeInOutBack'
	});
	previous_fs.show(); 
});

$(".submit").click(function(){
	//var validd = validation();
	//if(validd == true){
		
	if(animating) return false;
	animating = true;
	
	current_fs = $(this).parent().parent().parent();
	next_fs = $(this).parent().parent().parent().next();
	
	//activate next step on progressbar using the index of next_fs
	$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
	
	
	//hide the current fieldset with style
	current_fs.animate({opacity: 0}, {
		step: function(now, mx) {
			//as the opacity of current_fs reduces to 0 - stored in "now"
			//1. scale current_fs down to 80%
			scale = 1 - (1 - now) * 0.2;
			//2. bring next_fs from the right(50%)
			left = (now * 20)+"%";
			//3. increase opacity of next_fs to 1 as it moves in
			opacity = 1 - now;
			//current_fs.css({'transform': 'scale('+scale+')'});
			next_fs.css({'left': left, 'opacity': opacity});
		}, 
		duration: 100, 
		complete: function(){
			current_fs.hide();
			animating = false;
		}, 
		//this comes from the custom easing plugin
		easing: 'easeInOutBack'
	});
	//show the next fieldset
	next_fs.show(); 
	
});
/*******************************************************************/
//charactor counts
var totallength = 1000;

$('textarea#description').on('keydown, keyup', function(e) {
	var left =(totallength -  $(this).val().length);
     $('span#character-count').text(left);
     if(left < 10)
    	 $('span#character-count').css("color", "red");
});
/*----------------*/


		