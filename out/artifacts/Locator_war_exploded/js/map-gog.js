/**
 * 
 */
jQuery(document).ready(function() {
  // find DOM elements
  var canvas = document.getElementById('map');
  var addressField = document.getElementById('fulladd');
  // create map, marker, infowindow and geocoder objects
  var options = {
    zoom: 16,  
    center: new google.maps.LatLng(7.873054, 80.771797),  
    mapTypeId: google.maps.MapTypeId.ROADMAP  
  };
  var map = new google.maps.Map(canvas, options);
  var marker = new google.maps.Marker({
    map: map
  });
  var infowindow = new google.maps.InfoWindow();
  var geocoder = new google.maps.Geocoder();
  // set handler for form.onsubmit event
  
   showAddressOnMap(addressField.value);
  
  
  // worker function to display marker on map at address
  function showAddressOnMap(address) {
    try{
      var geocoderRequest = {
        address: address
      }
      geocoder.geocode(geocoderRequest, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          var location = results[0].geometry.location;
          map.setCenter(location);
          marker.setPosition(location);
          var content = [];
          content.push('<strong>' + results[0].formatted_address + '</strong>');
          /*content.push('Lat: ' + location.lat());
          content.push('Lng: ' + location.lng());*/
          content.push('<a href="https://www.google.com/maps?saddr=My+Location&daddr=' +location.lat()+ ',' +location.lng() + '" target="_blank">Get Direction</a>');
          infowindow.setContent(content.join('<br/>'));
          infowindow.open(map, marker);
        }
      });
      return true;
    }
    catch(e){
      return false;//ensure form does not submit, even if there's an error
    }
  }
});