/**
 * 
 */
jQuery(document).ready(function(){
	$('#resetbtn').click(function () {
		validatPassFields();
	});
});
function validatPassFields(){  
	$('#npasstxt').removeClass('error').removeClass('valid');
	$('#cnpasstxt').removeClass('error').removeClass('valid');
	$('#errormsg').text("");
	var nPass = $('#npasstxt');
    var cPass = $("#cnpasstxt");
    var nPassVal = $('#npasstxt').val().trim();
    var cPassVal = $("#cnpasstxt").val().trim();
    var isValid = true;
    if (nPassVal == '') {
    	$(nPass).removeClass("valid");
    	$(nPass).addClass("error");
    	isValid =false;
	}
    if (cPassVal == '') {
    	$(cPass).removeClass("valid");
    	$(cPass).addClass("error");
    	isValid =false;
	}
    if ( nPassVal.length > 5) {
		if(cPassVal != nPassVal){
			isValid = false;
			$(nPass).removeClass('valid');
			$(nPass).addClass('error');
			$(cPass).removeClass('valid');
			$(cPass).addClass('error');
			$('#errormsg').text("Passwords don't match!");
		}else{
			$(nPass).removeClass('error');
			$(nPass).addClass('valid');
			$(cPass).removeClass('error');
			$(cPass).addClass('valid');
		}
	}else{
		$('#errormsg').text("Password must contains atleast 6 characters!");
		isValid = false;
		$(nPass).removeClass('valid');
		$(nPass).addClass('error');
		$(cPass).removeClass('valid');
		$(cPass).addClass('error');
	}
    if (isValid == false) 
		return false;
	else 
		return true;
}
