/**
 * 
 */
$(document).ready(function(){
	anim();
	getData();
	
});
function getData() {
	var ur = $("#url").val();
	$.ajax({
		type : "POST",
		url : "../counter",
		data : {
			url : ur
		}
	}).done(function(msg) {
		if (msg > 0) {
			$(".num").html(msg);
			$(".card__text").html(" People viewed this place");
		} else if (msg < 0) {
		} else if (msg == "fail") {
		}
	})
	.fail(function(jqXHR, textStatus) {	
	});
}

function anim() {

	  // cache vars
	  var cards = document.querySelectorAll(".card.effect__random");
	  var timeMin = 5;
	  var timeMax = 8;
	  var timeouts = [];

	  // loop through cards
	  for ( var i = 0, len = cards.length; i < len; i++ ) {
	    var card = cards[i];
	    var cardID = card.getAttribute("data-id");
	    var id = "timeoutID" + cardID;
	    var time = randomNum( timeMin, timeMax ) * 1000;
	    cardsTimeout( id, time, card );
	  }

	  // timeout listener
	  function cardsTimeout( id, time, card ) {
	    if (id in timeouts) {
	      clearTimeout(timeouts[id]);
	    }
	    timeouts[id] = setTimeout( function() {
	      var c = card.classList;
	      var newTime = randomNum( timeMin, timeMax ) * 1000;
	      c.contains("flipped") === true ? c.remove("flipped") : c.add("flipped");
	      cardsTimeout( id, newTime, card );
	    }, time );
	  }

	  // random number generator given min and max
	  function randomNum( min, max ) {
	    return Math.random() * (max - min) + min;
	  }

	};