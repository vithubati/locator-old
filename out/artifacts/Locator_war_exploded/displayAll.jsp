<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.locator.model.Capture"%>
<%@page import="java.sql.ResultSet"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>All places </title>
	<meta charset="utf-8" />
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link rel="stylesheet" href="style.css" type="text/css"/>
	<meta name ="viewport" content="width=device-width, initial-scale=1.0" >		
</head>

<body class="body">
<header class="mainHeader">
<form class="search" action="find" method="post">
		<nav>
			<ul>
				<li><a href="#">Contect</a></li>
				<li><a href="reg.jsp">Your Shop</a></li>
				<li> <input type="submit" class="findButton" name="btfind" value="find"> </li>
				<li> <select id="district"	name="district" class="district" >
							<option value="none">District</option>
							<option value="Colombo">Colombo</option>
							<option value="Kandy">Kandy</option>
							<option value="Galle">Galle</option>
							<option value="Ampara">Ampara</option>
							<option value="Anuradhapura">Anuradhapura</option>
							<option value="Badulla">Badulla</option>
							<option value="Batticaloa">Batticaloa</option>
							<option value="Gampaha">Gampaha</option>
							<option value="Hambantota">Hambantota</option>
							<option value="Jaffna">Jaffna</option>
							<option value="Kalutara">Kalutara</option>
							<option value="Kegalle">Kegalle</option>
							<option value="Kilinochchi">Kilinochchi</option>
							<option value="Kurunegala">Kurunegala</option>
							<option value="Mannar">Mannar</option>
							<option value="Matale">Matale</option>
							<option value="Matara">Matara</option>
							<option value="Moneragala">Moneragala</option>
							<option value="Mullativu">Mullativu</option>
							<option value="Nuwara Eliya">Nuwara Eliya</option>
							<option value="Polonnaruwa">Polonnaruwa</option>
							<option value="Puttalam">Puttalam</option>
							<option value="Ratnapura">Ratnapura</option>
							<option value="Trincomalee">Trincomalee</option>
							<option value="Vavuniya">Vavuniya</option>
						</select></li>
				<li> <input type="text" class="findTextBox" name="txtfind" placeholder="Looking For"> </li>
				
		
				<a href="home"><img src="img/logo.gif"></a>
			</ul>
		</nav>
		</form>
	</header>
	<div class="main">	
	<div class="leftSideContent">
	<!--	<aside class="leftTop">
		<article>
			<h2>Popular Shops</h2>
			<p>Romafour</p>
			<p>NoLimit</p>
		</article>
	</aside> --->
	<aside class="leftBotom">
		<article><h3>Destricts</h3>
		<div class="destrics"><ul>	
			<li><a href="find?destrict=Colombo">Colombo</a></li>
						<li><a href="find?destrict=Kandy">Kandy</a></li>
						<li><a href="find?destrict=Galle">Galle</a></li>
						<li><a href="find?destrict=Ampara">Ampara</a></li>
						<li><a href="find?destrict=Anuradhapura">Anuradhapura</a></li>
						<li><a href="find?destrict=Badulla">Badulla</a></li>
						<li><a href="find?destrict=Batticaloa">Batticaloa</a></li>
						<li><a href="find?destrict=Gampaha">Gampaha</a></li>
						<li><a href="find?destrict=Hambantota">Hambantota</a></li>
						<li><a href="find?destrict=Jaffna">Jaffna</a></li>
						<li><a href="find?destrict=Kalutara">Kalutara</a></li>
						<li><a href="find?destrict=Kegalle">Kegalle</a></li>
						<li><a href="find?destrict=Kilinochchi">Kilinochchi</a></li>
						<li><a href="find?destrict=Kurunegala">Kurunegala</a></li>
						<li><a href="find?destrict=Mannar">Mannar</a></li>
						<li><a href="find?destrict=Matale">Matale</a></li>
						<li><a href="find?destrict=Matara">Matara</a></li>
						<li><a href="find?destrict=Moneragala">Moneragala</a></li>
						<li><a href="find?destrict=Mullativu">Mullativu</a></li>
						<li><a href="find?destrict=Nuwara Eliya">Nuwara Eliya</a></li>
						<li><a href="find?destrict=Polonnaruwa">Polonnaruwa</a></li>
						<li><a href="find?destrict=Puttalam">Puttalam</a></li>
						<li><a href="find?destrict=Ratnapura">Ratnapura</a></li>
						<li><a href="find?destrict=Trincomalee">Trincomalee</a></li>
						<li><a href="find?destrict=Vavuniya">Vavuniya</a></li>
			</ul></div>
		</article>
	</aside>
	</div>
	<div class="allmainContent">
		<div class="allcontent">
			<article class="alltopContent">
				<header>
					<span class="subtitle">All Shops and Institutions</span>
				</header>
				<content>
				
				</content>
			</article>
			<%if(request.getAttribute("Error")!=null)
	         { String msg = request.getAttribute("Error").toString();
		           if(msg.equals("noData")){ %>	         					
				         <article class="alltopContent">
								<span class="subtitle">0 results found</span>
						</article>
			
					<%}else if(msg.equals("error")){%>
							<article class="alltopContent">
								<span class="subtitle">Somthing went wrong!</span>
						</article>
					<%}
	         }
			else if(request.getAttribute("find")!=null)
	         { 
				List<?> lst = (List<?>)request.getAttribute("find");
				List<String> a1 = new ArrayList<String>();	List<String> a2 = new ArrayList<String>();
				List<String> a3 = new ArrayList<String>();	List<String> a4 = new ArrayList<String>();
				List<String> a5 = new ArrayList<String>();	List<Integer> a6 = new ArrayList<Integer>();
				List<Integer> a7 = new ArrayList<Integer>();	List<Integer>a8 = new ArrayList<Integer>();
				List<String> a9 = new ArrayList<String>();	List<String>a10 = new ArrayList<String>();
				List<String> a11 = new ArrayList<String>();	
				a1 = (List<String>)lst.get(0);
				a2 = (List<String>)lst.get(1);
				a3 = (List<String>)lst.get(2);
				a4 = (List<String>)lst.get(3);
				a5 = (List<String>)lst.get(4);
				a6 = (List<Integer>)lst.get(5);;
				a7 = (List<Integer>)lst.get(6);
				a8 = (List<Integer>)lst.get(7);
				a9 = (List<String>)lst.get(8);
				a10 = (List<String>)lst.get(9);
				a11 = (List<String>)lst.get(10);
				
				for(int i =0; i < a1.size(); i++){			
	         %>
	         <article class="allmidleContent">
				<div class="advert"><span class="shopname"><%=a1.get(i) %></span> <br>
				<%=a2.get(i) %> &nbsp;<%=a3.get(i) %> &nbsp;<%=a4.get(i) %> &nbsp;<%=a5.get(i) %> <br>
				<span class="addresstitle" >Mobile &nbsp;&nbsp;&nbsp;</span><%=a6.get(i) %>
				<% int tele2= a7.get(i);
	            if(tele2 != 0)
	            {%>
	            	<br><span class="addresstitle"> Tele &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><%=a7.get(i) %>
	            <% } 
	            int fax= a8.get(i); 
	            if(fax != 0)
	            { %>
	            	<br><span class="addresstitle"> Fax &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> <%=a8.get(i) %>
	            <% }
				String email= a9.get(i);
	            if(!email.equals("")){ %>
	            		<br> <span class="addresstitle">Email &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><%=a9.get(i) %>
	            <% }
	            String web= a10.get(i);
	            if(!web.equals("")){ %>
	            		&nbsp;&nbsp; &nbsp;&nbsp;
					&nbsp;&nbsp;<span class="addresstitle">Website &nbsp;&nbsp; </span> <%=a10.get(i) %>
				<% } %>
				<br><span class="addresstitle">About &nbsp;&nbsp; </span><%=a11.get(i) %>
	         	
				
				</div> 
				</article>
				<%} 
			}else if(request.getAttribute("find2")!=null)
	         { 
				List<?> lst = (List<?>)request.getAttribute("find2");
				List<String> b1 = new ArrayList<String>();	List<String> b2 = new ArrayList<String>();
				List<String> b3 = new ArrayList<String>();	List<String> b4 = new ArrayList<String>();
				List<String> b5 = new ArrayList<String>();	List<Integer> b6 = new ArrayList<Integer>();
				List<Integer> b7 = new ArrayList<Integer>();	List<Integer>b8 = new ArrayList<Integer>();
				List<String> b9 = new ArrayList<String>();	List<String>b10 = new ArrayList<String>();
				List<String> b11 = new ArrayList<String>();	
				b1 = (List<String>)lst.get(0);
				b2 = (List<String>)lst.get(1);
				b3 = (List<String>)lst.get(2);
				b4 = (List<String>)lst.get(3);
				b5 = (List<String>)lst.get(4);
				b6 = (List<Integer>)lst.get(5);;
				b7 = (List<Integer>)lst.get(6);
				b8 = (List<Integer>)lst.get(7);
				b9 = (List<String>)lst.get(8);
				b10 = (List<String>)lst.get(9);
				b11 = (List<String>)lst.get(10);
				
				for(int i =0; i < b1.size(); i++){			
	         %>
	         <article class="allmidleContent">
				<div class="advert"><span class="shopname"><%=b1.get(i) %></span> <br>
				<%=b2.get(i) %> &nbsp;<%=b3.get(i) %> &nbsp;<%=b4.get(i) %> &nbsp;<%=b5.get(i) %> <br>
				<span class="addresstitle" >Mobile &nbsp;&nbsp;&nbsp;</span><%=b6.get(i) %>
				<% int tele2= b7.get(i);
	            if(tele2 != 0)
	            {%>
	            	<br><span class="addresstitle"> Tele &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><%=b7.get(i) %>
	            <% } 
	            int fax= b8.get(i); 
	            if(fax != 0)
	            { %>
	            	<br><span class="addresstitle"> Fax &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> <%=b8.get(i) %>
	            <% }
				String email= b9.get(i);
	            if(!email.equals("")){ %>
	            		<br> <span class="addresstitle">Email &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><%=b9.get(i) %>
	            <% }
	            String web= b10.get(i);
	            if(!web.equals("")){ %>
	            		&nbsp;&nbsp; &nbsp;&nbsp;
					&nbsp;&nbsp;<span class="addresstitle">Website &nbsp;&nbsp; </span> <%=b10.get(i) %>
				<% } %>
				<br><span class="addresstitle">About &nbsp;&nbsp;&nbsp; </span><%=b11.get(i) %>
	         	
				
				</div> 
				</article>
				<% } } %>
			
			
		</div>
	</div>
	
	<footer class="mainFooter">
	<p>	Copyright &copy; locator.com </p>
	</footer>
</div>
</body>	
	
</html>