package com.locator.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

/**
 * Servlet Filter implementation class FilterLook
 */
@WebFilter("/filterlook")
public class FilterLook implements Filter {

    /**
     * Default constructor. 
     */
    public FilterLook() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		/*HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse hres = (HttpServletResponse) response;

		String pathInfo = httpRequest.getRequestURI().trim();

		//System.out.println(pathInfo);
		if (pathInfo.endsWith(".gif") || pathInfo.endsWith(".css")) {
			//chain.doFilter(request, response);
			request.getRequestDispatcher("/views.jsp").forward(request, response);
		}else{
			request.getRequestDispatcher(pathInfo.substring(8)).forward(request, response); // Forward to page controller.
		}*/
		chain.doFilter(request, response);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
