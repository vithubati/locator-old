package com.locator.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.locator.db.ConnectionFactory;
import com.locator.dto.Institution;

public class UpdateDB {
	private Connection con;
	private ConnectionFactory db = new ConnectionFactory();
	private Communication vdb = new Communication();
	private Subject s = new Subject();
	private AddressConverter add = new AddressConverter();
	/*=========================================================================*/
	public boolean insertTempInst(String catgry, String type, String nme, String no, String street, String city,
			String district, String tele1, String tele2, String fax, String shpemail,
			String website, String mail, String descript, String status, String token, String photoPath, String url)
			throws Exception {
		boolean rsult = false;
		PreparedStatement ps = null;
		try {
			float latitude = 7.873054f;
			float longitude = 80.771797f;
			//get GeoPoint
			String[] latAndLong = add.getLatAndLong(no + " " + street+ " " + city+ " " + district);
			if (latAndLong != null && latAndLong.length>0) {
				latitude = Float.parseFloat(latAndLong[0]);
				longitude = Float.parseFloat(latAndLong[1]);
			}
			java.sql.Timestamp timestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());			
			con = db.createConnection();

			String str = "insert into update_institution values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			ps = con.prepareStatement(str);
			ps.setInt(1, 0);
			ps.setString(2, catgry);
			ps.setString(3, type);
			ps.setString(4, nme);
			ps.setString(5, no);
			ps.setString(6, street);
			ps.setString(7, city);
			ps.setString(8, district);
			ps.setString(9, tele1);
			ps.setString(10, tele2);
			ps.setString(11, fax);
			ps.setString(12, shpemail);
			ps.setString(13, website);
			ps.setString(14, mail);
			ps.setString(15, descript);
			ps.setTimestamp(16, timestamp); //insert date
			ps.setString(17, status);
			ps.setString(18, token);
			ps.setString(19, url); //link
			ps.setTimestamp(20, timestamp); //status date
			ps.setString(21, photoPath);
			ps.setFloat(22, latitude);
			ps.setFloat(23, longitude);
			int res = ps.executeUpdate();

			if (res > 0) {
				rsult = true;
			}
			return rsult;
		} catch (Exception e) {
			System.out.println("UpdateModelException: insertTempInst() "+e);
			e.printStackTrace();
			rsult = false;
			return rsult;

		}finally {
			try {
				if (ps != null){
					ps.close();
				}
				if (con != null){
					con.close();
				}
			} catch (SQLException e) {
				System.out.println("inside Finally block " + e);
				rsult = false;
				return rsult;
			}
		}
	}
	/*=========================================================================*/
	private ResultSet captureTempData(String mail, String tokn) {
		ResultSet rs;
        PreparedStatement ps = null;
        try {
            con=db.createConnection();
            String str="select * from update_institution where email= ? and token=? ";
            ps=con.prepareStatement(str);
            ps.setString(1,mail);
            ps.setString(2,tokn);
            rs=ps.executeQuery();
            return rs;
        }
        catch(Exception e)
        {
        	System.out.println("UpdateModelException: captureTempData() "+e);
        	e.printStackTrace();
            rs=null;
            return rs;
        }/*finally {
			try {if (ps != null){ps.close();}
				if (con != null){con.close();}
			} catch (SQLException e) {
				System.out.println("inside Finally block " + e);
				 rs=null;
		            return rs;
			}
		}*/   
	}
	/*=========================================================================*/
	public boolean doUpdate(String email, String token){
		boolean rsult = false;
		ResultSet rs = null;
		try{
			rs = captureTempData(email, token);
			String catgry=""; String type=""; String nme=""; String no=""; String street=""; String city="";
			String district=""; String tele1= ""; String tele2 =""; String fax=""; String shpemail=""; String website="";
			String mail=""; String descript=""; String photoPath = null; String url=""; float latitude = 7.873054f;
			float longitude = 80.771797f;
			while (rs.next()) {
				 catgry= rs.getString(2);  type= rs.getString(3);  nme= rs.getString(4);  no= rs.getString(5);  
				 street= rs.getString(6);  city= rs.getString(7);  district= rs.getString(8); tele1 = rs.getString(9);
				 tele2= rs.getString(10); fax= rs.getString(11);  shpemail= rs.getString(12); website= rs.getString(13);
				 mail= rs.getString(14); descript= rs.getString(15); latitude = rs.getFloat(22); 
				 longitude = rs.getFloat(23); photoPath = rs.getString(21);  url = rs.getString(19);;																
			}
			rsult = updateInst(catgry, type, nme, no, street, city, district, tele1, tele2, fax, shpemail, website, 
					mail, descript, photoPath, url, latitude, longitude);
			return rsult;
		}catch(Exception ex){
			System.out.println("UpdateModelException: doUpdate() "+ex);
			ex.printStackTrace();
			rsult = false;
			return rsult;
		}finally {
			try {
				if (rs != null){
					rs.close();
				}
				if (con != null){
					con.close();
				}
			} catch (SQLException e) {
				System.out.println("inside Finally block " + e);
				rsult = false;
				return rsult;
			}
		}		
	}
	/*=========================================================================*/
	public boolean updateInst(String catgry, String type, String nme, String no, String street, String city,
			String district, String tele1, String tele2, String fax, String shpemail,
			String website, String mail, String descript, String photo, String url, float lat, float lon)
			throws Exception {
		boolean rsult = false;
		PreparedStatement ps =null;
		try {
			java.sql.Timestamp timestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
			con = db.createConnection();
			String str;
			//float latitude = 7.873054f;
			//float longitude = 80.771797f;
			int res;
			if(photo == null || photo.equals("")){
				str = "update institution set category= ?, inst_type= ?, inst_name= ?, inst_no= ?, street= ?, city= ?, district= ?, "
						+ "teleph_1= ?, teleph_2= ?, fax= ?, shop_email= ?, website= ?, email= ?, description= ?, link= ?, "
					+ "status_time_stamp= ?, inst_latitude = ?, inst_longitude = ? where link= ?";
				ps = con.prepareStatement(str);
				ps.setString(1, catgry);
				ps.setString(2, type);
				ps.setString(3, nme);
				ps.setString(4, no);
				ps.setString(5, street);
				ps.setString(6, city);
				ps.setString(7, district);
				ps.setString(8, tele1);
				ps.setString(9, tele2);
				ps.setString(10, fax);
				ps.setString(11, shpemail);
				ps.setString(12, website);
				ps.setString(13, mail);
				ps.setString(14, descript);
				ps.setString(15, url);
				ps.setTimestamp(16, timestamp);
				ps.setFloat(17, lat);
				ps.setFloat(18, lon);
				ps.setString(19, url);
				res = ps.executeUpdate();
			}else{
				str = "update institution set category= ?, inst_type= ?, inst_name= ?, inst_no= ?, street= ?, city= ?,"
					+ " district= ?, teleph_1= ?, teleph_2= ?, fax= ?, shop_email= ?, website= ?, email= ?, "
					+ " description= ?, link= ?, status_time_stamp= ?, shop_photo_path= ?, inst_latitude = ?, inst_longitude = ?"
					+ "where link= ?";
				ps = con.prepareStatement(str);
				ps.setString(1, catgry);
				ps.setString(2, type);
				ps.setString(3, nme);
				ps.setString(4, no);
				ps.setString(5, street);
				ps.setString(6, city);
				ps.setString(7, district);
				ps.setString(8, tele1);
				ps.setString(9, tele2);
				ps.setString(10, fax);
				ps.setString(11, shpemail);
				ps.setString(12, website);
				ps.setString(13, mail);
				ps.setString(14, descript);
				ps.setString(15, url);
				ps.setTimestamp(16, timestamp);
				ps.setString(17, photo);
				ps.setFloat(18, lat);
				ps.setFloat(19, lon);
				ps.setString(20, url);
				res = ps.executeUpdate();
				}
				if (res > 0) {
					rsult = true;
				}
				return rsult;
		}catch (Exception e) {
			System.out.println("UpdateModelException: updateInst() "+e);
			e.printStackTrace();
			rsult = false;
			return rsult;
		} finally {
			try {
				if (ps != null){
					ps.close();
				}
				if (con != null){
					con.close();
				}
			} catch (SQLException e) {
				System.out.println("inside Finally block " + e);
				rsult = false;
				return rsult;
			}
		}
	}
	/*====================================================================*/
	public List<Institution> captureShop(String nam, String url) {
		String query;
		query = "select category, inst_type, inst_name, inst_no, street, city, district, teleph_1, teleph_2, fax, shop_email, website, "
				+ "email, description, shop_photo_path from institution where inst_status = 'active' and inst_name = ? and link = ? ";
		List<Institution> list = new ArrayList<Institution>();
		Institution insti = null;
		PreparedStatement ps = null;
		try {
			con = db.createConnection();
			ps = con.prepareStatement(query);
			ps.setString(1, nam);
			ps.setString(2, url);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				insti = new Institution();
				insti.setCatgry(rs.getString(1));
				insti.setType(rs.getString(2));
				insti.setNme(rs.getString(3));
				insti.setNo(rs.getString(4));
				insti.setStreet(rs.getString(5));
				insti.setCity(rs.getString(6));
				insti.setDistrict(rs.getString(7));
				insti.setTele1(rs.getString(8));
				insti.setTele2(rs.getString(9));
				insti.setFax(rs.getString(10));
				insti.setShpemail(rs.getString(11));
				insti.setWebsite(rs.getString(12));
				insti.setMail(rs.getString(13));
				insti.setDescript(rs.getString(14));
				insti.setPhotoPath(rs.getString(15));
				list.add(insti);
			}
			rs.close();
		} catch (SQLException e) {
			System.out.println("UpdateModelException: captureShop() "+e);
			e.printStackTrace();
			list = null;
			return list;
		} finally {
			try {
				if (ps != null){
					ps.close();
				}
				if (con != null){
					con.close();
				}
			} catch (SQLException e) {
				System.out.println("inside Finally block " + e);
				list = null;
				return list;
			}
		}
		return list;
	}
	/************************************************************************************************************/
	public boolean setStatus(String stat, String email, String token){
		boolean result = false;
		PreparedStatement ps = null;
		try{
			con = db.createConnection();
			String str = "update update_institution set inst_status = ? where email = ? and token = ?";
			ps = con.prepareStatement(str);
			ps.setString(1, stat);
			ps.setString(2, email);
			ps.setString(3, token);
			int i = ps.executeUpdate();
			if(i > 0){
				result = true;
			}
			return result;
		}catch(Exception e){
			System.out.println("UpdateModelException: setStatus()" + e);
			e.printStackTrace();
			result = false;
			return result;
		}finally {
			try {
				if (ps != null){
					ps.close();
				}
				if (con != null){
					con.close();
				}
			} catch (SQLException e) {
				System.out.println("inside Finally block " + e);
				result = false;
				return result;
			}
		}
	}
	/************************************************************************************************************/
	public ResultSet verifyTempToken(String email, String tokn) {
		ResultSet res;
		PreparedStatement ps = null;
		try {
			con = db.createConnection();
			String str = "select inst_status, link from update_institution where email = ? and token = ?";
			ps = con.prepareStatement(str);
			ps.setString(1, email);
			ps.setString(2, tokn);
			res = ps.executeQuery();
			return res;
		} catch (Exception e) {
			System.out.println("UpdateModelException: VerifyTempToken() "+e);
			e.printStackTrace();
			res = null;
			return res;
		}/*finally {
			try {if (ps != null){ps.close();}
				if (con != null){con.close();}
			} catch (SQLException e) {
				System.out.println("inside Finally block " + e);
				res = null;
				return res;
			}
		}*/

	}
	/*******************************************************************************************/
	public boolean sendMailWithLink(String link, String mail) {
		boolean delivered = false;
		String subject= "Update information";
		try{
			String body = makeBody(link);
			if(!body.equals("")){
				delivered = vdb.setMail(mail, subject, body);
			}else{
				delivered = false;
			}
			return delivered;
		}catch(Exception e){
			System.out.println("UpdateModelException: SendMailWithLink() "+e);
			e.printStackTrace();
			delivered = false;
			return delivered;
		}
	}
	private String makeBody(String link){ // later have to decorate 
		String body = "";
		try{
			String domain = "http://locator.elasticbeanstalk.com/view/";
			body= s.htmlS + s.bodyS +
					s.container + 
					
					s.header + 
					"Your Place Ad Is Active on Locator" + 
					s.divE + 
					s.mainbody+
					" <br><p> Hi There, </p><br>"+
					s.divS+ "Your Place Ad has been updated successfully on Locator. <br><br>" +
					s.adNme+ 
					"<span style='color: #5cb85c; font-size:130%;'> Manage Your Ad </span> <p> Click below link to View, Edit or Delete Your place </p> <br>"
					+ "<a href='"+domain + link+"'>My Place</a>"
					+ s.divE+
					s.divE+
					
					"<p>  We hope your Place ad generates the response you're looking for. Thanks for using Locator and good luck with your place. </p>"
					+ "<br><p>Regards,<br>"
					+ " The Locator Team </p><br>" +				
					s.divE+
					s.footer+
					"<p>	Copyright &copy; locator.com </p>"+
					s.divE+
					s.divE
					+ s.bodyE + s.htmlE;
			return body;
		}catch(Exception e){
			System.out.println("UpdateModelException: makeBody() "+e);
			e.printStackTrace();
			return body;
		}
	}
}
