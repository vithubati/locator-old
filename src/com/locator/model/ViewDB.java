package com.locator.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.locator.db.ConnectionFactory;
import com.locator.dto.Institution;

public class ViewDB {
	private Connection con;
	private ConnectionFactory db = new ConnectionFactory();
	private PreparedStatement ps;
	
	/*====================================================================*/
	public List<Institution> captureShop(String url) {
		String query;
		query = "select inst_name, inst_no, street, city, district, teleph_1, teleph_2, fax, shop_email, website, description, "
				+ "shop_photo_path, inst_latitude, inst_longitude from "
					+ "institution where inst_status = 'active' and link = '"+ url + "'";
		List<Institution> list = new ArrayList<Institution>();
		Institution insti = null;
		try {
			con = db.createConnection();
			ps = con.prepareStatement(query);
			ResultSet rs = ps.executeQuery(query);
			while (rs.next()) {
				insti = new Institution();
				insti.setNme(rs.getString(1));
				insti.setNo(rs.getString(2));
				insti.setStreet(rs.getString(3));
				insti.setCity(rs.getString(4));
				insti.setDistrict(rs.getString(5));
				insti.setTele1(rs.getString(6));
				insti.setTele2(rs.getString(7));
				insti.setFax(rs.getString(8));
				insti.setShpemail(rs.getString(9));
				insti.setWebsite(rs.getString(10));
				insti.setDescript(rs.getString(11));
				insti.setPhotoPath(rs.getString(12));
				insti.setLatitude(rs.getFloat(13));
				insti.setLongitude(rs.getFloat(14));
				//System.out.println("photo: "+ rs.getBytes(12));
				list.add(insti);
			}
			rs.close();
		} catch (SQLException e) {
			System.out.println("ViewModelException: "+ e);
			e.printStackTrace();
			list = null;
			return list;
		} catch (Exception e) {
			System.out.println("ViewModelException: "+e);
			e.printStackTrace();
			list = null;
			return list;
		}finally {
			try {
				if (ps != null){
					ps.close();
				}
				if (con != null){
					con.close();
				}
			} catch (SQLException e) {
				list = null;
				return list;
			}
		}
		return list;
	}
	/* =============================================================================== */


}
