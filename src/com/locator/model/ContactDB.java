package com.locator.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Calendar;

import com.locator.db.ConnectionFactory;

public class ContactDB {
	private ConnectionFactory cf = new ConnectionFactory();
	private Connection con;
	public boolean insertMessage(String fname, String lname, String email, long phn, String subj, String msg) {
		PreparedStatement ps = null;
		boolean saved = false;
		java.sql.Timestamp timestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
		try {
			String STATUS = "await";
			String qry = "insert into contact_message values (?, ?, ?, ?, ?, ?, ?, ?, ?)";
			con = cf.createConnection();
			ps = con.prepareStatement(qry);
			ps.setInt(1, 0);
			ps.setString(2, fname);
			ps.setString(3, lname);
			ps.setString(4, email);
			ps.setLong(5, phn);
			ps.setString(6, subj);
			ps.setString(7, msg);
			ps.setTimestamp(8, timestamp);
			ps.setString(9, STATUS);
			int i = ps.executeUpdate();
			if(i > 0)
				saved = true;
			return saved;
		} catch (Exception e) {
			System.out.println("ContactModelException: insertMessage()" + e);
			e.printStackTrace();
			saved = false;
			return saved;
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				saved = false;
				return saved;
			}

		}
	}
}
