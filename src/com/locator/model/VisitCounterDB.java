package com.locator.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;

import com.locator.db.ConnectionFactory;

public class VisitCounterDB {
	private ConnectionFactory db = new ConnectionFactory();
	
	public boolean insertCount(String link, String email)throws Exception {
		boolean rsult = false;
		PreparedStatement ps = null;
		Connection con = null;
		try {
			java.sql.Timestamp timestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
			con = db.createConnection();
			int s = 1;
			String qry = "insert into visit_counter values(?,?,?,?,?,?)";
			ps = con.prepareStatement(qry);
			ps.setInt(1, 0);
			ps.setInt(2, s);
			ps.setTimestamp(3, timestamp);
			ps.setString(4, email);
			ps.setString(5, link );
			ps.setString(6, "");
			int rs =ps.executeUpdate();
			if(rs > 0){
				rsult = true;
			}
			return rsult;
		}catch(Exception ex){
			System.out.println("VisitCounterModelException: insertCount()" + ex);
			ex.printStackTrace();
			rsult = false;
			return rsult;
		}finally {
			try {
				if (ps != null){ps.close();}
				if (con != null){con.close();}
			} catch (SQLException e) {
				System.out.println("inside Finally block " + e);
				rsult = false;
				return rsult;
			}
		}

	}
	public int makeCount(String host, String link) {
		int rsult = -1;
		Connection conn = null;
		PreparedStatement ps =null;
		ResultSet rs =null;
		try {
			java.sql.Timestamp timestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
			//conn = db.createConnection();
			int totCount = 0;
			rs = checkCount(link, host);
			if(rs != null){
				if(rs.next()){
					rsult = rs.getInt(1);
					return rsult; 
				}else{
					if (rs != null){
						rs.close();
					}
					rs = null;
					conn = db.createConnection();
					rs = captureCount(link, host);
					if(rs.next()){
						do {
							totCount = rs.getInt(1);
						} while (rs.next());
						if (totCount > 0) {
							String qry = "update visit_counter set count = ?, time_stamp = ?, client_host = CONCAT(client_host, ?) where inst_url=?";
							try{
								ps = conn.prepareStatement(qry);
								totCount = totCount + 1;
								ps.setInt(1, totCount);
								ps.setTimestamp(2, timestamp);
								ps.setString(3, ","+host);
								ps.setString(4, link);
								int res;
								res = ps.executeUpdate();
								if (res > 0) {
									rsult = totCount;
								}
								return rsult;
							}catch(Exception ex){
								System.out.println("VisitCounterModel: " + ex);
								rsult = -1;
								return rsult;
							}
							
						}
					}else{
						System.out.println("AppException: no visit count found for this link");
						rsult = -1;
					}
					return rsult;					
				}
			}else{
				System.out.println("AppException: VisitCounterModel: TotalCount returned null in checkCount()");
				rsult = -1;
				return rsult;
			}			
		}catch(Exception ex){
			System.out.println("VisitCounterModelException: makeCount()" + ex);
			ex.printStackTrace();
			rsult = -1;
			return rsult;
		} finally {
			try {
				if (ps != null){ps.close();}
				if (rs != null){rs.close();}
				if (conn != null){conn.close();}
			} catch (SQLException e) {
				rsult = -1;
				return rsult;
			}
		}
	}
	/*
	 * ========================================================================== */
	public ResultSet checkCount(String link, String host){
		PreparedStatement pst =null;
		Connection con = null;
		ResultSet rslt = null;
		String query = "select count, time_stamp from visit_counter where inst_url = ? and client_host like ?";
		try {
			con = db.createConnection();
			pst = con.prepareStatement(query);
			pst.setString(1, link);
			pst.setString(2, "%" + host + "%");
			rslt =pst.executeQuery();
		} catch (SQLException e) {
			System.out.println("VisitCounterModelException: checkCount()" + e);
			rslt = null;
			return rslt;
		}/* finally {
			try {
				if (pst != null){pst.close();}
				if (rslt != null){rslt.close();}
				if (con != null){con.close();}
			} catch (SQLException e) {
				System.out.println("inside Finally block " + e);
				rslt = null;
				return rslt;
			}
		}*/
		return rslt;
	}

	public boolean clearData(String host, String link) {
		boolean rsult = false;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		long msec = 10 *60 *60 *1000;
		try {
			java.sql.Timestamp curTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime() - msec);
			con = db.createConnection();
			java.sql.Timestamp upTimStmp = null;
			rs = captureCount(link, host);
			if(rs.next()){
				upTimStmp = rs.getTimestamp(2);					
			}
			if(upTimStmp != null){
				if(upTimStmp.before(curTimestamp)){
					rsult = true;	
					String qry = "update visit_counter set client_host = ? where inst_url= ?";
					rsult = false;
					ps = con.prepareStatement(qry);
					ps.setString(1, "");
					ps.setString(2, link);
					int res;
					res = ps.executeUpdate();
					if (res > 0) 
						rsult = true;		
				}else{
					rsult = true;
				}
			}
			return rsult;
		} 
		catch (Exception ex) {
			System.out.println("VisitCounterModelException: clearData()" + ex);
			ex.printStackTrace();
			rsult = false;	
			return rsult;
		}finally {
			try {
				if (ps != null){ps.close();}
				if (rs != null){rs.close();}
				if (con != null){con.close();}
			} catch (SQLException e) {
				System.out.println("inside Finally block " + e);
				rsult = false;	
				return rsult;
			}
		}
	}
	public ResultSet captureCount(String link, String host) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query2 = "select count, time_stamp from visit_counter where inst_url = ?";
		try {
			con = db.createConnection();
			ps = con.prepareStatement(query2);
			ps.setString(1, link);
			rs = ps.executeQuery();
			return rs;
		} catch (SQLException e) {
			System.out.println("VisitCounterModelException: captureCount()" + e);
			e.printStackTrace();
			rs = null;
			return rs;
		}/*finally {
			try {
				if (ps != null){ps.close();}
				if (con != null){con.close();}
			} catch (SQLException e) {
				System.out.println("inside Finally block " + e);
				rs = null;
				return rs;
			}
		}*/
	}
}
