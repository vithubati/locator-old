package com.locator.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Calendar;

import com.locator.db.ConnectionFactory;

public class ReportDB {
	private ConnectionFactory cf = new ConnectionFactory();
	private Connection con;
	public boolean insertReport(String email, String subj, String msg, String url) {
		PreparedStatement ps = null;
		boolean saved = false;
		java.sql.Timestamp timestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
		try {
			String STATUS = "await";
			String qry = "insert into report_place values (?, ?, ?, ?, ?, ?, ?)";
			con = cf.createConnection();
			ps = con.prepareStatement(qry);
			ps.setInt(1, 0);
			ps.setString(2, email);
			ps.setString(3, subj);
			ps.setString(4, msg);
			ps.setString(5, url);
			ps.setTimestamp(6, timestamp);
			ps.setString(7, STATUS);
			int i = ps.executeUpdate();
			if(i > 0)
				saved = true;
			return saved;
		} catch (Exception e) {
			System.out.println("ReportModelException: insertReport()" + e);
			e.printStackTrace();
			saved = false;
			return saved;
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				saved = false;
				return saved;
			}

		}
	}

}
