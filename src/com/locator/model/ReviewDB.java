package com.locator.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import com.locator.db.ConnectionFactory;
import com.locator.dto.Review;

public class ReviewDB {
	private Connection con;
	private ConnectionFactory db = new ConnectionFactory();
	private int noOfRecords;
	private Capture capt = new Capture();
	/*====================================================================*/
	public List<Review> captureReview(String url) {
		PreparedStatement ps = null;
		String query;
		query = "select * from review where status = 'good' and insti_url = ? ORDER BY created_at desc";
		List<Review> list = new ArrayList<Review>();
		Review revw = null;
		try {
			con = db.createConnection();
			ps = con.prepareStatement(query);
			ps.setString(1, url);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				revw = new Review();
				revw.setUrl(rs.getString(2));
				revw.setRating(rs.getInt(3));
				revw.setComent(rs.getString(4));
				revw.setStatus(rs.getString(5));
				revw.setTimestamp(rs.getTimestamp(6));
				list.add(revw);
			}
			rs.close();
		} catch (SQLException e) {
			System.out.println("ReviewModelException: captureReview()" + e);
			e.printStackTrace();
			list = null;
			return list;
		} finally {
			try {
				if (ps != null){
					ps.close();
				}
				if (con != null){
					con.close();
				}
			} catch (SQLException e) {
				list = null;
				return list;
			}
		}
		return list;
	}
	/* =============================================================================== */
	public int getNoOfRecords() {
		return noOfRecords;
	}
	/* ========================================================================= */

	public List<Review> captureReviews(String url, int offset, int recordsPerPage, int curPage) {
		boolean available = capt.captureInsti(url);
		List<Review> list = new ArrayList<Review>();
		if (available) {
			PreparedStatement ps =null;
			PreparedStatement ps2 =null;
			String query = "select SQL_CALC_FOUND_ROWS insti_url, rating, comment, created_at from "
					+ "review where status = 'good' and insti_url = ? ORDER BY created_at desc limit ?, ?";
			Review revw = null;
			try {
				con = db.createConnection();
				ps = con.prepareStatement(query);
				ps.setString(1, url);
				ps.setInt(2, offset);	
				ps.setInt(3, recordsPerPage);
				ResultSet rs = ps.executeQuery();
				ps2 = con.prepareStatement("SELECT FOUND_ROWS()");
				ResultSet rs2 = ps2.executeQuery();
				if (rs2.next())
					this.noOfRecords = rs2.getInt(1);
				int noOfPages =(int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
				while (rs.next()) {
					revw = new Review();
					revw.setUrl(rs.getString(1));
					revw.setRating(rs.getInt(2));
					revw .setComent(rs.getString(3));
					//java.sql.Timestamp timestamp = rs.getTimestamp(4);
					revw.setTimestamp(rs.getTimestamp(4)); 
					revw.setNoOfRecords(this.noOfRecords);
					revw.setNoOfPages(noOfPages);
					revw.setCurentPage(curPage);
					list.add(revw);
				}
				Collections.reverse(list);
				
			} catch (SQLException e) {
				System.out.println("ReviewModelException: captureReviews()" + e);
				e.printStackTrace();
				list = null;
				return list;
			} finally {
				try {
					if (ps != null){
						ps.close();
					}
					if (con != null){
						con.close();
					}
				} catch (SQLException e) {
					list = null;
					return list;
				}
			}
		}else{
			return list;
		}
		return list;
	}
	/* =============================================================================== */
	
	/* ========================================================================= */
	public boolean insertReview(String coments, int rating, String url,
			String status) throws Exception {
		PreparedStatement ps = null;
		boolean rslt = false;
		java.sql.Timestamp timestamp = new java.sql.Timestamp(Calendar
				.getInstance().getTime().getTime());
		try {
			con = db.createConnection();
			String qry = "insert into review values(?,?,?,?,?,?)";
			ps = con.prepareStatement(qry);
			ps.setInt(1, 0);
			ps.setString(2, url);
			ps.setInt(3, rating);
			ps.setString(4, coments);
			ps.setString(5, status);
			ps.setTimestamp(6, timestamp);
			int s = ps.executeUpdate();
			if (s > 0) {
				rslt = true;
			}
			return rslt;
		} catch (Exception e) {
			System.out.println("ReviewModelException: insertReview()" + e);
			e.printStackTrace();
			rslt = false;
			return rslt;
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				rslt = false;
				return rslt;
			}

		}
	}
}
