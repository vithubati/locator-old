package com.locator.model;

public class Subject {
	public String  htmlS="<html>";
	public String  bodyS="<body>";
	public String  divS="<div>";
	public String  divE="</div>";
	/*public String  pS="<p>";
	public String  pE="</p>";
	public String  br="<br>";*/
	public String  container="<div style='font-family: Trebuchet MS, Trebuchet, Lucida Sans Unicode, Lucida Grande, "
			+ "Lucida Sans, Arial, sans-serif ; width:90%;  margin:0 auto;  border:1px solid #fff; box-shadow:0px 2px 7px #292929;"
			+ "  -moz-box-shadow: 0px 2px 7px #292929; -webkit-box-shadow: 0px 2px 7px #292929;   border-radius:10px; -moz-border-radius:10px;"
			+ " -webkit-border-radius:10px; background-color:#E6E6DA; color: black; padding: 1% 1% 1% 1%;'>";
	
	public String  header="<div style='text-align:center;font-size:150%;	background-color:#DDF0DD; border-radius:100px / 5px; height: 40px; "
			+ "padding: 5px; margin-bottom: 1%; color: #5CB85C;'>";
	
	public String  mainbody="<div style='height:auto; width:90%; border: solid #eee; border-width:2px 0; padding-left: 3%;'>";
	
	public String  footer="<div style='height: 40px; padding: 5px;	margin-top:1%; -webkit-border-bottom-right-radius:5px;"
			+ " -webkit-border-bottom-left-radius:5px; -moz-border-radius-bottomright:5px; -moz-border-radius-bottomleft:5px; "
			+ "border-bottom-right-radius:5px;   border-bottom-left-radius:5px;'>";
	
	public String adNme = "<div style='border-radius: 5px; -moz-border-radius: 5px; -webkit-border-radius: 5px; padding: 1% 5%; margin-bottom: 1%;'>";
	public String  htmlE="</html>";
	public String  bodyE="</body>";
	

}
