package com.locator.model;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;

import com.locator.db.ConnectionFactory;

public class DeleteDB {
	private Connection con;
	private ConnectionFactory db = new ConnectionFactory();
	private PreparedStatement ps;
	UserAuthentication usA = new UserAuthentication();
	public boolean authenticate(String link, String name, String pass) throws NoSuchAlgorithmException, InvalidKeySpecException {
		boolean valid = false;
		ResultSet rs = null;
		String qry = "select pass_word , salt_val from institution where inst_name = ? and link = ? and inst_status = 'active'";
		try {
			byte[] food = null;
			byte[] salt = null;
			con = db.createConnection();
			ps = con.prepareStatement(qry);
			ps.setString(1, name);
			ps.setString(2, link);
			rs = ps.executeQuery();
			if (rs.next()) {
				food = rs.getBytes(1);
				salt = rs.getBytes(2);
				if (food ==null || salt == null) {
					valid = false;
				}
			} else {
				// Computation time is equal to the time needed for a legitimate user
				/*food = "000000000000000000000000000=";
	            salt = "00000000000=";*/
				valid = false;
			}
			valid  = usA.authenticate(pass, food, salt);
		} catch (SQLException e) {
			System.out.println("DeleteModelException: authenticate()" + e);
			e.printStackTrace();
			valid = false;
		} finally {
			try {if (ps != null) {ps.close();}
				if (rs != null) {rs.close();}
				if (con != null) {con.close();}
			} catch (SQLException e) {
				valid = false;
				return valid;
			}
		}
		return valid;
	}
	public ResultSet checkDeleted(String name, String url){ //deactivate
		ResultSet rs;
		String qry = "select inst_status from institution where inst_name = ? and link = ?";
		try {
			con = db.createConnection();
			ps = con.prepareStatement(qry);
			ps.setString(1, name);
			ps.setString(2, url);
			rs = ps.executeQuery();
		} catch (SQLException e) {
			System.out.println("DeleteModelException: checkDeleted()" + e);
			e.printStackTrace();
			rs = null;
			return rs;
		}
		/*finally {
			try {if (ps != null){ps.close();}
				if (con != null){con.close();}
			} catch (SQLException e) {
				System.out.println("inside Finally block " + e);
				rs = null;
				return rs;
			}
		}*/
		return rs;
		
	}
	public boolean setStatus(String stat, String name, String url){ //deactivate
		boolean result = false;
		PreparedStatement ps = null;
		try{
			java.sql.Timestamp timestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
			con = db.createConnection();
			String str = "update institution set inst_status = ?, status_time_stamp = ? where inst_name = ? and link = ?";
			ps = con.prepareStatement(str);
			ps.setString(1, stat);
			ps.setTimestamp(2, timestamp);
			ps.setString(3, name);
			ps.setString(4, url);
			int i = ps.executeUpdate();
			if(i > 0){
				result = true;
			}
			return result;
		}catch(Exception e){
			System.out.println("DeleteModelException: setStatus()" + e);
			e.printStackTrace();
			result = false;
			return result;
		}finally {
			try {
				if (ps != null){
					ps.close();
				}
				if (con != null){
					con.close();
				}
			} catch (SQLException e) {
				System.out.println("inside Finally block " + e);
				result = false;
				return result;
			}
		}
		
	}
}
