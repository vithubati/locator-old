package com.locator.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.locator.db.ConnectionFactory;

public class ActiveDB {
	private Connection con;
	private ConnectionFactory db = new ConnectionFactory();
	private Communication vdb = new Communication();
	private Subject s = new Subject();
	public ResultSet VerifyToken(String email, String tokn) {
		ResultSet res;
		PreparedStatement ps = null;
		try {
			con = db.createConnection();
			String str = "select inst_status from institution where email = ? and token = ?";
			ps = con.prepareStatement(str);
			ps.setString(1, email);
			ps.setString(2, tokn);
			res = ps.executeQuery();
			return res;
		} catch (Exception ex) {
			System.out.println("ActiveModelException: VerifyToken()" + ex);
			ex.printStackTrace();
			res = null;
			return res;
		}/*finally {
			try {
				if (ps != null){ps.close();}
				if (con != null){con.close();}
			} catch (SQLException e) {
				System.out.println("inside Finally block " + e);
				res = null;
				return res;
			}
		}*/
	}

	public boolean setStatus(String stat, String email, String token){
		boolean result = false;
		PreparedStatement ps = null;
		try{			
			con = db.createConnection();
			String str = "update institution set inst_status = ? where email = ? and token = ?";
			ps = con.prepareStatement(str);
			ps.setString(1, stat);
			ps.setString(2, email);
			ps.setString(3, token);
			int i = ps.executeUpdate();
			if(i > 0){
				result = true;
			}
			return result;
		}catch(Exception e){
			System.out.println("ActiveModelException: setStatus()" + e);
			e.printStackTrace();
			result = false;
			return result;
		}finally {
			try {
				if (ps != null){
					ps.close();
				}
				if (con != null){
					con.close();
				}
			} catch (SQLException e) {
				System.out.println("inside Finally block " + e);
				result = false;
				return result;
			}
		}
		
	}
	public String getURL(String email, String tokn) {
		ResultSet res = null;
		String URL = null;
		PreparedStatement ps = null;
		try {
			con = db.createConnection();
			String qry = "select inst_name, city, district from institution where email = ? and token = ?";
			ps = con.prepareStatement(qry);
			ps.setString(1, email);
			ps.setString(2, tokn);
			res = ps.executeQuery();
			if(res.next()){
				String name, city, distrct;
				do{
					name = res.getString(1);
					city = res.getString(2);
					distrct = res.getString(3);				
				}while(res.next());
				if(name.contains(" ")){
					name = name.replace(" ", "-");
				}
				if(city.contains(" ")){
					city = city.replace(" ", "-");
				}
				if(distrct.contains(" ")){
					distrct = distrct.replace(" ", "-");
				}
				URL = name + "-" + city + "-" +  distrct;
				int foundRows = getDuplicateEntryRow(URL);
				if (foundRows > 0) {
					URL = URL + "-" + (foundRows +1);
				}else if(foundRows == -1) {
					URL = null;
				}
			}else{
				URL = null;
			}
			return URL;
		} catch (Exception e) {
			System.out.println("ActiveModelException: getURL()" + e);
			e.printStackTrace();
			URL = null;
			return URL;
		}finally {
			try {
				if (ps != null){ps.close();}
				if (res != null){res.close();}
				if (con != null){con.close();}
			} catch (SQLException e) {
				System.out.println("inside Finally block " + e);
				URL = null;
				return URL;
			}
		}

	}
	private int getDuplicateEntryRow(String url) {
		ResultSet res = null;
		int fRows = 0;
		PreparedStatement ps = null;
		try {
			String link = "%" + url + "%";
			con = db.createConnection();
			String selQry = "select COUNT(*) from institution where link like ?" ;
			ps = con.prepareStatement(selQry);
			ps.setString(1, link);
			res = ps.executeQuery();
			if(res.next()){
				fRows = res.getInt(1);
			}
			return fRows;
		}catch(Exception ex){
			System.out.println("ActiveModelException: getDuplicateEntryRow()" + ex);
			ex.printStackTrace();
			fRows = -1;
			return fRows;
		}finally {
			try {
				if (ps != null){ps.close();}
				if (res != null){res.close();}
				if (con != null){con.close();}
			} catch (SQLException e) {
				System.out.println("inside Finally block " + e);
				fRows = -1;
				return fRows;
			}
		}
	}
	public boolean setURL(String link, String email, String token){
		boolean result = false;
		PreparedStatement ps = null;
		try{
			con = db.createConnection();
			String str = "update institution set link = ? where email = ? and token = ?";
			ps = con.prepareStatement(str);
			ps.setString(1, link);
			ps.setString(2, email);
			ps.setString(3, token);
			int i = ps.executeUpdate();
			if(i > 0){
				result = true;
			}
			return result;
		}catch(Exception e){
			System.out.println("ActiveModelException: setURL()" + e);
			e.printStackTrace();
			result = false;
			return result;
		}finally {
			try {
				if (ps != null){ps.close();}
				if (con != null){con.close();}
			} catch (SQLException e) {
				System.out.println("inside Finally block " + e);
				result = false;
				return result;
			}
}
		
	}
	public boolean SendMailWithLink(String link, String mail) {
		boolean delivered = false;
		String subject= "Activation";
		try{
			String body = makeBody(link);
			if(!body.equals("")){
				delivered = vdb.setMail(mail, subject, body);
			}else{
				delivered = false;
			}
			return delivered;
		}catch(Exception e){
			System.out.println("ActiveModelException: SendMailWithLink() "+e);
			e.printStackTrace();
			delivered = false;
			return delivered;
		}
	}
	private String makeBody(String link){ // later have to decorate 
		String body = "";
		try{
			String domain = "http://locator.elasticbeanstalk.com/view/";
			body= s.htmlS + s.bodyS +
					s.container + 
					
					s.header + 
					"Your Place Ad Is Active on Locator" + 
					s.divE + 
					s.mainbody+
					" <br><p> Hi There, </p><br>"+
					s.divS+ "Thanks for posting your Place Ad on Locator. <br><br>" +
					s.adNme+ 
					"<span style='color: #5cb85c; font-size:130%;'> Manage Your Ad </span> <p> Click below link to View, Edit or Delete Your place </p> <br>"
					+ "<a href='"+domain + link+"'>My Place</a>"
					+ s.divE+
					s.divE+
					
					"<p>  We hope your Place ad generates the response you're looking for. Thanks for using Locator and good luck with your place. </p>"
					+ "<br><p>Regards,<br>"
					+ " The Locator Team </p><br>" +				
					s.divE+
					s.footer+
					"<p>	Copyright &copy; locator.com </p>"+
					s.divE+
					s.divE
					+ s.bodyE + s.htmlE;
			//String body = "Your AD Link; "+ domain + link;
			return body;
		}catch(Exception e){
			System.out.println("ActiveModelException: makeBody() "+e);
			e.printStackTrace();
			return body;
		}
	}

}
