package com.locator.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.locator.db.ConnectionFactory;
import com.locator.dto.Institution;

public class Capture {

    private Connection con;
    private ConnectionFactory db = new ConnectionFactory();
    private int noOfRecords;
    private PreparedStatement ps;

    public List<Institution> captureAll(int offset, int noOfRecords) {
        String query = "select inst_name, inst_no, street, city, district, teleph_1, teleph_2, fax, shop_email, website, description from "
                + "institution where inst_status = 'active' limit " + offset + ", " + noOfRecords;
        List<Institution> list = new ArrayList<Institution>();
        Institution insti = null;
        try {
            con = db.createConnection();
            ps = con.prepareStatement(query);
            ResultSet rs = ps.executeQuery(query);
            while (rs.next()) {
                insti = new Institution();
                insti.setNme(rs.getString(1));
                insti.setNo(rs.getString(2));
                insti.setStreet(rs.getString(3));
                insti.setCity(rs.getString(4));
                insti.setDistrict(rs.getString(5));
                insti.setTele1(rs.getString(6));
                insti.setTele2(rs.getString(7));
                insti.setFax(rs.getString(8));
                insti.setShpemail(rs.getString(9));
                insti.setWebsite(rs.getString(10));
                insti.setDescript(rs.getString(11));
                list.add(insti);
            }
            rs.close();

            rs = ps.executeQuery("SELECT FOUND_ROWS()");
            if (rs.next())
                this.noOfRecords = rs.getInt(1);
        } catch (SQLException e) {
            System.out.println("CaptureModelException: captureAll()" + e);
            e.printStackTrace();
            list = null;
            return list;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                list = null;
                return list;
            }
        }
        return list;
    }

    public int getNoOfRecords() {
        return noOfRecords;
    }

    /*====================================================================*/
    public List<Institution> captureshops(String keyword, String dist, int offset, int noOfRecords) {
        String query;
        String[] wrds;
        String key = "";
        if (keyword.equals("")) {
            if (dist.equals("none")) {
                query = "select SQL_CALC_FOUND_ROWS inst_name, inst_no, street, city, district, teleph_1, teleph_2, fax, shop_email, "
                        + "website, description, link from institution where inst_status = 'active' limit " + offset + ", " + noOfRecords;
            } else {
                query = "select SQL_CALC_FOUND_ROWS inst_name, inst_no, street, city, district, teleph_1, teleph_2, fax, shop_email, "
                        + "website, description, link from institution where inst_status = 'active' and "
                        + "district = '" + dist + "' limit " + offset + ", " + noOfRecords;
            }
        } else {
            wrds = keyword.split("\\s+");
            int i = 0;
            while (i < wrds.length) {
                key = key + " " + wrds[i] + "*";
                i++;
            }
            if (dist.equals("none")) {
                query = "select SQL_CALC_FOUND_ROWS inst_name, inst_no, street, city, district, teleph_1, teleph_2, fax, shop_email, "
                        + "website, description, link from institution where MATCH  (inst_name, insti_tags, category, city) AGAINST "
                        + "('" + key.trim() + "' IN BOOLEAN MODE) and inst_status = 'active' limit " + offset + ", " + noOfRecords;
                /*query = "select SQL_CALC_FOUND_ROWS name, no, street, city, district, teleph_1, teleph_2, fax, shop_email, website, description, "
						+ "link from institution where status = 'active' and name like '%" + keyword	+ "%' or "+ "description like '%"
						+ keyword + "%' and status = 'active' or " + "category like '%"	+ keyword + "%' and status = 'active' "
								+ "limit " + offset	+ ", " + noOfRecords ;*/
            } else {
                query = "select SQL_CALC_FOUND_ROWS inst_name, inst_no, street, city, district, teleph_1, teleph_2, fax, shop_email, "
                        + "website, description, link from institution where MATCH  (inst_name, insti_tags, category, city) AGAINST "
                        + "('" + key.trim() + "' IN BOOLEAN MODE) and inst_status = 'active' and district = '" + dist + "'"
                        + "limit " + offset + ", " + noOfRecords;
				/*query = "select SQL_CALC_FOUND_ROWS name, no, street, city, district, teleph_1, teleph_2, fax, shop_email, website, description, "
						+ "link from institution where status = 'active' and district = '" + dist	+ "' and name like '%"+ keyword
						+ "%' or " + "description like '%"	+ keyword + "%' and district = '" + dist + "' and status = 'active' or "
						+ "category like '%" + keyword + "%' and status = 'active' and district = '" + dist + "' "
								+ "limit " + offset	+ ", " + noOfRecords;*/
            }

        }


        List<Institution> list = new ArrayList<Institution>();
        Institution insti = null;
        try {
            con = db.createConnection();
            ps = con.prepareStatement(query);
            ResultSet rs = ps.executeQuery(query);
            while (rs.next()) {
                insti = new Institution();
                insti.setNme(rs.getString(1));
                insti.setNo(rs.getString(2));
                insti.setStreet(rs.getString(3));
                insti.setCity(rs.getString(4));
                insti.setDistrict(rs.getString(5));
                insti.setTele1(rs.getString(6));
                insti.setTele2(rs.getString(7));
                insti.setFax(rs.getString(8));
                insti.setShpemail(rs.getString(9));
                insti.setWebsite(rs.getString(10));
                insti.setDescript(rs.getString(11));
                insti.setLink(rs.getString(12));
                list.add(insti);
            }
            rs.close();

            rs = ps.executeQuery("SELECT FOUND_ROWS()");
            if (rs.next())
                this.noOfRecords = rs.getInt(1);
        } catch (SQLException e) {
            System.out.println("CaptureModelException: captureshops()" + e);
            e.printStackTrace();
            list = null;
            return list;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                list = null;
                return list;
            }
        }
        return list;
    }
	/* =============================================================================== */

    public List<Institution> captureShopWDist(String dist, int offset, int noOfRecords) {
        con = db.createConnection();
        String query = "";
        if (dist.equals("none")) {
            query = "select SQL_CALC_FOUND_ROWS inst_name, inst_no, street, city, district, teleph_1, teleph_2, fax, shop_email, "
                    + "website, description, link from institution where inst_status = 'active' limit " + offset + ", " + noOfRecords;
        } else {
            query = "select SQL_CALC_FOUND_ROWS inst_name, inst_no, street, city, district, teleph_1, teleph_2, fax, shop_email, "
                    + "website, description, link from institution where inst_status = 'active' and district = '"
                    + dist + "' limit " + offset + ", " + noOfRecords;
        }

        List<Institution> list = new ArrayList<Institution>();
        Institution insti = null;
        if (con != null) {
            try {
                con = db.createConnection();
                ps = con.prepareStatement(query);
                ResultSet rs = ps.executeQuery(query);
                while (rs.next()) {
                    insti = new Institution();
                    insti.setNme(rs.getString(1));
                    insti.setNo(rs.getString(2));
                    insti.setStreet(rs.getString(3));
                    insti.setCity(rs.getString(4));
                    insti.setDistrict(rs.getString(5));
                    insti.setTele1(rs.getString(6));
                    insti.setTele2(rs.getString(7));
                    insti.setFax(rs.getString(8));
                    insti.setShpemail(rs.getString(9));
                    insti.setWebsite(rs.getString(10));
                    insti.setDescript(rs.getString(11));
                    insti.setLink(rs.getString(12));
                    list.add(insti);
                }
                rs.close();

                rs = ps.executeQuery("SELECT FOUND_ROWS()");
                if (rs.next())
                    this.noOfRecords = rs.getInt(1);
            } catch (SQLException e) {
                System.out.println("CaptureModelException: captureShopWDist()" + e);
                e.printStackTrace();
                list = null;
                return list;
            } finally {
                try {
                    if (ps != null) {
                        ps.close();
                    }
                    if (con != null) {
                        con.close();
                    }
                } catch (SQLException e) {
                    list = null;
                    return list;
                }
            }
        } else {
            System.out.println("CaptureModelException: captureShopWDist() DBConnection Not successs: Returned null");
            list = null;
        }
        return list;
    }

    /*
     * ========================================================================== */
    public boolean captureInsti(String link) {
        boolean available = false;
        String query = "select inst_name from institution where link = ? and inst_status = 'active'";
        try {
            con = db.createConnection();
            ps = con.prepareStatement(query);
            ps.setString(1, link);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                available = true;
            }
            rs.close();
        } catch (SQLException e) {
            System.out.println("CaptureModelException: captureInsti()" + e);
            e.printStackTrace();
            available = false;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                available = false;
            }
        }
        return available;
    }
	/* ========================================================================= */
}
