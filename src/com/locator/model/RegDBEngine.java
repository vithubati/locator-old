package com.locator.model;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Calendar;

import com.locator.db.ConnectionFactory;

public class RegDBEngine {

	private Connection con;
	private ConnectionFactory db = new ConnectionFactory();
	private UserAuthentication user = new UserAuthentication();
	private Subject s = new Subject();
	private AddressConverter add = new AddressConverter();
	/* =========================================================================*/
	public boolean insertInst(String catgry, String type, String nme, String no, String street, String city,
			String district, String tele1, String tele2, String fax, String shpemail,
			String website, String mail, String pswd, String descript, String status, String token, String photoPath)
			throws Exception {
		boolean rsult = false;
		PreparedStatement ps = null;
		try {
			byte[] encryptedPassword;
			byte[] salt;
			salt = user.generateSalt();
			encryptedPassword = user.getEncryptedPassword(pswd, salt);
			float latitude = 7.873054f;
			float longitude = 80.771797f;
			//get GeoPoint
			String[] latAndLong = add.getLatAndLong(no + " " + street+ " " + city+ " " + district);
			if (latAndLong != null && latAndLong.length>0) {
				latitude = Float.parseFloat(latAndLong[0]);
				longitude = Float.parseFloat(latAndLong[1]);
			}
			java.sql.Timestamp timestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
			con = db.createConnection();

			String str = "insert into institution values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			ps = con.prepareStatement(str);
			ps.setInt(1, 0);
			ps.setString(2, catgry);
			ps.setString(3, type);
			ps.setString(4, nme);
			ps.setString(5, no);
			ps.setString(6, street);
			ps.setString(7, city);
			ps.setString(8, district);
			ps.setString(9, tele1);
			ps.setString(10, tele2);
			ps.setString(11, fax);
			ps.setString(12, shpemail);
			ps.setString(13, website);
			ps.setString(14, mail);
			ps.setBytes(15, encryptedPassword);
			ps.setString(16, descript);
			ps.setTimestamp(17, timestamp); //insert date
			ps.setString(18, status);
			ps.setString(19, token);
			ps.setString(20, null); //link
			ps.setTimestamp(21, null); //status date
			ps.setString(22, photoPath);
			ps.setString(23, null);
			ps.setBytes(24, salt);
			ps.setFloat(25, latitude);
			ps.setFloat(26, longitude);
			int res = ps.executeUpdate();

			if (res > 0) {
				rsult = true;
			}
			return rsult;
		} catch (NoSuchAlgorithmException e) {
			System.out.println("RegisterModelException: insertInst()" + e);
			e.printStackTrace();
			rsult = false;
			return rsult;
		} catch (InvalidKeySpecException e) {
			System.out.println("RegisterModelException: insertInst()" + e);
			e.printStackTrace();
			rsult = false;
			return rsult;
		}catch (Exception e) {
			System.out.println("RegisterModelException: insertInst()" + e);
			e.printStackTrace();
			rsult = false;
			return rsult;
		}finally {
			try {
				if (ps != null){ps.close();}
				if (con != null){con.close();}
			} catch (SQLException e) {
				System.out.println("inside Finally block " + e);
				rsult = false;
				return rsult;
			}
		}
	}	
	/*=========================================================================*/
	public String generateBody(String path, String cat, String typ, String nme, String no, String street, String city, String district,
							   String tele1, String tel2, String fax, String shpemail, String website, String descript, String email, String status){
		String body  = s.htmlS + s.bodyS+
				"<br><table>"
				+ "<tr><td> Category</td><td>"+ cat +"</td></tr>"
				+ "<tr><td>Type</td><td>"+ typ +"</td></tr>"
				+ "<tr><td>Name</td><td>"+ nme +"</td></tr>"
				+ "<tr><td>No</td><td>"+ no +"</td></tr>"
				+ "<tr><td>Street</td><td>"+ street +"</td></tr>"
				+ "<tr><td>City</td><td>"+ city +"</td></tr>"
				+ "<tr><td>District</td><td>"+ district +"</td></tr>"
				+ "<tr><td>Tele-1</td><td>"+ tele1 +"</td></tr>"
				+ "<tr><td>Tele-2</td><td>"+ tel2 +"</td></tr>"
				+ "<tr><td>Fax</td><td>"+ fax +"</td></tr>"
				+ "<tr><td>Shop EMail</td><td>"+ shpemail +"</td></tr>"
				+ "<tr><td>Website</td><td>"+ website +"</td></tr>"
				+ "<tr><td>Description</td><td>"+ descript +"</td></tr>"
				+ "<tr><td>Email</td><td>"+ email +"</td></tr>"
				+ "<tr><td>Status</td><td>"+ status +"</td></tr>"
				+ "</table>"+
				"<br>" + "<a href='"+path+"'>"+ path +"</a>" +
				s.bodyE+s.htmlE;
		return body;
	}
}
