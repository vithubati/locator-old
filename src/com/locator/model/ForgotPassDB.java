package com.locator.model;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

import com.locator.db.ConnectionFactory;

public class ForgotPassDB {

	private Connection con;
	private ConnectionFactory db = new ConnectionFactory();
	private PreparedStatement ps;
	private Subject s = new Subject();
	private Communication com = new Communication();
	private UserAuthentication user = new UserAuthentication();
	public boolean checkAvailable(String link, String name, String rEmail) {
		boolean valid = false;
		ResultSet rs = null;
		String qry = "select inst_name, token from institution where inst_name = ? and link = ? and email = ? and inst_status = 'active'";
		try {
			con = db.createConnection();
			ps = con.prepareStatement(qry);
			ps.setString(1, name);
			ps.setString(2, link);
			ps.setString(3, rEmail);
			rs = ps.executeQuery();
			if (rs.next()) {
				valid = true;
			} else {
				valid = false;
			}
		} catch (SQLException e) {
			System.out.println("ForgotPasswordModelException: authenticate()" + e);
			e.printStackTrace();
			valid = false;
		} finally {
			try {if (ps != null) {ps.close();}
				if (con != null) {con.close();}
				if (rs != null) {rs.close();}
			} catch (SQLException e) {
				System.out.println("ForgotPasswordModelException: checkAvailable() finally block" + e);
				valid = false;
				return valid;
			}
		}
		return valid;
	}
	public boolean updateNewPassword(String link, String mail, String pass){
		boolean updated = false;
		try{
			String newToken = generateToken();
			byte[] encryptedPassword;
			byte[] salt;
			salt = user.generateSalt();
			encryptedPassword = user.getEncryptedPassword(pass, salt);
			PreparedStatement ps;
			con = db.createConnection();
			String str = "update institution set token = ?, pass_word = ?, salt_val = ?  where email = ? and link = ?";
			ps = con.prepareStatement(str);
			ps.setString(1, newToken);
			ps.setBytes(2, encryptedPassword);
			ps.setBytes(3, salt);
			ps.setString(4, mail);
			ps.setString(5, link);
			int i = ps.executeUpdate();
			if(i > 0){
				updated = true;
			}
			return updated;
		}catch(NoSuchAlgorithmException e){
			e.printStackTrace();
			System.out.println("ForgotPasswordModelException: updateNewPassword()" + e);
			e.printStackTrace();
			updated = false;
			return updated;
		}catch(InvalidKeySpecException e){
			e.printStackTrace();
			System.out.println("ForgotPasswordModelException: updateNewPassword()" + e);
			e.printStackTrace();
			updated = false;
			return updated;
		}catch(Exception e){
			System.out.println("ForgotPasswordModelException: updateNewPassword()" + e);
			e.printStackTrace();
			updated = false;
			return updated;
		}finally {
			try {if (ps != null) {ps.close();}
			if (con != null) {con.close();}
			} catch (SQLException e) {
				System.out.println("ForgotPasswordModelException: updateNewPassword() finally block" + e);
				updated = false;
				return updated;
			}
		}
	}
	public boolean updateToken(String link, String mail, String token){
		boolean updated = false;
		try{
			PreparedStatement ps;
			con = db.createConnection();
			String str = "update institution set token = ? where email = ? and link = ?";
			ps = con.prepareStatement(str);
			ps.setString(1, token);
			ps.setString(2, mail);
			ps.setString(3, link);
			int i = ps.executeUpdate();
			if(i > 0){
				updated = true;
			}
			return updated;
		}catch(Exception e){
			System.out.println("ForgotPasswordModelException: updateToken()" + e);
			e.printStackTrace();
			updated = false;
			return updated;
		}finally {
			try {
				if (ps != null){
					ps.close();
				}
				if (con != null){
					con.close();
				}
			} catch (SQLException e) {
				System.out.println("inside Finally block " + e);
				updated = false;
				return updated;
			}
		}
	}
	public boolean SendMailWithLink(String link, String mail, String reqst) {
		boolean delivered = false;
		String token = 	generateToken();
		String subject= "Locator Reset Your Password";
		boolean updToken = updateToken(link, mail, token);
		if (updToken) {
			String urlpath ="http://locator.elasticbeanstalk.com/PasswordReset?email="+ mail + "&token="+token + "&link=" 
					+ link +"&reqst=" + reqst;
			try{
				String body = makeBody(urlpath);
				if(!body.equals("")){
					delivered = com.setMail(mail, subject, body);
				}else{
					System.out.println("ForgotPasswordModelException: SendMailWithLink() Password Reset Mail has not been sent");
					delivered = false;
				}
				return delivered;
			}catch(Exception e){
				System.out.println("ForgotPasswordModelException: SendMailWithLink() "+e);
				e.printStackTrace();
				delivered = false;
				return delivered;
			}			
		} else {
			System.out.println("ForgotPasswordModelException: SendMailWithLink() Token has not been updated.");
			return delivered;
		}
		
	}
	public boolean validToken(String link, String mail, String token) {
		boolean valid = false;
		ResultSet rs = null;
		String qry = "select inst_name, district from institution where token = ? and link = ? and email = ? and inst_status = 'active'";
		try {
			con = db.createConnection();
			ps = con.prepareStatement(qry);
			ps.setString(1, token);
			ps.setString(2, link);
			ps.setString(3, mail);
			rs = ps.executeQuery();
			if (rs.next()) {
				valid = true;
			} else {
				valid = false;
			}
		} catch (SQLException e) {
			System.out.println("ForgotPasswordModelException: validToken()" + e);
			e.printStackTrace();
			valid = false;
		} finally {
			try {if (ps != null) {ps.close();}
				if (con != null) {con.close();}
				if (rs != null) {rs.close();}
			} catch (SQLException e) {
				System.out.println("ForgotPasswordModelException: validToken() finally block" + e);
				valid = false;
				return valid;
			}
		}
		return valid;		
	}
	private String makeBody(String path){ // later have to decorate 
		String body = "";
		try{
			body= s.htmlS + s.bodyS +
					s.container + 
					
					s.header + 
					"Reset Your Place Password" + 
					s.divE + 
					s.mainbody+
					" <br><p> Hi There, </p><br>"+
					s.divS+ "This email was sent automatically by Locator in response to your request to reset your password. "
							+ "" +
					s.adNme+ 
					" <p> To reset your password, Either click on the following link or copy and paste the link "
					+ "into the address bar of your browser </p> <br>"
					+ "<a href='"+path+"'>"+ path +"</a>"
					+ s.divE+
					"<p> If you didn't request to reset the password, you can simply ignore this message and this Password "
					+ "reset process will be cancelled.</p>"+
					s.divE+
					
					"<br><p>  We hope your Place ad generates the response you're looking for. Thanks for using Locator and good luck with your place. </p>"
					+ "<br><p>Regards,<br>"
					+ " The Locator Team </p><br>" +				
					s.divE+
					s.footer+
					"<p>	Copyright &copy; locator.com </p>"+
					s.divE+
					s.divE
					+ s.bodyE + s.htmlE;
			//String body = "Your AD Link; "+ domain + link;
			return body;
		}catch(Exception e){
			System.out.println("ForgotPasswordModelException	: makeBody() "+e);
			e.printStackTrace();
			return body;
		}
	}

	public String generateToken(){
		UUID token = UUID.randomUUID(); 
		return token.toString();		
	}
}
