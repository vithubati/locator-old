package com.locator.model;
import java.io.UnsupportedEncodingException;
import java.util.Properties;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class Communication {
	public String generateToken(){
		UUID token = UUID.randomUUID(); 
		return token.toString();		
	}
	
	public boolean setMail(String recip, String subject, String messageBody) {
		boolean delivered = false;
		try {
			String[] recipients = new String[] { recip };
			String[] bccRecipients = new String[] { "vithustar@yahoo.com" };
			
			delivered = new Communication().sendMail(recipients, bccRecipients, subject, messageBody);
			return delivered;			
		} catch (Exception ex) {
			System.out.println("CommunicationModelException: setMail() "+ex);
			Logger.getLogger(Communication.class.getName()).log(Level.SEVERE,
					null, ex);
			ex.printStackTrace();
			return delivered;
		}
	}
	
	private String SMTP_HOST = "smtp.gmail.com";
	private String FROM_ADDRESS = "vithubati@gmail.com";
	private String PASSWORD = "24890vinoth";
	private String FROM_NAME = "Locator.lk";

	private boolean sendMail(String[] recipients, String[] bccRecipients,
			String subject, String message) {
		try {
			Properties props = new Properties();
			props.put("mail.smtp.host", SMTP_HOST);
			props.put("mail.smtp.auth", "true");
			// props.put("mail.transport.protocol", "smtp");
			props.put("mail.debug", "false");
			props.put("mail.smtp.ssl.enable", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.port", "465");

			Session session = Session.getInstance(props, new SocialAuth());
			Message msg = new MimeMessage(session);

			InternetAddress from = new InternetAddress(FROM_ADDRESS, FROM_NAME);
			msg.setFrom(from);

			InternetAddress[] toAddresses = new InternetAddress[recipients.length];
			for (int i = 0; i < recipients.length; i++) {
				toAddresses[i] = new InternetAddress(recipients[i]);
			}
			msg.setRecipients(Message.RecipientType.TO, toAddresses);

			InternetAddress[] bccAddresses = new InternetAddress[bccRecipients.length];
			for (int j = 0; j < bccRecipients.length; j++) {
				bccAddresses[j] = new InternetAddress(bccRecipients[j]);
			}
			msg.setRecipients(Message.RecipientType.BCC, bccAddresses);

			msg.setSubject(subject);
			msg.setContent(message, "text/html; charset=utf-8");
			Transport.send(msg);
			return true;
		} catch (UnsupportedEncodingException ex) {
			System.out.println("CommunicationModelException: sendMail() "+ex);
			Logger.getLogger(Communication.class.getName()).log(Level.SEVERE,
					null, ex);
			ex.printStackTrace();
			return false;

		} catch (MessagingException ex) {
			System.out.println("CommunicationModelException: sendMail() "+ex);
			Logger.getLogger(Communication.class.getName()).log(Level.SEVERE,
					null, ex);
			ex.printStackTrace();
			return false;
		}
	}

	class SocialAuth extends Authenticator {
		@Override
		protected PasswordAuthentication getPasswordAuthentication() {
			return new PasswordAuthentication(FROM_ADDRESS, PASSWORD);
		}
	}


}
