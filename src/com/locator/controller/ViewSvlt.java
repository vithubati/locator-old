package com.locator.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.locator.dto.Institution;
import com.locator.model.ViewDB;

/**
 * Servlet implementation class ViewSvlt
 */

public class ViewSvlt extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private ViewDB view = new ViewDB();
//	private ReviewDB review = new ReviewDB();
    public ViewSvlt() {
        super();
    }

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		try {
			String dataa = request.getPathInfo();
			//System.out.println(request.getRequestURI());
			if(dataa != null){
				//System.out.println(dataa);
				String url = dataa.substring(1);
				System.out.println(url);
				if(!url.trim().equals("")){
					List<Institution> lst = view.captureShop(url);
					int page = 1;
					//int recordsPerPage = 5;
					String link = url;
					if (request.getParameter("page") != null)
						page = Integer.parseInt(request.getParameter("page"));
					if (request.getParameter("linkparam") != null)
						link = request.getParameter("linkparam");
					//List<Review> rlstList = review.captureReviews(link, (page - 1) * recordsPerPage, recordsPerPage, page);
					//int noOfRecords = review.getNoOfRecords();
					//System.out.println("No of records=" + noOfRecords);
					if (!lst.isEmpty()) {
						request.setAttribute("instiList", lst);
						request.setAttribute("URL", url);
					//}
					//if (!rlstList.isEmpty()) {
						//int noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
						request.setAttribute("instiList", lst);
						//request.setAttribute("reviewList", rlstList);
						request.setAttribute("URL", url);
						//request.setAttribute("noOfPages", noOfPages);
						request.setAttribute("currentPage", page);
						request.setAttribute("link", link);
						request.setAttribute("data", "yes");
						request.getRequestDispatcher("/views.jsp").forward(request, response);
					}else {
						request.setAttribute("instiError", "noData");
						request.getRequestDispatcher("/views.jsp").forward(request, response);
					}
				}else{
					request.setAttribute("instiError", "noData");
					request.getRequestDispatcher("/views.jsp").forward(request, response);
				}
			}else{
				request.setAttribute("instiError", "noData");
				request.getRequestDispatcher("/views.jsp").forward(request, response);
			}
			
		}catch (Exception ex) {
			System.out.println("ViewServletException: "+ex);
			request.setAttribute("instiError", "error");
			request.getRequestDispatcher("/views.jsp").forward(request,response);
		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

}
