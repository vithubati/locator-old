package com.locator.controller;

import java.io.IOException;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.locator.model.DeleteDB;

public class DeleteSvlt extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private DeleteDB del = new DeleteDB();
	private final String DEACTIVATE = "deactivate";
	public DeleteSvlt() {
		super();
	}

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		boolean success = false;
		try {
			if (request.getParameter("deletebtn") != null) {
				String url = request.getParameter("url").trim();
				String name = request.getParameter("name").trim();
				String paswrd = request.getParameter("passtxt").trim();
				boolean valid = del.authenticate(url, name, paswrd);
				if (valid) {
					success = true;
					request.setAttribute("link", url);
					request.setAttribute("nme", name);
					request.setAttribute("valid", success);
					request.getRequestDispatcher("/delete.jsp").forward(request, response);
				} else {
					success = false;
					request.setAttribute("link", url);
					request.setAttribute("nme", name);
					request.setAttribute("valid", success);
					request.getRequestDispatcher("/delete.jsp").forward(request, response);
				}
			}else if (request.getParameter("confirm") != null) {
				String url = request.getParameter("url").trim();
				String name = request.getParameter("name").trim();
				ResultSet deleted = del.checkDeleted(name, url); 
				String stats = "";
				if (deleted != null) {
					while (deleted.next()) {
						stats = deleted.getString(1);
					}
				}
				if(stats.equals("deactivate")){
					request.setAttribute("delete", "already");
					request.getRequestDispatcher("/delete.jsp").forward(request, response);
				}else{
					boolean set = del.setStatus(DEACTIVATE, name, url);
					if (set) {
						request.setAttribute("delete", "success");
						request.getRequestDispatcher("/delete.jsp").forward(request, response);
					}else{
						request.setAttribute("delete", "fail");
						request.getRequestDispatcher("/delete.jsp").forward(request, response);
					}
				}
			}else{
				success = false;
				request.setAttribute("valid", success);
				request.setAttribute("delError", "error");
				request.getRequestDispatcher("/views.jsp").forward(request,	response);
			}
		}catch(NullPointerException ex){
			System.out.println("DeleteServletException: "+ex);
			success = false;
			request.setAttribute("valid", success);
			request.setAttribute("delError", "badRequest");
			request.getRequestDispatcher("/views.jsp").forward(request,	response);
		}
		catch (Exception ex) {
			System.out.println("DeleteServletException: "+ex);
			success = false;
			request.setAttribute("valid", success);
			request.setAttribute("delError", "error");
			request.getRequestDispatcher("/views.jsp").forward(request,	response);
		}
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/index.html").forward(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

}
