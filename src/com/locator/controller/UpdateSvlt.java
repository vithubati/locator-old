package com.locator.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.locator.dto.Institution;
import com.locator.model.DeleteDB;
import com.locator.model.UpdateDB;

/**
 * Servlet implementation class UpdateSvlt
 */
/*@WebServlet("/Updation")*/
public class UpdateSvlt extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UpdateDB udb = new UpdateDB();
	/*private VerificationDB verf = new VerificationDB();
	private final String ACTIVE = "active";
	private final String INACTIVE = "inactive";
	private final String EMAIL = "vithu123@hotmail.com";
	private final String SUBJECT1 = "New Registration";
	private final String AWAIT = "await";*/
	private DeleteDB del = new DeleteDB();
    public UpdateSvlt() {
        super();
        // TODO Auto-generated constructor stub
    }

    protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		boolean success = false;
		try {
			if (request.getParameter("editbtn") != null) {
				String url = request.getParameter("url").trim();
				String name = request.getParameter("name").trim();
				String paswrd = request.getParameter("passtxt").trim();
				boolean valid = del.authenticate(url, name, paswrd);
				if (valid) {
					List<Institution> lst = udb.captureShop(name, url);
					if(!lst.isEmpty())
					success = true;
					request.setAttribute("instiList", lst);
					request.setAttribute("link", url);
					request.setAttribute("nme", name);
					request.setAttribute("validUpd", success);
					request.getRequestDispatcher("/update.jsp").forward(request, response);
				} else { // invalid credential
					success = false;
					request.setAttribute("link", url);
					request.setAttribute("nme", name);
					request.setAttribute("validUpd", success);
					request.getRequestDispatcher("/dispatch.jsp").forward(request, response);
				}
			}else{
				success = false;
				request.setAttribute("validUpd", success);
				request.setAttribute("delError", "error");
				request.getRequestDispatcher("/views.jsp").forward(request,	response);
			}
		}catch(NullPointerException ex){
			System.out.println("UpdateServletException: "+ex);
			success = false;
			request.setAttribute("valid", success);
			request.setAttribute("delError", "badRequest");
			request.getRequestDispatcher("/views.jsp").forward(request,	response);
		}
		catch (Exception ex) {
			System.out.println("UpdateServletException: "+ex);
			success = false;
			request.setAttribute("valid", success);
			request.setAttribute("delError", "error");
			request.getRequestDispatcher("/views.jsp").forward(request,	response);
		}
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/index.html").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

}
