package com.locator.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.locator.dto.Institution;
import com.locator.model.Capture;
/**
 * Servlet implementation class Search
 */

public class SearchSvlt extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Capture capt = new Capture();
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SearchSvlt() {
		super();
		// TODO Auto-generated constructor stub
	}

	@SuppressWarnings("unused")
	private void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			int page = 1;
			int recordsPerPage = 5;
			if (request.getParameter("page") != null)
				page = Integer.parseInt(request.getParameter("page"));
			String place = request.getParameter("destrict");
			List<Institution> lst = capt.captureShopWDist(place, (page - 1)
					* recordsPerPage, recordsPerPage);
			int noOfRecords = capt.getNoOfRecords();
			System.out.println("No of records=" + noOfRecords);
			if (lst != null) {
				int noOfPages = (int) Math.ceil(noOfRecords * 1.0
						/ recordsPerPage);
				request.setAttribute("instituteList", lst);
				request.setAttribute("noOfPages", noOfPages);
				request.setAttribute("currentPage", page);
				request.setAttribute("currentDist", place);
				// request.setAttribute("find", lst);
				System.out.println("Page = " + page + " noOfPages ="
						+ noOfPages);
				request.getRequestDispatcher("display.jsp").forward(request,
						response);
			} else {
				request.setAttribute("Error", "noData");
				request.getRequestDispatcher("display.jsp").forward(request,
						response);
			}

		} catch (Exception ex) {
			System.out.println("SearchServletException: "+ex);
			request.setAttribute("Error", "error");
			request.getRequestDispatcher("display.jsp").forward(request, response);
		}

	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		//processRequest(request, response);
		response.sendRedirect("index.html");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		//processRequest(request, response);
		response.sendRedirect("index.html");
	}

}
