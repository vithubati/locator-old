package com.locator.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.locator.dto.Institution;
import com.locator.model.Capture;

/**
 * Servlet implementation class CaptureSvlt
 */

public class CaptureSvlt extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Capture capt = new Capture();
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CaptureSvlt() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			
			if (request.getParameter("btfind") != null) {
				int page = 1;
				int recordsPerPage = 5;
				if (request.getParameter("page") != null)
					page = Integer.parseInt(request.getParameter("page").trim());
				String keyword = "";
				keyword = request.getParameter("txtfind").trim();
				if (keyword ==null) {
					keyword = "";
				}
				String place = "";
				place = request.getParameter("district").trim();
				if (place ==null) {
					place = "none";
				}
				List<Institution> lst = capt.captureshops(keyword, place,
						(page - 1) * recordsPerPage, recordsPerPage);
				int noOfRecords = capt.getNoOfRecords();
				//System.out.println("records ="+ noOfRecords);
				//System.out.println("No of records=" + noOfRecords);
				if (lst != null) {
					if (!lst.isEmpty()) {
						int noOfPages = (int) Math.ceil(noOfRecords * 1.0
								/ recordsPerPage);
						request.setAttribute("instituteList", lst);
						request.setAttribute("noOfPages", noOfPages);
						request.setAttribute("currentPage", page);
						request.setAttribute("currentDist", place);
						request.setAttribute("currentKey", keyword);
						// request.setAttribute("find", lst);
						// System.out.println("Page = " + page + " noOfPages ="+
						// noOfPages);
						request.getRequestDispatcher("/display.jsp").forward(
								request, response);
					} else {
						System.out.println("Nodata: for " + keyword);
						request.setAttribute("currentKey", keyword);
						request.setAttribute("Error", "noData");
						request.getRequestDispatcher("display.jsp").forward(
								request, response);
					}
				}else{
					System.out.println("CaptureServletException: Might be DB Error");
					request.setAttribute("Error", "error");
					request.getRequestDispatcher("display.jsp").forward(request,response);
				}
			}else {
				int page = 1;
				int recordsPerPage = 5;
				if (request.getParameter("page") != null)
					if(!request.getParameter("page").trim().equals(""))
						page = Integer.parseInt(request.getParameter("page").trim());
				String  place = "";
				if (request.getParameter("destrict") != null)
					place = request.getParameter("destrict").trim();
				String key ="";
				if (request.getParameter("keyword") != null)
					key = request.getParameter("keyword").trim();
				if (key == null || key.equals("")) {
					List<Institution> lst = capt.captureShopWDist(place,
							(page - 1) * recordsPerPage, recordsPerPage);
					int noOfRecords = capt.getNoOfRecords();
					//System.out.println("No of records=" + noOfRecords);
					if (lst != null) {
						if (!lst.isEmpty()) {
							int noOfPages = (int) Math.ceil(noOfRecords * 1.0
									/ recordsPerPage);
							request.setAttribute("instituteList", lst);
							request.setAttribute("noOfPages", noOfPages);
							request.setAttribute("currentPage", page);
							request.setAttribute("currentDist", place);
							// request.setAttribute("find", lst);
							// System.out.println("Page = " + page +
							// " noOfPages ="+ noOfPages);
							request.getRequestDispatcher("display.jsp")
									.forward(request, response);
						} else {
							request.setAttribute("currentKey", place);
							request.setAttribute("Error", "noData");
							request.getRequestDispatcher("display.jsp")
									.forward(request, response);
						}
					}else{
						System.out.println("CaptureServletException: Might be DB Error");
						request.setAttribute("Error", "error");
						request.getRequestDispatcher("display.jsp").forward(request,response);
					}

				} else {
					List<Institution> lst = capt.captureshops(key, place,
							(page - 1) * recordsPerPage, recordsPerPage);
					int noOfRecords = capt.getNoOfRecords();
					//System.out.println("No of records=" + noOfRecords);
					if (!lst.isEmpty()) {
						int noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
						request.setAttribute("instituteList", lst);
						request.setAttribute("noOfPages", noOfPages);
						request.setAttribute("currentPage", page);
						request.setAttribute("currentDist", place);
						request.setAttribute("currentKey", key);
						// request.setAttribute("find", lst);
						//System.out.println("Page = " + page + " noOfPages ="+ noOfPages);
						request.getRequestDispatcher("display.jsp").forward(request, response);
					} else {
						System.out.println("AppException: Nodata CaptureServlet for key: " + key);
						request.setAttribute("currentKey", key);
						request.setAttribute("Error", "noData");
						request.getRequestDispatcher("display.jsp").forward(request, response);
					}
				}
			}
		}catch(NumberFormatException ex)  {
			System.out.println("CaptureServletException: "+ex);
			ex.printStackTrace();
			request.setAttribute("Bad", "BadRequest");
			request.getRequestDispatcher("display.jsp").forward(request,response);
		}catch (Exception ex) {
			System.out.println("CaptureServletException: "+ex);
			ex.printStackTrace();
			request.setAttribute("Error", "error");
			request.getRequestDispatcher("display.jsp").forward(request,response);
		}
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);

	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

}
