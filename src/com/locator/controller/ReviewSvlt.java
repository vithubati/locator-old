package com.locator.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.locator.dto.Review;
import com.locator.model.ReviewDB;

public class ReviewSvlt extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private final String GOOD = "good";
	//private final String SPAM = "spam";
	private ReviewDB rdb = new ReviewDB();
    public ReviewSvlt() {
        super();
        // TODO Auto-generated constructor stub
    }
    protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
    	response.setContentType("text/html");
		try {
			int rating = 3;
			String url = "";
			String coment = "";
			boolean saved = false;
			String buton= "";
			if(request.getParameter("butn") != null)
				buton= request.getParameter("butn");
			if(request.getParameter("link")!= null)
				url= request.getParameter("link");
			if(request.getParameter("rat") != null )
				rating= Integer.parseInt(request.getParameter("rat"));
			if(request.getParameter("com") != null)
				coment = request.getParameter("com");
			if (buton.equals("Save")) {
				saved = rdb.insertReview(coment, rating, url, GOOD);
				if(saved){
					response.setContentType("text/plain");
					response.setCharacterEncoding("UTF-8");
					response.getWriter().write("success");
				}else{
					response.setContentType("text/plain");
					response.setCharacterEncoding("UTF-8");
					response.getWriter().write("fail");
				}
			}else{ /*must redirect to empty page saying 404*/
				int page = 1;
				int recordsPerPage = 5;
				if (request.getParameter("page") != null)
					page = Integer.parseInt(request.getParameter("page"));
				String link = request.getParameter("linkparam");
				if(link.equals("")){
					request.setAttribute("link", link);
					response.setContentType("text/plain");
					response.setCharacterEncoding("UTF-8");
					response.getWriter().write("fail");
				}else{
					List<Review> lst = rdb.captureReviews(link, (page - 1) * recordsPerPage, recordsPerPage, page);
					/*int noOfRecords = rdb.getNoOfRecords();*/
					//System.out.println("No of records=" + noOfRecords);
					if (!lst.isEmpty()) { // also check for null values
						String json = new Gson().toJson(lst);
						response.setContentType("application/json");
				        response.setCharacterEncoding("UTF-8");
				        response.getWriter().write(json);
						/*int noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
						request.setAttribute("instituteList", lst);
						request.setAttribute("link", link);
						// request.setAttribute("find", lst);
*/						/*System.out.println("Page = " + page + " noOfPages ="+ noOfPages);
						request.getRequestDispatcher("/views.jsp").forward(request, response); */
					}else {
						request.setAttribute("link", link);
						response.setContentType("text/plain");
						response.setCharacterEncoding("UTF-8");
						response.getWriter().write("fail");
					}
				}
			}
		}catch (Exception ex) {
			System.out.println("ReviewServletException: "+ ex);
			response.setContentType("text/plain");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write("fail");
		}
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/index.html").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

}
