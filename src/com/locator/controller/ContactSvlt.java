package com.locator.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.locator.model.ContactDB;

/**
 * Servlet implementation class ContactSvlt
 */
public class ContactSvlt extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private ContactDB cdb = new ContactDB();
    public ContactSvlt() {
        super();
        // TODO Auto-generated constructor stub
    }
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
		response.setContentType("text/html");
		try {
			request.getSession().removeAttribute("contMsg");
			if(request.getParameter("send") != null){
				String fname = request.getParameter("fname").trim();
				String lname = request.getParameter("lname").trim();
				String email = request.getParameter("email").trim();
				String subject = request.getParameter("subject").trim();
				String message = request.getParameter("message").trim();
				long phone = 0;
				System.out.println("vashmi");
				if(request.getParameter("phone") != null )
					phone= Long.parseLong(request.getParameter("phone"));
				System.out.println("vithu");
				boolean saved =false;
				saved= cdb.insertMessage(fname, lname, email, phone, subject, message);
				if (saved) {
					request.getSession().setAttribute("contMsg", "success");
				} else {
					request.getSession().setAttribute("contMsg", "fail");
				}
				//request.getRequestDispatcher("contactus.jsp").forward(request, response);
				response.sendRedirect("contactus.jsp");
			}
		} catch (Exception e) {
			System.out.println("ContactServletException: " + e);
			request.setAttribute("contMsg", "fail");
			request.getRequestDispatcher("contactus.jsp").forward(request, response);
		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/index.html").forward(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

}
