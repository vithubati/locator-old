package com.locator.controller;

import java.io.IOException;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.locator.model.ActiveDB;
import com.locator.model.UpdateDB;
import com.locator.model.VisitCounterDB;

/**
 * Servlet implementation class ActiveSvl
 */
@WebServlet("/Activation")
public class ActiveSvl extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private final String ACTIVE = "active";
	private final String INACTIVE = "inactive";
	private final String AWAIT = "await";
	private final String UPDATED = "updated";
	ActiveDB act = new ActiveDB();
	private UpdateDB udb = new UpdateDB();
	private VisitCounterDB vc = new VisitCounterDB();

	public ActiveSvl() {
		super();
	}

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		try {
			String email = request.getParameter("email").trim();
			String token = request.getParameter("token").trim();
			String reqst = request.getParameter("reqst").trim();
			/*
			 * String url = "vv"; if(request.getParameter("link")!= null) url=
			 * request.getParameter("link");
			 */

			if (reqst.equals("regist")) {
				ResultSet rslt = act.VerifyToken(email, token);
				String stats = "";
				if (rslt != null) {
					while (rslt.next()) {
						stats = rslt.getString(1);
					}
					if (stats.equals(ACTIVE)) {
						request.setAttribute("status", "alreadyVarified");
						request.getRequestDispatcher("approval.jsp").forward(
								request, response);
					} else if (stats.equals(INACTIVE)) {
						boolean set = act.setStatus(ACTIVE, email, token);
						if (set) {
							// send user saying activated with ad link
							String link = act.getURL(email, token);
							if (link != null) {
								boolean urlUpdated = act.setURL(link, email,
										token);
								boolean counterStarted = vc.insertCount(link, email);
								if(counterStarted){
									request.setAttribute("counter","counterStarted");
								}else{
									System.out.println("Exception: VC Not Started");
									request.setAttribute("counter","counterfailed");
								}
								if (urlUpdated) {
									boolean sent = act.SendMailWithLink(link, email);
									if (sent) {
										request.setAttribute("status", "sent");	request.getRequestDispatcher("approval.jsp").forward(request, response);
									} else {
										System.out.println("ActiveUpdatedMsgNotSent");
										request.setAttribute("status","notSent");
										request.getRequestDispatcher("approval.jsp").forward(request, response);
									}
								} else {
									System.out.println("Exception: ActiveUrlNotUpdated");
									request.setAttribute("status","urlUpdateError");
									request.getRequestDispatcher("approval.jsp").forward(request, response);
								}
							}else {
								System.out.println("Exception: ActiveUrlNotGenerated");
								request.setAttribute("status", "linkError");
								request.getRequestDispatcher("approval.jsp").forward(request, response);
							}
						}else {
							System.out.println("Exception: ActiveStatusNotSet");
							request.setAttribute("status", "updateError");
							request.getRequestDispatcher("approval.jsp").forward(request, response);
						}

					} else {
						System.out.println("Exception: ActiveInvalidURL");
						request.setAttribute("status", "invalid");
						request.getRequestDispatcher("approval.jsp").forward(request, response);
					}
				} else {
					System.out.println("Exception: ActiveInvalidURL");
					request.setAttribute("status", "invalid");
					request.getRequestDispatcher("approval.jsp").forward(request, response);
				}
			} else if (reqst.equals("update") && (!reqst.equals(""))) {
				ResultSet rslt = udb.verifyTempToken(email, token);
				String stats = "";
				String lnk = "";
				if (rslt != null) {
					while (rslt.next()) {
						stats = rslt.getString(1);
						lnk = rslt.getString(2);
					}if(stats.equals(UPDATED)){
						request.setAttribute("status", "alreadyUpdated");
						request.getRequestDispatcher("approval.jsp").forward(
								request, response);
					}else if (stats.equals(AWAIT)) {
						boolean set = udb.setStatus(UPDATED, email, token);
						if(set){
							boolean updated = udb.doUpdate(email, token);
							if (updated) {
								boolean sent = udb.sendMailWithLink(lnk, email);
								if (sent) {
									request.setAttribute("status", "sent");
									request.getRequestDispatcher("approval.jsp").forward(request, response);
								} else {
									request.setAttribute("status", "notSent");
									request.getRequestDispatcher("approval.jsp").forward(request, response);
								}
							} else {
								System.out.println("Exception: ActiveDataNotUpdated");
								request.setAttribute("status", "UpdateError");
								request.getRequestDispatcher("approval.jsp").forward(request, response);
							}
						}else{
							System.out.println("Exception: ActiveStatusNotSet");
							request.setAttribute("status", "updateError");
							request.getRequestDispatcher("approval.jsp").forward(request, response);
						}
					} else {
						request.setAttribute("status", "invalid");
						request.getRequestDispatcher("approval.jsp").forward(request, response);
					}
				}
			}else{
				response.sendRedirect("index.html");
			}

		} catch (Exception ex) {
			System.out.println("LocatorActivationException:  "+ ex);
			request.setAttribute("status", "error");
			request.getRequestDispatcher("approval.jsp").forward(request,response);
		}
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

}
