package com.locator.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.locator.model.ForgotPassDB;

public class ForgotPassSvlt extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private ForgotPassDB pass =new ForgotPassDB();  
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ForgotPassSvlt() {
        super();
        // TODO Auto-generated constructor stub
    }
	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		boolean success = false;
		Map<String, String> messages = new HashMap<String, String>();
		request.setAttribute("errorMessages", messages);
		final String regex = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
		 
		
		try {
			if (request.getParameter("forgotbtn") != null) {
				String url = request.getParameter("url").trim();
				String name = request.getParameter("name").trim();
				String rEmail = request.getParameter("txtemail").trim();
				
				if (url == null || url.trim().isEmpty()) {
		            messages.put("url", "invalid");
		        }if (name == null || name.trim().isEmpty()) {
		            messages.put("name", "invalid");
		        }if (rEmail == null || rEmail.trim().isEmpty()) {
		            messages.put("rEmail", "");
		        }else{
		        	Pattern pattern = Pattern.compile(regex);
			        Matcher matcher = pattern.matcher(rEmail);
			        if (!matcher.matches()) {
			        	messages.put("rEmail", "Invalid Email");
			        }
		        }		        
		        if (messages.isEmpty()) {
		        	boolean valid = pass.checkAvailable(url, name, rEmail);
					if(valid){
						String reqst = "resetPass";
						boolean sent = pass.SendMailWithLink(url, rEmail, reqst);
						if (sent) {
							success = true;
							request.setAttribute("valid", success);
							request.setAttribute("rEmail", rEmail);
							request.getRequestDispatcher("/PasswordUpdate").forward(request,	response);
						}else{
							request.setAttribute("valid", success);
							request.getRequestDispatcher("/PasswordUpdate").forward(request,	response);
						}
					}
					else{
						success = false;
						request.setAttribute("link", url);
						request.setAttribute("nme", name);
						request.setAttribute("available", valid);
						request.getRequestDispatcher("/PasswordUpdate").forward(request, response);
					}
		        }else{
		        	request.setAttribute("validPsw", false);
		        	request.setAttribute("link", url);
					request.setAttribute("nme", name);
		        	request.getRequestDispatcher("/PasswordUpdate").forward(request, response);
		        }
			
			}else if (request.getParameter("resetbtn") != null) {
				String emil = request.getParameter("acemail").trim();
				String passwrd = request.getParameter("npasstxt").trim();
				String cPasswrd = request.getParameter("cnpasstxt").trim();
				String lnk = request.getParameter("link").trim();
				//request.setAttribute("errorMessages", messages);
				if (emil == null || emil.trim().isEmpty()) {
		            messages.put("url", "invalid");
		        }if (passwrd == null || passwrd.trim().isEmpty()) {
		            messages.put("rstPass", "Both Fields should be entered");
		        }else if (cPasswrd == null || cPasswrd.trim().isEmpty()) {
		            messages.put("rstPass", "Both Fields should be entered");
		        }else if(passwrd.length() < 5){
		        	messages.put("rstPass", "Password must contains atleast 6 characters!");
		        }else if(!cPasswrd.equals(passwrd)){
		        	messages.put("rstPass", "Passwords did not match. ");
		        }		        
		        if (messages.isEmpty()) {
		        	boolean updTokn = pass.updateNewPassword(lnk, emil, passwrd);
					if (updTokn) {
						request.setAttribute("passReset", "success");
						request.setAttribute("url", lnk);
						request.getRequestDispatcher("/PasswordUpdate").forward(request,	response);
					} else {
						request.setAttribute("resetError", "error");
						request.getRequestDispatcher("/PasswordUpdate").forward(request,	response);
					}
		        }else{
		        	request.setAttribute("passValidated", "success");
		        	request.setAttribute("link", lnk);
					request.setAttribute("email", emil);
		        	request.getRequestDispatcher("/PasswordUpdate").forward(request, response);
		        }				
			}else{
				response.sendRedirect("index.html");
			}
		}catch(NullPointerException ex){
			System.out.println("ForgotPasswordServletException: "+ex);
			success = false;
			request.setAttribute("valid", success);
			request.setAttribute("resetError", "badRequest");
			request.getRequestDispatcher("/PasswordUpdate").forward(request,	response);
		}catch (Exception ex) {
			System.out.println("ForgotPasswordServletException: "+ex);
			success = false;
			request.setAttribute("valid", success);
			request.setAttribute("resetError", "error");
			request.getRequestDispatcher("/PasswordUpdate").forward(request,	response);
		}
	}
	protected void getProcessRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		boolean success = false;
		try {
			if (request.getParameter("reqst") != null){
				String reqst = request.getParameter("reqst").trim();
				if (reqst.equals("resetPass")) {
					String emil = request.getParameter("email").trim();
					String tokn = request.getParameter("token").trim();
					String lnk = request.getParameter("link").trim();
					boolean validToken = pass.validToken(lnk, emil, tokn);
					if(validToken){
						request.setAttribute("email", emil);
						request.setAttribute("url", lnk);
						request.setAttribute("passValidated", "success");
						request.getRequestDispatcher("/PasswordUpdate").forward(request,	response);
					}else{
						//invalid token
						request.setAttribute("invalidToken", "true");
						request.getRequestDispatcher("/PasswordUpdate").forward(request,	response);
					}										
				}else{
					response.sendError(HttpServletResponse.SC_BAD_REQUEST);
				}				
			}else{
				response.sendRedirect("index.html");
			}
		}catch(NullPointerException ex){
			System.out.println("ForgotPasswordServletException: "+ex);
			success = false;
			request.setAttribute("valid", success);
			request.setAttribute("resetError", "badRequest");
			request.getRequestDispatcher("/PasswordUpdate").forward(request,	response);
		}catch (Exception ex) {
			System.out.println("ForgotPasswordServletException: "+ex);
			success = false;
			request.setAttribute("valid", success);
			request.setAttribute("resetError", "error");
			request.getRequestDispatcher("/PasswordUpdate").forward(request,	response);
		}
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		getProcessRequest(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

}
