package com.locator.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.locator.model.VisitCounterDB;

/**
 * Servlet implementation class VisitCounterSvlt
 */
public class VisitCounterSvlt extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private VisitCounterDB vc =new VisitCounterDB();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public VisitCounterSvlt() {
        super();
    }
    protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		try {
			String link = request.getParameter("url").trim();
			String host = null;
			int counted = 0;
			boolean cleared = vc.clearData(host, link);
			if(!cleared){
			System.out.println("AppException insideCleardata VisitCounterModel");
			}
			host= getClientIpAddr(request);
			if(host!= null){
				counted = vc.makeCount(host, link);
			}else {
				System.out.println("AppException: VisitCounterServlet ClientHostNotTaken");
			}
			if(counted > 0){
				response.setContentType("text/plain");
				response.setCharacterEncoding("UTF-8");
				response.getWriter().print(counted);
			}else{
				response.setContentType("text/plain");
				response.setCharacterEncoding("UTF-8");
				response.getWriter().write("fail");
			}				
		}catch (Exception ex) {
			System.out.println("VisitCounterServletException: "+ ex);
			response.setContentType("text/plain");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write("error");
		}
    }

	private static String getClientIpAddr(HttpServletRequest request) {
		String ip = null;
		try {
			ip = request.getHeader("X-Forwarded-For");
			if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
				ip = request.getHeader("Proxy-Client-IP");
			}if (ip == null || ip.length() == 0	|| "unknown".equalsIgnoreCase(ip)) {
				ip = request.getHeader("WL-Proxy-Client-IP");
			}if (ip == null || ip.length() == 0	|| "unknown".equalsIgnoreCase(ip)) {
				ip = request.getHeader("HTTP_CLIENT_IP");
			}if (ip == null || ip.length() == 0	|| "unknown".equalsIgnoreCase(ip)) {
				ip = request.getHeader("HTTP_X_FORWARDED_FOR");
			}if (ip == null || ip.length() == 0	|| "unknown".equalsIgnoreCase(ip)) {
				ip = request.getRemoteAddr();
			}
		} catch (Exception ex) {
			System.out.println("Exception: getClientIpAddr() "+ ex);
			ip = null;
			return ip;
		}
		return ip;
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/index.html").forward(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

}
