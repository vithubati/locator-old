package com.locator.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.locator.model.ReportDB;

public class ReportSvlt extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private ReportDB rd = new ReportDB();
    public ReportSvlt() {
        super();
    }

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
		response.setContentType("text/html");
		String url = request.getParameter("url").trim();
		try {
			request.getSession().removeAttribute("RepPlc");
			if(request.getParameter("reportsub") != null){
				
				String email = request.getParameter("email").trim();
				String subject = request.getParameter("reason").trim();
				String message = request.getParameter("message").trim();
				String urls = request.getParameter("url").trim();
				boolean saved =false;
				saved= rd.insertReport(email, subject, message, urls);
				if (saved) {
					request.getSession().setAttribute("RepPlc", "success");
				} else {
					request.getSession().setAttribute("RepPlc", "fail");
				}
				//request.getRequestDispatcher("/view/"+ urls).forward(request, response);
				response.sendRedirect("view/"+ url);
			}
		} catch (Exception e) {
			System.out.println("ReportServletException: " + e);
			request.getSession().setAttribute("RepPlc", "error");
			response.sendRedirect("view/"+ url);
		}
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/index.html").forward(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

}
