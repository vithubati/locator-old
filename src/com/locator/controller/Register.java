package com.locator.controller;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.locator.model.RegDBEngine;
import com.locator.model.UpdateDB;
import com.locator.model.Communication;

/**
 * Servlet implementation class Register
 */
@MultipartConfig
public class Register extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private RegDBEngine rdb = new RegDBEngine();
	private Communication verf = new Communication();
	/*private final String ACTIVE = "active";*/
	private final String INACTIVE = "inactive";
	private final String EMAIL = "vithu123@hotmail.com";
	private final String SUBJECT1 = "New Registration";
	private final String SUBJECT2 = "Updation";
	private UpdateDB udb = new UpdateDB();
	private final String AWAIT = "await";
	private String dataPath = "/home/bati/Documents/uploads"; //D:/uploads  /var/locator/images
	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		Map<String, String> messages = new HashMap<String, String>();
		final String regex = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
		try {
			if (request.getParameter("submit") != null) {
				String token = 	verf.generateToken();
				String cat = request.getParameter("category").trim();
				String typ = request.getParameter("type").trim();
				String nme = request.getParameter("shopName").trim();
				String no = request.getParameter("no").trim();
				String street = request.getParameter("street").trim();
				String city = request.getParameter("city").trim();
				String district = request.getParameter("dists").trim();
				String tel1 =request.getParameter("telephone1").trim();
				String tel2 = request.getParameter("telephone2").trim();
				String fx = request.getParameter("fax").trim();
				String tele1 = ""; String tele2 = ""; String fax = "";
				String shpemail = null;
				shpemail = request.getParameter("shopEmail").trim();
				String website = null;
				website = request.getParameter("website").trim();
				String mail= request.getParameter("email").trim();
				String pswd = request.getParameter("password").trim();
				String cPswd = request.getParameter("cpassword").trim();
				String descript = request.getParameter("description").trim();
				  // obtains the upload file part in this multipart request
		        Part filePart =null;
		        filePart = request.getPart("photo");
		        
		        if (cat == null || cat.trim().isEmpty()) {
		            messages.put("emptyF", "This Field Must not be empty");
		        }if (typ == null || typ.trim().isEmpty()) {
		            messages.put("emptyF", "This Field Must not be empty");
		        }if (nme == null || nme.trim().isEmpty()) {
		            messages.put("emptyF", "This Field Must not be empty");
		        }if (no == null || no.trim().isEmpty()) {
		            messages.put("emptyF", "This Field Must not be empty");
		        }if (street == null || street.trim().isEmpty()) {
		            messages.put("emptyF", "This Field Must not be empty");
		        }if (city == null || city.trim().isEmpty()) {
		            messages.put("emptyF", "This Field Must not be empty");
		        }if (district == null || district.trim().isEmpty()) {
		            messages.put("emptyF", "This Field Must not be empty");
		        }if (tel1 == null || tel1.trim().isEmpty()) {
		            messages.put("tele1", "This Field Must not be empty");
		        }if (mail == null || mail.trim().isEmpty()) {
		            messages.put("rEmail", "This Field Must not be empty");
		        }if (pswd == null || pswd.trim().isEmpty()) {
		            messages.put("passChar", "This Field Must not be empty");
		        }if (cPswd == null || cPswd.trim().isEmpty()) {
		            messages.put("passChar", "This Field Must not be empty");
		        }if (descript == null || descript.trim().isEmpty()) {
		            messages.put("emptyF", "This Field Must not be empty");
		        }else{
		        	if(pswd.length() < 5 ||  cPswd.length() < 5){
			        	messages.put("passChar", "Password must contains atleast 6 characters!");
			        }else if(!pswd.equals(cPswd)){
			        	messages.put("passChar", "Passwords did not match. ");
			        }if((mail != null) || (!mail.equals(""))){
			        	Pattern pattern = Pattern.compile(regex);
				        Matcher matcher = pattern.matcher(mail);
				        if (!matcher.matches()) {
				        	messages.put("rEmail", "Invalid Email");
				        }
			        }if((shpemail != null) || (!shpemail.equals(""))){
			        	Pattern pattern = Pattern.compile(regex);
				        Matcher matcher = pattern.matcher(mail);
				        if (!matcher.matches()) {
				        	messages.put("sEmail", "Invalid Email");
				        }
			        }
			        
			        if (!tel1.equals("")){
				        if(!tel1.matches("\\d{10}") || tel1.length() >10 || tel1.length() <9){
				        	messages.put("tele1F", "Invalid Phone Number");
				        }
			        }
					if (!tel2.equals("")){
						if(!tel2.matches("\\d{10}") || tel2.length() >10 || tel2.length() <9){
				        	messages.put("tele2F", "Invalid Phone Number");
				        }
					}
					if(!fx.equals("")){
						if(!fx.matches("\\d{10}") || fx.length() >10 || fx.length() <9){
				        	messages.put("faxF", "Invalid Fax Number");
				        }
					}
					if (filePart != null) {
						if(filePart.getSize() > 5125){
							messages.put("photoF", "Image Size Must be less than 5mb");
						}							
					}			        
		        }
		        if (messages.isEmpty()) {
		        	File uploads = new File(dataPath);
					String fileName = null;
					String folderpath = null;
					String filePath = "";
					if (filePart != null) {
			            // obtains input stream of the upload file
			            if (filePart.getSize() !=0){	
			            	folderpath =  district.replaceAll("\\s+","")+ "/"+ city.replaceAll("\\s+","");
							File path = new File(dataPath + "/" + folderpath);
							if (!path.exists()) {
							    path.mkdirs();
							}
			            	fileName = no.replaceAll("\\s+","")  + street.replaceAll("\\s+","")  + city.replaceAll("\\s+","")+ ".jpg";
			            	File file = new File(uploads+ "/" + folderpath, fileName); // Or File.createTempFile("somefilename-", ".ext", uploads) if you want to autogenerate an unique name.
			            	try (InputStream input = filePart.getInputStream()) {  // How to obtain part is answered in http://stackoverflow.com/a/2424824
			            		Files.copy(input, file.toPath(), StandardCopyOption.REPLACE_EXISTING);
			            	}catch (Exception e) {
			            		System.out.println("imageNotSaved: " + e);
			            		e.printStackTrace();
							}
			            	filePath = folderpath + "/" +fileName;
			            }
			        }					
					boolean rslt = rdb.insertInst(cat, typ, nme, no, street, city, district, tele1, tele2, fax, shpemail, website, mail,  pswd,
							descript, INACTIVE, token, filePath);
					if (rslt == true) {
						// inform admin 
						String reqst = "regist";
						String path ="http://locator.elasticbeanstalk.com/Activation?email="+ mail + "&token="+token + "&reqst=" + reqst;
						String body = rdb.generateBody(path, cat, typ, nme, no, street, city, district, tele1, tel2, fax, shpemail, website, descript, mail, INACTIVE);
						boolean sent= verf.setMail(EMAIL, SUBJECT1, body);
						if(sent){
							//display and email user saying sent to review
							request.getSession().setAttribute("flagadd", "success");
						}else{
							request.getSession().setAttribute("flagadd", "emailError");
						}				
					}else{
						System.out.println("LocatorRegisterException: ");
						request.getSession().setAttribute("flagadd", "insertError");
					}
					//request.getRequestDispatcher("confrm.jsp").forward(request, response);
					response.sendRedirect("confrm.jsp");
					// response.sendRedirect("reg.html");
		        }else{
		        	//error message is not empty
		        	request.setAttribute("errorMessages", messages);
		        	request.getRequestDispatcher("/YourPlace").forward(request,	response);
		        }
				
			}else if (request.getParameter("update") != null) {
				String token = 	verf.generateToken();
				String cat = request.getParameter("category").trim();
				String typ = request.getParameter("type").trim();
				String nme = request.getParameter("shopName").trim();
				String no = request.getParameter("no").trim();
				String street = request.getParameter("street").trim();
				String city = request.getParameter("city").trim();
				String district = request.getParameter("dists").trim();

				String tele1 = request.getParameter("telephone1").trim();
				String tele2 = request.getParameter("telephone2").trim();
				String fax = request.getParameter("fax").trim();

				String shpemail = null;
				shpemail = request.getParameter("shopEmail").trim();
				String website = null;
				website = request.getParameter("website").trim();
				String mail= request.getParameter("email").trim();
				//String pswd = request.getParameter("password").trim();
				String descript = request.getParameter("description").trim();
				String link = request.getParameter("s_url").trim();
				
				  // obtains the upload file part in this multipart request
		        Part filePart = request.getPart("photo");
				//String fieldname = request.getPart("photo").toString();
				//System.out.println(fieldname.isEmpty());
				//InputStream inputStream = null; // input stream of the upload file
		        
				File uploads = new File(dataPath);
				String fileName = null;
				String folderpath = null;
				String filePath = "";
				if (filePart != null) {
		            // obtains input stream of the upload file
		            if (filePart.getSize() !=0){	
		            	folderpath =  district.replaceAll("\\s+","")+ "/"+ city.replaceAll("\\s+","");
						File path = new File(dataPath +"/" + folderpath);
						if (!path.exists()) {
						    path.mkdirs();
						}
		            	fileName = no.replaceAll("\\s+","")  + street.replaceAll("\\s+","")  + city.replaceAll("\\s+","")+ ".jpg";
		            	File file = new File(uploads+ "/" + folderpath, fileName); // Or File.createTempFile("somefilename-", ".ext", uploads) if you want to autogenerate an unique name.
		            	try (InputStream input = filePart.getInputStream()) {  // How to obtain part is answered in http://stackoverflow.com/a/2424824
		            		Files.copy(input, file.toPath(), StandardCopyOption.REPLACE_EXISTING);
		            	}catch (Exception e) {
		            		System.out.println("imageNotSaved: " + e);
		            		e.printStackTrace();
						}
		            	filePath = folderpath + "/" +fileName;
		            }
		        }	
				//String uploadedFileName = file.getName().toString();
				boolean rslt = udb.insertTempInst(cat, typ, nme, no, street, city, district, tele1, tele2, fax, shpemail, website, 
						mail, descript, AWAIT, token, filePath, link);
				if (rslt == true) {
					// inform admin 
					String reqst = "update";
					String path ="http://locator.elasticbeanstalk.com/Activation?email="+ mail + "&token="+token + "&reqst=" + reqst;
					String body = rdb.generateBody(path, cat, typ, nme, no, street, city, district, tele1, tele2, fax, shpemail, website, descript, mail, AWAIT);
					boolean sent= verf.setMail(EMAIL, SUBJECT2, body);
					if(sent){
						//display and email user saying sent to review
						request.getSession().setAttribute("flagup", "success");
					}else{
						System.out.println("Update Email Error");
						request.getSession().setAttribute("flagup", "emailError");
					}				
				}else{
					System.out.println("Update Error inside inserttempInst");
					request.getSession().setAttribute("flagup", "updateError");
				}
				//request.getRequestDispatcher("dispatch.jsp").forward(request, response); // create new jsp
				response.sendRedirect("dispatch.jsp");
			}						
		}catch(NullPointerException ex){
			System.out.println("LocatorRegisterAndUpdateException: " +ex);
			ex.printStackTrace();
			request.getSession().setAttribute("flagadd", "error");
			//request.getRequestDispatcher("confrm.jsp").forward(request,	response);
			response.sendRedirect("confrm.jsp");
		}catch (Exception ex) {
			System.out.println("LocatorRegisterAndUpdateException: " +ex);
			ex.printStackTrace();
			request.getSession().setAttribute("flagadd", "error");
			//request.getRequestDispatcher("confrm.jsp").forward(request,	response);
			response.sendRedirect("confrm.jsp");
		}
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		//processRequest(request, response);
		request.getRequestDispatcher("/index.html").forward(request, response);

	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);

	}

}
