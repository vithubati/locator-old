<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="img/favicon-loc.ico" />
    <title>Contact Us</title>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/main.css" type="text/css" />
    <link rel="stylesheet" href="css/samp.css" type="text/css" />
    <link rel="stylesheet" href="css/view.css" type="text/css" />
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
<div class="body">
<%  
StringBuffer sb=new StringBuffer();  
for(int i=1;i<=3;i++)  
{  
   // sb.append((char)(int)(Math.random()*79+23+7));  
    sb.append((int)(Math.random()*100));
}  
String cap=new String(sb); 
System.out.println(cap);
%>  
<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="home">Locator</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse" aria-expanded="true">
          <form class="navbar-form navbar-right search" action="find" method="post">
            <div class="form-group">
              <input type="text" placeholder="Look for" class="form-control findTextBox" name="txtfind">
            </div>
            <div class="form-group">
              <select class="form-control" name="district">
              <option value="none">District</option>
							<option value="Colombo">Colombo</option>
							<option value="Kandy">Kandy</option>
							<option value="Galle">Galle</option>
							<option value="Ampara">Ampara</option>
							<option value="Anuradhapura">Anuradhapura</option>
							<option value="Badulla">Badulla</option>
							<option value="Batticaloa">Batticaloa</option>
							<option value="Gampaha">Gampaha</option>
							<option value="Hambantota">Hambantota</option>
							<option value="Jaffna">Jaffna</option>
							<option value="Kalutara">Kalutara</option>
							<option value="Kegalle">Kegalle</option>
							<option value="Kilinochchi">Kilinochchi</option>
							<option value="Kurunegala">Kurunegala</option>
							<option value="Mannar">Mannar</option>
							<option value="Matale">Matale</option>
							<option value="Matara">Matara</option>
							<option value="Moneragala">Moneragala</option>
							<option value="Mullativu">Mullativu</option>
							<option value="Nuwara Eliya">Nuwara Eliya</option>
							<option value="Polonnaruwa">Polonnaruwa</option>
							<option value="Puttalam">Puttalam</option>
							<option value="Ratnapura">Ratnapura</option>
							<option value="Trincomalee">Trincomalee</option>
							<option value="Vavuniya">Vavuniya</option>
              </select>
            </div>
            <button type="submit" class="btn btn-success form-control" name="btfind">Find</button>
            <div class="form-group">
            <a href="YourPlace" class="hlink">Your Shop</a>
            </div>
            <div class="form-group">
            <a href="ContactUs" class="active hlink">Contact Us</a>
            </div>
          </form>
        </div><!--/.navbar-collapse -->
      </div>
    </nav>
<div class="mainContent">
<div class="container">
<div class="row">
<div class="col-md-8 col-md-8 col-lg-8 col-sm-10">
<c:if test="${contMsg eq 'success'}">
<div class="topcontent"><div class="effect8"><p class="bg-success text-center">
Your message has been submitted successfully.
<br>
Thank you for your support.
</p></div>
</div>
<c:remove var="contMsg" scope="session" />
</c:if>
<c:if test="${contMsg eq 'fail'}">
<div class="topcontent"><div class="effect8"><p class="bg-danger text-center">
There was an error occurred while submitting your message. We Apologize for the inconvenience. <br>Please Try again.</p></div>
</div>
<c:remove var="contMsg" scope="session" />
</c:if>
<div class="topcontent">
<form class="form-horizontal" method="post" action="contact" name="contform" id="contform" onsubmit="return validation()">
                    <fieldset>
                        <h3 class="" id="ribbon-container"><span>Contact us</span></h3>
                        <div class="form-group">
                            <div class="col-md-8">
                                <input id="fname" name="fname" type="text" placeholder="First Name (Required)" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8">
                                <input id="lname" name="lname" type="text" placeholder="Last Name" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8">
                                <input id="email" name="email" type="text" placeholder="Email Address (Required)" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8">
                                <input id="phone" name="phone" type="text" placeholder="Phone" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8">
                                <select id="subject" name="subject" placeholder="subject" class="form-control">
                                <option value="none" disabled="disabled" selected="selected">Select Subject</option>
                                <option value="Question">Question</option>
                                <option value="Business Related">Business Related</option>
                                <option value="Advertising">Advertising</option>
                                <option value="Complaint">Complaint</option>
                                <option value="Other">Other</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8">
                                <textarea class="form-control" id="message" name="message" maxlength="500" rows="7"
                                placeholder="Enter your massage for us here. We will get back to you within 2 business days. (Required)" ></textarea>
                            </div>
                        </div>
						 <div class="form-group">
                            <div class="col-md-3">
                            <label for="inputEmail3" class="col-md-12 captcha" unselectable='on' onselectstart='return false;' onmousedown='return false;'>
                            <s><i><%=cap%></i></s></label>
                            </div>
                            <div class="col-md-5">
                                <input id="captcha" name="captcha" type="text" placeholder="Enter the Captcha" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 text-right">
                           		<span id="worning"></span>
                                <button type="submit" class="btn btn-success btn-lg" id="send" name="send" value="send">Submit</button>
                            </div>
                        </div>
                    </fieldset>
                    <input type="hidden" name="cap2" id="cap2" value='<%=cap%>' readonly="readonly">
                </form>
</div>
</div>
</div>	
</div>
</div>
</div>
<footer class="mainFooter">
	<p>	Copyright &copy; locator.com </p>
</footer>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/contact.js"></script>
</body>
</html>