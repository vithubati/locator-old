<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" type="image/png" href="img/favicon-loc.ico" />
<title>Yours</title>
<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<link rel="stylesheet" href="css/samp.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/anim.css">
<link rel="stylesheet" href="css/main.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/register.css" />
</head>

<body>
<div class="body">
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"	aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="home">Locator</a>
			</div>
			<div id="navbar" class="navbar-collapse collapse" aria-expanded="true">
				<form class="navbar-form navbar-right search" action="find" method="post">
					<div class="form-group">
						<input type="text" placeholder="Look for" class="form-control findTextBox" name="txtfind" >
					</div>
					<div class="form-group">
						<select class="form-control" name="district">
							<option value="none">District</option>
							<option value="Colombo">Colombo</option>
							<option value="Kandy">Kandy</option>
							<option value="Galle">Galle</option>
							<option value="Ampara">Ampara</option>
							<option value="Anuradhapura">Anuradhapura</option>
							<option value="Badulla">Badulla</option>
							<option value="Batticaloa">Batticaloa</option>
							<option value="Gampaha">Gampaha</option>
							<option value="Hambantota">Hambantota</option>
							<option value="Jaffna">Jaffna</option>
							<option value="Kalutara">Kalutara</option>
							<option value="Kegalle">Kegalle</option>
							<option value="Kilinochchi">Kilinochchi</option>
							<option value="Kurunegala">Kurunegala</option>
							<option value="Mannar">Mannar</option>
							<option value="Matale">Matale</option>
							<option value="Matara">Matara</option>
							<option value="Moneragala">Moneragala</option>
							<option value="Mullativu">Mullativu</option>
							<option value="Nuwara Eliya">Nuwara Eliya</option>
							<option value="Polonnaruwa">Polonnaruwa</option>
							<option value="Puttalam">Puttalam</option>
							<option value="Ratnapura">Ratnapura</option>
							<option value="Trincomalee">Trincomalee</option>
							<option value="Vavuniya">Vavuniya</option>
						</select>
					</div>
					<button type="submit" class="btn btn-success form-control" name="btfind">Find</button>
					<div class="form-group">
						<a href="YourPlace"  class="active hlink">Your Shop</a>
					</div>
					<div class="form-group">
						<a href="ContactUs" class="hlink">Contact Us</a>
					</div>
				</form>
			</div>
			<!--/.navbar-collapse -->
		</div>
	</nav>
	<div class="mainContent">
		<div id="msform">
			<!-- progressbar -->
			<ul id="progressbar">
				<li class="active">Fill in details</li>
				<li>Verify your Ad</li>
				<li>Confirmation</li>
			</ul>
			<form name="regForm2" action="yourshop" method="post" id="regForm2"
				class="regForm2 form-horizontal" enctype="multipart/form-data">
				<!-- fieldsets -->
				<fieldset>
					<h2 class="fs-title">Fill in details</h2>
					<h3 class="fs-subtitle">Your institution (Shop) Information</h3>
					<div class="center-col">
						<article class="topcontent effect2"> <!-- topcontent effect6 -->
							<!-- Left  -->
							<div class="col-md-6" style="padding-top: 2%">
								<div class="form-group required">
									<label for="inputEmail3" class="control-label col-md-3">Name</label>
									<div class="col-sm-6">
										<input type="text" class="form-control " id="shopName" name="shopName" placeholder="">
										<span id="" class="text-danger">${errorMessages.emptyF}</span>
									</div>
								</div>
								<div class="form-group required">
									<label for="inputEmail3" class="control-label col-md-3">Category</label>
									<div class="col-sm-6">
										<select class="form-control " id="category"	name="category">
										<option value="none" disabled="disabled" selected="selected">Select Category</option>
											<optgroup label="Arts and Crafts">
												<option value="Antiques Shops">Antiques Shop</option>
											</optgroup>
											<optgroup label="Books & Stationaries">
												<option value="Library">Library</option>
												<option value="Novelty Store">Novelty Store</option>
												<option value="Stationary Shop">Stationary Shop</option>
											</optgroup>
											<optgroup label="Clothing & Fashion Shops">
												<option value="Clothing and Designer Wear Shop">Clothing and Designer Wear Shop</option>
												<option value="Jewelry shop">Jewelry shop</option>
												<option value="Launderette">Launderette</option>
												<option value="Shoe Shop">Shoe Shop</option>
												<option value="Watch Shop">Watch Shop</option>	
											</optgroup>
											<optgroup label="Computing & Electronics">
												<option value="Camera and Camcorder Shop">Camera and Camcorder Shop</option>
												<option value="Computer Accessories and Repair Shop">Computer Accessories and Repair Shop</option>
												<option value="Cellphones and Accessories Shop">Cellphones and Accessories Shop</option>
												<option value="Home Electronics Shop">Home Electronics Shop</option>
												<option value="Laptop, Desktop and Tablet Shop">Laptop, Desktop and Tablet Shop</option>
												<option value="TV, Audio and Surveillance Shop">TV, Audio and Surveillance Shop</option>
												<option value="Video games and Consoles Shop">Video games and Consoles Shop</option>
											</optgroup>
											<optgroup label="Groceries & Retails Shops">
												<option value="Bakery">Bakery</option>
												<option value="Butcher">Butcher</option>
												<option value="Corner Shop / Convenience Shop">Corner Shop / Convenience Shop</option>
												<option value="Department Store">Department Store</option>
												<option value="Fishmonger">Fishmonger</option>
												<option value="Florist">Florist</option>
												<option value="Gift Shop">Gift Shop</option>
												<option value="Liquor Shop">Liquor Shop</option>
												<option value="Super Market">Super Market</option>
											</optgroup>
											<optgroup label="Education">
												<option value="Campus">Campus</option>
												<option value="College">College</option>
												<option value="Institute">Institute</option>
												<option value="Montessori">Montessori</option>
												<option value="Tuition">Tuition</option>
											</optgroup>
											<optgroup label="Health & Beauty">
												<option value="Beauty Saloon / Barber Shop">Beauty Saloon / Barber Shop</option>
												<option value="Gym and Fitness center">Gym and Fitness center</option>
												<option value="Hairdresser">Hairdresser</option>
												<option value="Hospital">Hospital</option>
												<option value="Medical">Medical</option>
												<option value="Pharmacy / Chemist">Pharmacy / Chemist</option>
												<option value="Sports Shop">Sports Shop</option>
											</optgroup>
											<optgroup label="Personal Finance">
												<option value="Bank">Bank</option>
												<option value="Insurance Company">Insurance Company</option>
												<option value="Money Transfer">Money Transfer</option>
												<option value="Pawning Center">Pawning Center</option>
											</optgroup>
											<optgroup label="Others">
												<option value="Communication">Communication</option>
												<option value="Furniture Shop">Furniture Shop</option>
												<option value="Garage">Garage</option>
												<option value="Gas Station">Gas Station</option>
												<option value="Hardware Shop">Hardware Shop</option>
												<option value="Hotels">Hotels</option>
												<option value="Pet Shop">Pet Shop</option>
												<option value="Restaurant">Restaurant</option>
												<option value="Service Center">Service Center</option>
												<option value="Theatres">Theatres</option>
												<option value="Toy Store">Toy Store</option>
												<option value="Travel Agent">Travel Agent</option>
												<option value="Vehicle Shop">Vehicle Shop</option>
												<option value="Video Rental Shop">Video Rental Shop</option>
											</optgroup>
										</select>
										<span id="" class="text-danger">${errorMessages.emptyF}</span>
									</div>
								</div>
								<div class="form-group required">
									<label for="inputEmail3" class="control-label col-md-3">Type</label>
									<div class="col-sm-6">
										<select class="form-control " id="type" name="type">
										<option value="none" disabled="disabled" selected="selected">Select Type </option>
											<option>Physical</option>
											<option>On-line</option>
										</select>
										<span id="" class="text-danger">${errorMessages.emptyF}</span>
									</div>
								</div>
								<label for="inputEmail3" class="control-label">Address</label>
								<div class="effect"> <!--effect8  -->
									<div class="form-group required">
										<label for="inputEmail3" class="control-label col-md-3">No</label>
										<div class="col-sm-6">
											<input type="text" class="form-control " name="no" id="no" placeholder="">
											<span id="" class="text-danger">${errorMessages.emptyF}</span>
										</div>
									</div>
									<div class="form-group required">
										<label for="inputEmail3" class="control-label col-md-3">Street</label>
										<div class="col-sm-6">
											<input type="text" class="form-control " id="street" name="street" placeholder="">
											<span id="" class="text-danger">${errorMessages.emptyF}</span>
										</div>
									</div>
									<div class="form-group required">
										<label for="inputEmail3" class="control-label col-md-3">District</label>
										<div class="col-sm-6">
											<select id="dists" name="dists" class="form-control ">
											<option value="none" disabled="disabled" selected="selected">Select District</option>
												<option value="Colombo">Colombo</option>
												<option value="Kandy">Kandy</option>
												<option value="Galle">Galle</option>
												<option value="Ampara">Ampara</option>
												<option value="Anuradhapura">Anuradhapura</option>
												<option value="Badulla">Badulla</option>
												<option value="Batticaloa">Batticaloa</option>
												<option value="Gampaha">Gampaha</option>
												<option value="Hambantota">Hambantota</option>
												<option value="Jaffna">Jaffna</option>
												<option value="Kalutara">Kalutara</option>
												<option value="Kegalle">Kegalle</option>
												<option value="Kilinochchi">Kilinochchi</option>
												<option value="Kurunegala">Kurunegala</option>
												<option value="Mannar">Mannar</option>
												<option value="Matale">Matale</option>
												<option value="Matara">Matara</option>
												<option value="Moneragala">Moneragala</option>
												<option value="Mullativu">Mullativu</option>
												<option value="Nuwara Eliya">Nuwara Eliya</option>
												<option value="Polonnaruwa">Polonnaruwa</option>
												<option value="Puttalam">Puttalam</option>
												<option value="Ratnapura">Ratnapura</option>
												<option value="Trincomalee">Trincomalee</option>
												<option value="Vavuniya">Vavuniya</option>
											</select>
											<span id="" class="text-danger">${errorMessages.emptyF}</span>
										</div>
									</div>
									<div class="form-group required">
										<label for="inputEmail3" class="control-label col-md-3">City / Area</label>
										<div class="col-sm-6">
											<select class="form-control " name="city" id="city">
											</select>
											<span id="" class="text-danger">${errorMessages.emptyF}</span>
										</div>
									</div>
								</div>
								<div class="form-group required">

									<label for="inputEmail3" class="control-label col-md-3">Description</label>

									<div class="col-sm-6">
										<textarea class="form-control" rows="5" id="description" maxlength="1000" name="description" 
										placeholder="About Your institution! Good discription allows people to get to know about your place"></textarea>
										<span id="character-count" class="character-count">1000</span > <span class="character-count"> &nbsp;characters remaining.</span>
										<span id="" class="text-danger">${errorMessages.emptyF}</span>
									</div>
								</div>
							</div>
							<!--Left-End  -->

							<div class="form-group col-md-6">
								<label for="inputEmail3" class="control-label">Contects</label>
								<div class="effect2"> <!--effect8  -->
									<div class="form-group required">
										<label for="inputEmail3" class="control-label col-md-3">Tele 1</label>
										<div class="col-sm-6">
											<input type="text" class="form-control " id="telephone1" name="telephone1" placeholder="">
											<span id="" class="text-danger">${errorMessages.tele1F}</span>
										</div>
									</div>
									<div class="form-group">
										<label for="inputEmail3" class="control-label col-md-3">Tele 2</label>
										<div class="col-sm-6">
											<input type="text" class="form-control " id="telephone2" name="telephone2" placeholder="Eg: 0114567891">
											<span id="" class="text-danger">${errorMessages.tele2F}</span>
										</div>
									</div>
									<div class="form-group">
										<label for="inputEmail3" class="control-label col-md-3">Fax</label>
										<div class="col-sm-6">
											<input type="text" class="form-control " id="fax" name="fax" placeholder="Eg: 0114567891">
											<span id="" class="text-danger">${errorMessages.photoF}</span>
										</div>
									</div>
									<div class="form-group">
										<label for="inputEmail3" class="control-label col-md-3">Shop Email</label>
										<div class="col-sm-6">
											<input type="text" class="form-control " id="shopEmail" name="shopEmail" placeholder="">
											<span id="" class="text-danger">${errorMessages.sEmail}</span>
										</div>
									</div>
									<div class="form-group">
										<label for="inputEmail3" class="control-label col-md-3">Website</label>
										<div class="col-sm-6">
											<input type="text" class="form-control " id="website" name="website" placeholder="">
										</div>
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="control-label col-md-3">Photo</label>
									<div class="col-sm-6">
										<input type="file" id="photo" class="" name="photo">
										<span id="" class="text-danger">${errorMessages.photoF}</span>
									</div>
								</div>
								<div class="form-group required">
									<label for="inputEmail3" class="control-label col-md-3">Your Email</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="email" name="email" placeholder="This Email will be hidden.">
										<span id="" class="text-danger">${errorMessages.rEmail}</span>
									</div>
								</div>
								<div class="form-group required">
									<label for="inputEmail3" class="control-label col-md-3">Password</label>
									<div class="col-sm-6">
										<input type="password" class="form-control " id="password" name="password" placeholder="">
										<span id="" class="text-danger">${errorMessages.passChar}</span>
									</div>
								</div>
								<div class="form-group required">
									<label for="inputEmail3" class="control-label col-md-3">Confirm	Password</label>
									<div class="col-sm-6">
										<input type="password" class="form-control" id="cpassword" name="cpassword" placeholder="">
										<span id="" class="text-danger">${errorMessages.passChar}</span>
									</div>
								</div>
								<div class="form-group text-center" >
								<span id="errormsg" class="text-danger"></span>
								</div>
							</div>
							<!-- right end -->
							<div class="form-group">
								<div class="col-sm-6 col-md-6 col-md-offset-6">	
								</div>
							</div>
							<input type="hidden" name="s_url" value="${link}">
						</article>
					</div>
					<div class="form-group">
								<div class="col-sm-6 col-xs-8 pull-right">	
								<input type="button" id="next" name="next" class="next btn btn-success pull-center" value="Next" />
								</div>
							</div>
					
				</fieldset>
				<fieldset>
					<h2 class="fs-title">Verify your Ad</h2>
					<h3 class="fs-subtitle">Please verify your Ad before
						publishing</h3>
					<div id="fetch_step">
					<div class="center-col">
						<article class="topcontent effect2"> <!-- topcontent effect6 -->
						<div class="mainSummary">
							<table class="table table-striped">
								<tr class="warning">
									<td>Category</td>
									<td></td>
								</tr>
								<tr class="warning">
									<td>Type</td>
									<td></td>
								</tr>
								<tr class="warning">
									<td>Shop Name</td>
									<td></td>
								</tr>
								<tr class="warning">
									<td>Address</td>
									<td></td>
								</tr>
								<tr class="warning">
									<td>Contacts</td>
									<td></td>
								</tr>
								<tr class="warning">
									<td>website</td>
									<td></td>
								</tr>
								<tr class="warning">
									<td>Description</td>
									<td class="des">
										<div class="ds"></div>
									</td>
								</tr>

							</table>
						</div>
						</article>
						</div>
						<!-- clearfix -->
						<div class="clear"></div>
						<!-- /clearfix -->
					</div>
					<div class="form-group">
					<div class="col-sm-7 col-xs-9 pull-right" >	
						<input type="button" id="previous" name="previous" class="previous btn btn-success" value="Previous" />
						<input type="submit" id="submit" name="submit" class="submit btn btn-success" value="Submit" />
					</div>
					</div>
				</fieldset>
				<fieldset>
					<h2 class="fs-title">Confirmation</h2>

					<h3 class="fs-subtitle"></h3>
					<div id="floatingBarsG" style="margin-left: 45%;">
						<div class="blockG" id="rotateG_01"></div>
						<div class="blockG" id="rotateG_02"></div>
						<div class="blockG" id="rotateG_03"></div>
						<div class="blockG" id="rotateG_04"></div>
						<div class="blockG" id="rotateG_05"></div>
						<div class="blockG" id="rotateG_06"></div>
						<div class="blockG" id="rotateG_07"></div>
						<div class="blockG" id="rotateG_08"></div>
					</div>
				</fieldset>
			</form>
		</div>

	</div>

	</div>
	<footer class="mainFooter">
	<p>	Copyright &copy; locator.com </p>
	</footer>

	<!-- jQuery -->
	<script src="js/jquery-1.9.1.min.js" type="text/javascript"></script>
	<!-- jQuery easing plugin -->
	<script src="js/jquery.easing.min.js" type="text/javascript"></script>
	<script src="js/bootstrap.js"></script>
	<script type="text/javascript" src="js/function.js"></script>
	<script src="js/register.js" type="text/javascript"></script>
	<script src="js/location.js" type="text/javascript"></script>
	

</body>
</html>