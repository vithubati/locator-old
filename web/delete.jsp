<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.locator.model.Capture"%>
<%@page import="java.sql.ResultSet"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Delete</title>
    <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/img/favicon-loc.ico" />
    <!-- Bootstrap core CSS -->
    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/main.css" type="text/css" />
     <link rel="stylesheet" href="${pageContext.request.contextPath}/css/view.css" type="text/css" />
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <c:set var="root" value="${pageContext.request.contextPath}" />	
  </head>
  <body>
<div class="body">
  <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="home">Locator</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse" aria-expanded="true">
          <form class="navbar-form navbar-right search" action="${pageContext.request.contextPath}/find" method="post">
            <div class="form-group">
              <input type="text" placeholder="Look for" class="form-control findTextBox" name="txtfind" >
            </div>
            <div class="form-group">
              <select class="form-control" name="district">
              <option value="none">District</option>
							<option value="Colombo">Colombo</option>
							<option value="Kandy">Kandy</option>
							<option value="Galle">Galle</option>
							<option value="Ampara">Ampara</option>
							<option value="Anuradhapura">Anuradhapura</option>
							<option value="Badulla">Badulla</option>
							<option value="Batticaloa">Batticaloa</option>
							<option value="Gampaha">Gampaha</option>
							<option value="Hambantota">Hambantota</option>
							<option value="Jaffna">Jaffna</option>
							<option value="Kalutara">Kalutara</option>
							<option value="Kegalle">Kegalle</option>
							<option value="Kilinochchi">Kilinochchi</option>
							<option value="Kurunegala">Kurunegala</option>
							<option value="Mannar">Mannar</option>
							<option value="Matale">Matale</option>
							<option value="Matara">Matara</option>
							<option value="Moneragala">Moneragala</option>
							<option value="Mullativu">Mullativu</option>
							<option value="Nuwara Eliya">Nuwara Eliya</option>
							<option value="Polonnaruwa">Polonnaruwa</option>
							<option value="Puttalam">Puttalam</option>
							<option value="Ratnapura">Ratnapura</option>
							<option value="Trincomalee">Trincomalee</option>
							<option value="Vavuniya">Vavuniya</option>
              </select>
            </div>
            <button type="submit" class="btn btn-success form-control" name="btfind">Find</button>
            <div class="form-group">
            <a href="${pageContext.request.contextPath}/YourPlace" class="hlink">Your Shop</a>
            </div>
            <div class="form-group">
            <a href="${pageContext.request.contextPath}/ContactUs" class="hlink">Contact Us</a>
            </div>
          </form>
        </div><!--/.navbar-collapse -->
      </div>
    </nav>
	<form action="${root}/delete" method="post">
<div class="mainContent">	
		<c:if test="${valid eq 'true'}">
			<article class="topcontent">
					<div id="ribbon-container"><span>Delete '${nme}'</span></div>
					<h3><span class="text-danger">Are you sure, You want to delete '${nme}'?</span></h3>
					<span> <input type="submit" id="confirm" name="confirm" class="btn btn-danger deleteButton" value="Yes, Delete" /></span>
					<input type="hidden" name="url" value="${link}">
					<input type="hidden" name="name" value="${nme}">
			</article>
		</c:if>
		
		<c:if test="${delete eq 'success'}">
			<article class="topcontent">
				<h3 class="" id="ribbon-container">
						<span style="text-align: center;">Your Registerd Place is removed successfully!</span></h3>
						<span class="text-info">
				<!-- 		It may take a few hours for your institution ad to be published across the site as it undergoes a routine quality control review.
						Once the institution ad details have been reviewed, you will receive an
						email. You can also keep track of your ad through your account. -->
						</span>
			</article>
		</c:if>
		<c:if test="${delete eq 'already'}">
			<article class="topcontent">
				<h3 class="" id="ribbon-container">
						<span style="text-align: center; color: #8a6d3b;">Your Registerd Place has Already been removed.</span></h3>
			</article>
		</c:if>
		<c:if test="${delete eq 'fail'}">
			<article class="topcontent">
				Some error occured while deleting your registered place! Please try again!
			</article>
		</c:if>	
		<c:if test="${valid eq 'false'}">
		
				<article class="topcontent">
					<div id="ribbon-container"><span>Delete '${nme}'</span></div>
					<h3><span class="text-danger">The Password is incorrect!</span> </h3>
					<h4><span>Enter Password to delete <span class="shopname">'${nme}'</span>.</span></h4>
					<span style="color: #FF6666">Account password</span>	<br>
					<span><input type="password" name="passtxt" class="input-sm"></span>
					<span> <input type="submit" id="delete2" name="deletebtn" class="btn btn-danger deleteButton" value="Delete" /></span>
					<input type="hidden" name="url" value="${link}">
					<input type="hidden" name="name" value="${nme}">
				</article>
		</c:if>
					
	</div>
	</form>
	</div>
	<footer class="mainFooter">
	<p>	Copyright &copy; locator.com </p>
	</footer>

  <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.js"></script>
</body>	
	
</html>