
$(document).ready(function() {
	$("#update").click(function (e) {
	   var validate = validation();
	   if(!validate){
		   e.preventDefault();
	   }
	  return validate;
	});
});

function validation(){
	$('#regForm2 input').removeClass('error').removeClass('valid');
	$('#errormsg').text("");
	 var fields = $('#regForm2 input[type=text], input[type=password]');
	 var email = $('#regForm2 input[id=email]');
	 var emailVal = $('#regForm2 input[id=email]').val().trim();
	 var teleNo = $('#regForm2 input[id=telephone1]');
	 var telNoVal = $('#regForm2 input[id=telephone1]').val().trim();
	 var teleNo2 = $('#regForm2 input[id=telephone2]');
	 var telNoVal2 = $('#regForm2 input[id=telephone2]').val().trim();
	 var fax = $('#regForm2 input[id=fax]');
	 var faxVal = $('#regForm2 input[id=fax]').val().trim();;
	 var pswd = $('#regForm2 input[id=password]');
	 var pswdVal = $('#regForm2 input[id=password]').val().trim() ;
	 var cpswd = $('#regForm2 input[id=cpassword]');
	 var cpswdVal = $('#regForm2 input[id=cpassword]').val().trim();
	 var descrpt = $('#description');
	 var descrptVal = $('#description').val();
	 var semail = $('#regForm2 input[id=shopEmail]');
	 var semailVal = $('#regForm2 input[id=shopEmail]').val().trim();
	 var web = $('#regForm2 input[id=website]');
	 var webVal = $('#regForm2 input[id=website]').val().trim();
	 var isValid = true;
	 var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
	 var Valid = true;
	 fields.each(function() {
	    if ($.trim($(this).val()) == '') {
	    	Valid = false;
			$(this).removeClass('valid');
			$(this).addClass('error');
			$('#errormsg').text("Please Fill Required(*) Fields!");
			if(this.id === 'fax' || this.id === 'telephone2' || this.id === 'shopEmail' || this.id === 'website'){
				$(this).removeClass('error');	
				Valid =true;
			}
		}
		else{
			$(this).removeClass('error');
			$(this).addClass('valid');
		}
	});
	if(Valid){
		if ( pswdVal.length > 5) {
			if(pswdVal != cpswdVal){
				isValid = false;
				$(pswd).removeClass('valid');
				$(pswd).addClass('error');
				$(cpswd).removeClass('valid');
				$(cpswd).addClass('error');
				$('#errormsg').text("Passwords don't match!");
			}else{
				$(pswd).removeClass('error');
				$(cpswd).addClass('valid');
				$(cpswd).removeClass('error');
				$(cpswd).addClass('valid');
			}
		}else{
			$('#errormsg').text("Password must contains atleast 6 characters!");
			isValid = false;
			$(pswd).removeClass('valid');
			$(pswd).addClass('error');
			$(cpswd).removeClass('valid');
			$(cpswd).addClass('error');
		}
		if (emailVal.match(filter) == null) {
			$('#errormsg').text("Invalid Email!");
			$(email).removeClass('valid');
			$(email).addClass('error');
			isValid = false;
		}else{
			$(email).removeClass('error');
			$(email).addClass('valid');
		}
		if(semailVal.length > 0){
			if ( semailVal.match(filter) == null) {
				isValid = false;
				$(semail).removeClass('valid');
				$(semail).addClass('error');
				$('#errormsg').text("Invalid Email!");
			}else{
				$(semail).removeClass('error');
				$(semail).addClass('valid');
			}					
		}
		if(telNoVal2.length > 0){
			if ( telNoVal2.length > 10 || telNoVal2.length < 9 || telNoVal2.match(/^[0-9]+$/) == null) {
				isValid = false;
				$(teleNo2).removeClass('valid');
				$(teleNo2).addClass('error');
				$('#errormsg').text("Telephone Number not valid!");
			}else{
				$(teleNo2).removeClass('error');
				$(teleNo2).addClass('valid');
			}					
		}
		if(faxVal.length > 0){
			if ( faxVal.length > 10 || faxVal.length < 9 || faxVal.match(/^[0-9]+$/) == null) {
				isValid = false;
				$(fax).removeClass('valid');
				$(fax).addClass('error');
				$('#errormsg').text("Fax number not valid!");
			}else{
				$(fax).removeClass('error');
				$(fax).addClass('valid');
			}					
		}
		if ( telNoVal.length > 10 || telNoVal.length < 9 || telNoVal.match(/^[0-9]+$/) == null) {
			isValid = false;
			$(teleNo).removeClass('valid');
			$(teleNo).addClass('error');
			$('#errormsg').text("Telephone Number not valid!");
		}else{
			$(teleNo).removeClass('error');
			$(teleNo).addClass('valid');
		}
		
		if ( descrptVal.length == '' ) {
			isValid = false;
			$('#errormsg').text("Please Fill Required(*) Fields!");
			$(descrpt).removeClass('valid');
			$(descrpt).addClass('error');
		}else{
			$(descrpt).removeClass('error');
			$(descrpt).addClass('valid');
		}
		
	}else{
		isValid=false;
		Valid = false;
	}
	if (isValid == false){
		return false;
	}
	if(Valid == false){
		return false;
	}
	else{ 
		$('#errormsg').text("");
		return true;
	}
}