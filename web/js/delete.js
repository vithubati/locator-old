$(".editButton").click(function () {
	    $(this).hide();
	    $(".deleteButton").hide();	   
	    $("#report").hide();	
	    $('.hideEdt').slideDown( "slow" );
	});
$(".deleteButton").click(function () {
    $(this).hide();
    $(".editButton").hide();
    $("#report").hide();
    $('.hideDel').slideDown( "slow" );
});
$(".forgPassButton").click(function () {
    $('.hideEdt').hide();
    $('.hideDel').hide();
    $('.hidepass').slideDown( "slow" );
});
$("#report").on("click", function() {
	  $("body").toggleClass("dialogIsOpen");
});
$("#cancel").on("click", function() {
	  $("body").toggleClass("dialogIsOpen");
	  /*$('html').css("background-color", "red");*/
});

function validation(){  
	$('#repform input, select, textarea').removeClass('error').removeClass('valid');
   
    var email = $('#repform input[id=email]');
    var subject = $("#reason");
    var message = $("#message");
    var emailVal = $('#repform input[id=email]').val().trim();
    var subjectVal = $("#reason").val();
    var messageVal = $("#message").val().trim();
    var isValid = true;
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (emailVal == '') {
    	$(email).removeClass("valid");
    	$(email).addClass("error");
    	isValid =false;
	}
    if (subjectVal == null) {
    	$(subject).removeClass("valid");
    	$(subject).addClass("error");
    	isValid =false;
	}if (messageVal.length == '') {
    	$(message).removeClass("valid");
    	$(message).addClass("error");
    	isValid =false;
	}
	if (emailVal.match(filter) == null) {
		$('#errormsg').text("Invalid Email!");
		$(email).removeClass('valid');
		$(email).addClass('error');
		isValid = false;
	}
	
    if (isValid == false) 
		return false;
	else 
		return true;
}