<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/img/favicon-loc.ico" />
   <!--  <meta property="fb:admins" content="vithunan"/> -->
    <meta property="fb:app_id" content="773556639428606" />
    <meta property="og:site_name" content="www.locator.elasticbeanstalk.com">
    <meta property="og:type" content="place"/>
     <meta property="og:url" content="http://www.locator.elasticbeanstalk.com/view/${URL}"/>
     <c:forEach var="institute" items="${instiList}">
     <meta property="og:title" content="${institute.nme} | ${institute.city}" />
     <meta property="og:description" content="${institute.descript}"/>
     <meta property="place:location:latitude"  content="${institute.latitude}" /> 
  	  <meta property="place:location:longitude" content="${institute.longitude}" />
  	  <c:if test="${empty institute.photoPath}">
  	  <meta property="og:image"content="http://www.locator.elasticbeanstalk.com/img/bg2.png" />
  	  </c:if>
  	  <c:if test="${not empty institute.photoPath}">
  	  <meta property="og:image"content="http://locator.elasticbeanstalk.com/Locator/images/${institute.photoPath}" />
  	  </c:if>
     </c:forEach>
    <title>Locator</title>
    <!-- Bootstrap core CSS -->
    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/main.css" type="text/css" />
     <link rel="stylesheet" href="${pageContext.request.contextPath}/css/view.css" type="text/css" />
     <link rel="stylesheet" href="${pageContext.request.contextPath}/css/common.css" type="text/css" />
     
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/institution-frontpage.css" type="text/css"/>
	<c:set var="root" value="${pageContext.request.contextPath}" />	
</head>
<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.3&appId=773556639428606";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div class="body">
  <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="${pageContext.request.contextPath}/home">Locator</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse" aria-expanded="true">
          <form class="navbar-form navbar-right search" action="${pageContext.request.contextPath}/find" method="post">
            <div class="form-group">
              <input type="text" placeholder="Look for" class="form-control findTextBox" name="txtfind" >
            </div>
            <div class="form-group">
              <select class="form-control" name="district">
              <option value="none">District</option>
							<option value="Colombo">Colombo</option>
							<option value="Kandy">Kandy</option>
							<option value="Galle">Galle</option>
							<option value="Ampara">Ampara</option>
							<option value="Anuradhapura">Anuradhapura</option>
							<option value="Badulla">Badulla</option>
							<option value="Batticaloa">Batticaloa</option>
							<option value="Gampaha">Gampaha</option>
							<option value="Hambantota">Hambantota</option>
							<option value="Jaffna">Jaffna</option>
							<option value="Kalutara">Kalutara</option>
							<option value="Kegalle">Kegalle</option>
							<option value="Kilinochchi">Kilinochchi</option>
							<option value="Kurunegala">Kurunegala</option>
							<option value="Mannar">Mannar</option>
							<option value="Matale">Matale</option>
							<option value="Matara">Matara</option>
							<option value="Moneragala">Moneragala</option>
							<option value="Mullativu">Mullativu</option>
							<option value="Nuwara Eliya">Nuwara Eliya</option>
							<option value="Polonnaruwa">Polonnaruwa</option>
							<option value="Puttalam">Puttalam</option>
							<option value="Ratnapura">Ratnapura</option>
							<option value="Trincomalee">Trincomalee</option>
							<option value="Vavuniya">Vavuniya</option>
              </select>
            </div>
            <button type="submit" class="btn btn-success form-control" name="btfind">Find</button>
            <div class="form-group">
            <a href="${pageContext.request.contextPath}/YourPlace" class="hlink">Your Shop</a>
            </div>
            <div class="form-group">
            <a href="${pageContext.request.contextPath}/ContactUs" class="hlink">Contact Us</a>
            </div>
          </form>
        </div><!--/.navbar-collapse -->
      </div>
    </nav>
    <c:if test="${RepPlc eq 'success'}">
			<div class="alert alert-success">
    			<a href="#" class="close" data-dismiss="alert">&times;</a>
    			<strong>Success!</strong> Thank you for taking the time to report this place. We have been natified and will investigate.
    			<br>Many placess are removed based on reports from Locator users like you.
			</div>
			<c:remove var="RepPlc" scope="session" />
	</c:if>
	<c:if test="${RepPlc eq 'fail'}">
			<div class="alert alert alert-danger alert-error">
    			<a href="#" class="close" data-dismiss="alert">&times;</a>
    			<strong>Failed!</strong> an Internal error occured. Please try again later. apologize for the inconvenience.
			</div>
			<c:remove var="RepPlc" scope="session" />
	</c:if>
	<c:if test="${RepPlc eq 'error'}">
			<div class="alert alert alert-danger alert-error">
    			<a href="#" class="close" data-dismiss="alert">&times;</a>
    			<strong>Error!</strong> An Error occured, Please try again. apologize for the inconvenience.
			</div>
			<c:remove var="RepPlc" scope="session" />
	</c:if>
	<div class="main" id="main">	
	<!-- Image Background Page Header -->
<!-- Carousel
================================================== -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <c:if test="${data eq 'yes'}">
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>
</c:if>
  <c:forEach var="institute" items="${instiList}">
  <div class="carousel-inner">
   <c:if test="${empty institute.photoPath}">
  	 <div class="item active">
      <img src="" style="width:100%"  class="img-responsive">
      <div class="container">
        <div class="carousel-caption">
          <h1>No images Found</h1>
          <p>The Place does not have any photos</p>
        </div>
      </div>
    </div>
    <div class="item">
      <img src="" style="width:100%"  class="img-responsive">
      <div class="container">
        <div class="carousel-caption">
          <h1>No images Found</h1>
          <p>The Place does not have any photos</p>
        </div>
      </div>
    </div>
    <div class="item">
      <img src="" style="width:100%"  class="img-responsive">
      <div class="container">
        <div class="carousel-caption">
          <h1>No images Found</h1>
          <p>The Place does not have any photos</p>
        </div>
      </div>
    </div>
  </c:if>
  <c:if test="${not empty institute.photoPath}">
    <div class="item active">
      <img src="/locator/images/${institute.photoPath}" style="width:100%" class="img-responsive">
    </div>
    <div class="item">
      <img src="/locator/images/${institute.photoPath}" style="width:100%" class="img-responsive">
    </div>
    <div class="item">
      <img src="/locator/images/${institute.photoPath}" style="width:100%" class="img-responsive">
    </div>
    </c:if>
  </div>
  </c:forEach>
  <c:if test="${data eq 'yes'}">
  <!-- Controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="icon-prev"></span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="icon-next"></span>
  </a> 
  </c:if> 
</div>
<!-- /.carousel -->
<div class="row">
<div class="col-lg-12">
	<c:if test="${instiError eq 'noData'}">
			<article class="topcontent-rds-less">
			<div class="bg-warning"><div class="well-lg text-center"> 
				<h3 class="text-default">The Place You are looking for is not available. </h3>
			</div></div>
			</article>
			<div class="well-lg text-center" style="background-color: rgba(0, 0, 0, 0.1);">
			<h2 style="color: white;">REGISTER + PUBLISH = BE FOUND</h2>
			<h3 style="color: white;">Publish Your Place for Free</h3>
			<a href="${pageContext.request.contextPath}/YourPlace" class="btn btn-success btn-lg">Publish Your Place</a>
			
			</div>
			</c:if>
			<c:if test="${instiError eq 'error'}">
			<article class="topcontent-rds-less">
			<div class="bg-danger"><div class="well-lg text-center"> 
				<h3 class="text-danger">Something went wrong. We apologize for the inconvenience!</h3>
				<h4 class="text-default">Go to Locator's <a href="home"><u>Home</u></a> Page</h4>
				</div></div>
			</article>
			</c:if>
			<c:if test="${delError eq 'badRequest'}">
			<article class="topcontent-rds-less">
				<div class="bg-danger"><div class="well-lg text-center">
				<h3 class="text-danger">Invalid url. The request had bad syntax or was impossible to be satisified.</h3> 
				<h4 class="text-default">Go to Locator's <a href="home"><u>Home</u></a> Page</h4>
				</div></div>
			</article>
			</c:if>
			<c:if test="${delError eq 'error'}">
			<article class="topcontent-rds-less">
				<div class="bg-danger"><div class="well-lg text-center"> 
				<h3 class="text-danger">Something went wrong. We apologize for the inconvenience!</h3>
				<h4 class="text-default">Go to Locator's <a href="home"><u>Home</u></a> Page</h4>
				</div></div>
			</article>
			</c:if>
</div>
</div>
<div class="row">
            <div class="col-lg-8">
			<c:forEach var="institute" items="${instiList}">
	         <article class="topcontent-rds-less" style="min-height: 350px;">
				<div class="row form-group"><div class="col-xs-12 col-md-12 col-lg-12" id="ribbon-container">
					<span>${institute.nme} </span> </div></div>				
				<div class="row form-group"><div class="col-xs-12 col-md-12 col-lg-12 ad-detils">
					${institute.no} &nbsp;${institute.street} &nbsp;${institute.city} ${institute.district} </div></div>
				
				<div class="row form-group ad-detils"><div class="col-xs-3 col-md-6 col-lg-2"> <span class="addresstitle"> Mobile </span></div>
				<div class="col-xs-9 col-md-6 col-lg-4"> ${institute.tele1} </div></div>
	          <c:if test="${institute.tele2 ne '0'}">
	          		<div class="row form-group ad-detils"><div class="col-xs-3 col-md-6 col-lg-2"> <span class="addresstitle"> Tele </span></div>
	            	<div class="col-xs-9 col-md-6 col-lg-4"> ${institute.tele2} </div></div>
	           </c:if>
	            <c:if test="${institute.fax ne '0'}">
	            	<div class="row form-group ad-detils"><div class="col-xs-3 col-md-6 col-lg-2"> <span class="addresstitle"> Fax </span></div>
	            	<div class="col-xs-9 col-md-6 col-lg-4"> ${institute.fax} </div></div>
	            </c:if>
				<c:if test="${institute.shpemail ne ''}">
					<div class="row form-group ad-detils"><div class="col-xs-3 col-md-6 col-lg-2"> <span class="addresstitle"> Email </span></div>
					<div class="col-xs-9 col-md-6 col-lg-4"> ${institute.shpemail} </div></div>
	            </c:if>
	            <c:if test="${institute.website ne ''}">
	            	<div class="row form-group ad-detils"><div class="col-xs-3 col-md-6 col-lg-2"><span class="addresstitle"> Website </span></div>
	            	<div class="col-xs-9 col-md-6 col-lg-4"> ${institute.website} </div></div>
				</c:if>
				<div class="row form-group ad-detils"><div class="col-xs-3 col-md-6 col-lg-2"><span class="addresstitle"> About </span></div>
				<div class="col-xs-9 col-md-6 col-lg-10" style="text-align:justify;">${institute.descript} </div></div>
				<input type="hidden" id="fulladd" value="${institute.no}, ${institute.street}, ${institute.city}, ${institute.district}">
				<%-- <img src='data:image/jpg;base64,${institute.encodedImage}' alt="my image" /> --%>
				</article>
				
				<article class="topcontent-rds-less">
				<form action="${root}/delete" method="post">
				<input type="button" class="btn-link deleteButton" value="Delete" style="float: left; margin-right: 5px">
				<div class ="hideDel row" style="display:none;">
					<span>Enter Password to delete <span class="headtg">'${institute.nme}'</span>.</span> <br>
					<span style="color: #FF6666">Account password</span><br>		
					<span><input type="password" name="passtxt" class="input-sm"></span> 
					<span> <input type="submit" id="delete" name="deletebtn" class="btn btn-danger" value="Delete" /></span>
					<input type="button" class="btn-link forgPassButton" value="Forgot password?">
					<input type="hidden" name="url" value="${URL}">
					<input type="hidden" name="name" value="${institute.nme}">
				</div>
				</form>
					<form action="${root}/Updation" method="post">
				<input type="button" class="btn-link editButton" value="Edit" style="float: left; margin-right: 5px">
				<div class ="hideEdt row" style="display:none;">
					<span>Enter Password to Edit  <span class="headtg">'${institute.nme}'</span>.</span> <br>
					<span style="color: #FF6666">Account password</span><br>		
					<span><input type="password" name="passtxt" class="input-sm"></span>
					<span> <input type="submit" id="edit" name="editbtn" class="btn btn-success" value="Edit" /></span>
					<input type="button" class="btn-link forgPassButton" value="Forgot password?">
					<input type="hidden" name="url" value="${URL}">
					<input type="hidden" name="name" value="${institute.nme}">
				</div>
				</form>
				<form action="${root}/PasswordReset" method="post">
				<div class ="hidepass row" style="display:none;">
					<span>Enter The Email to Reset the Password for <span class="headtg">'${institute.nme}'</span>.</span> <br>
					<span style="color: #FF6666">Account Email</span><br>		
					<span><input type="text" name="txtemail" class="input-sm"></span>
					<span> <input type="submit" id="forgotbtn" name="forgotbtn" class="btn btn-info forgButton" value="Submit" /></span>
					<input type="hidden" name="url" value="${URL}">
					<input type="hidden" name="name" value="${institute.nme}">
				</div>
				</form>
				<button id="report" class="btn-link">Report</button>
				</article>
				
				</c:forEach>			
		
            </div>
            <!-- Address on MAP  -->
            <c:if test="${data eq 'yes'}">
            <div class="col-lg-4">
                <div id="map" style="width:100%;height:350px;border:1px solid #999;"></div>
             </div>
             </c:if>
                <c:if test="${data eq 'yes'}">
              <div class="col-lg-4">
              <div class="topcontent-rds-less" style="margin-top: 1%; text-align: center;">
              <span class="num"></span><span class="card__text text-primary"></span>
			</div>
            </div>
            </c:if>
            <!--Address End  -->
            

        </div>
        <!-- /.row -->
        <!--Review-Start  -->
        <c:if test="${data eq 'yes'}">
        <div class="row">
			<div class="col-lg-8">				
					<div class="topcontent-rds-less" id="post-review-box" >
		<div class="fb-like" data-href="http://www.locator.elasticbeanstalk.com/view/${URL}" data-layout="standard" data-action="like" 
		data-show-faces="true" data-share="true"></div>
		<div class="fb-comments" data-href="http://www.locator.elasticbeanstalk.com/view/${URL}" data-numposts="5" 
		data-colorscheme="light" data-version="v2.3"></div>
		</div>
		</div>
		</div>
		</c:if>
	<!--Review-End  -->

</div>        <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
				  
				<div id="dialog">
					<form class="form-horizontal" method="post" action="${root}/Report" name="repform" id="repform" onsubmit="return validation()">
                    <fieldset style="padding-left: 3%; padding-right: 2%;">
                        <h3 class="" id="ribbon-container"><span>Report this Place</span></h3>
                        <div class="form-group col-md-12 text-warning">
                        	Please describe the reason you think this place should be removed from Locator.<br>
							Your feedback helps everyone in the Locator community. Thank you.</div>
                        <div class="form-group">
                            <div class="col-md-8">
                                <input id="email" name="email" type="text" placeholder="Email Address (Required)" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8">
                                <select id="reason" name="reason" placeholder="Reason" class="form-control">
                                <option value="none" disabled="disabled" selected="selected">Select a Reason</option>
                                <option value="Duplicate">Duplicate</option>
                                <option value="Spam">Spam</option>
                                <option value="Miscotegorized">Miscotegorized</option>
                                <option value="No Longer Relevent">No Longer Relevent / available</option>
                                <option value="Other">Other</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8">
                                <textarea class="form-control" id="message" name="message" maxlength="500" rows="7"
                                placeholder="Enter your massage for us here. Example: This Place contains wrong data (Required)" ></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 text-right">
                           		<span id="worning"></span>
                                <input type="submit" name="reportsub" id="reportsub" class="btn btn-success btn-default" value="Submit">
                                <button type="button" name="cancel" id="cancel" class="btn btn-danger btn-default">Cancel</button>
                            </div>
                        </div>
                        <input id="url" name="url" type="hidden" value="${URL}">
                    </fieldset>
                </form>
				  
				</div>

			</div>
</div>
	<footer class="mainFooter">
	<p>	Copyright &copy; locator.com </p>
	</footer>
</body>	
		<script src="${pageContext.request.contextPath}/js/jquery-1.9.1.min.js" type="text/javascript"></script>
			<!-- Bootstrap Core JavaScript -->
    <script src="${pageContext.request.contextPath}/js/bootstrap.js"></script>
		<script src="${pageContext.request.contextPath}/js/view-interface.js" type="text/javascript"></script>
<%-- 		<script src="${pageContext.request.contextPath}/js/Review-rating.js" type="text/javascript"></script> --%>
		<script src="${pageContext.request.contextPath}/js/doly-dowtter.js"></script>
	<script src="${pageContext.request.contextPath}/js/delete.js" type="text/javascript"></script>
	<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/map-gog.js"></script>

</html>